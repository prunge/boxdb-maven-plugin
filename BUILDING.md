# Building BoxDB Maven Plugin

## Setup / Requirements

To build the plugin, the following is required:

- Java 1.8 or later
- Maven 3.3 or later

For running integration tests, the following software is also required:

- Docker
- Vagrant / VirtualBox

Integration tests will run under JDK 8, however JDK 17 or later is recommended as some databases
only work with these later JDK versions and won't be tested when running under JDK 8.

#### Docker

Docker is used for running a number of database types.

Installation instructions: https://www.docker.com/get-started

Linux users can install Docker using their package manager.

Windows or Mac users can download from the above link, or use 
Chocolatey / Homebrew.

Some older versions of Docker won't work with newer versions of some database's images.
The symptom is getting permission errors in the logs of these databases when they try to start up.
This can be worked around by setting the `boxdb.docker.useUnconfinedSecComp` property.
So when building BoxDB, add a `-Dboxdb.docker.useUnconfinedSecComp` parameter - it will be 
passed through to all the integration tests.

Since BoxDB 3.3, some older Docker platforms on Windows, such as docker-machine, 
won't work with the new default path binding, and will need to be configured 
to use the legacy path binding to work properly.  This can be done by setting the 
`boxdb.docker.bindPathTranslationMode` property to `LEGACY_WINDOWS`.
When building BoxDB, add a `-Dboxdb.docker.bindPathTranslationMode=LEGACY_WINDOWS` to the
command line.  It will be passed through to all the integration tests.

#### Vagrant / VirtualBox

Vagrant is used for running SQL Server for Windows.

- Vagrant installation: https://www.vagrantup.com/downloads.html
- VirtualBox installation: https://www.virtualbox.org/wiki/Downloads  

Vagrant needs to be on the `PATH` for it to be picked up by the plugin.
The installer should do this by default.

## Building

#### Full build

Run the following to perform a full build, including running integration tests:

```
mvn clean install
```

The integration tests take a long time (over an hour), as they exercise 
all the functionality of every database type.

#### Simple build

During development, running every integration test every time is overkill.
To build the plugin without running the integration tests, use:

```
mvn clean install -DskipITs
```  

#### Running particular integration tests

This is useful when modifying code for a single database.  Use:

```
mvn clean install -Dinvoker.test=asetup-*,<test name>
```

where `<test name>` is the name of a test under the `src/it` directory.
It is necessary to always include the asetup-* tests since they are prerequisites
for all the other tests, including parent POMs etc.  Depending on your shell, the
`*` might need escaping with `\` (such as when running under Bash shell) so you might
need to use `-Dinvoker.test=asetup-\*,...` instead.

For example, to run the Postgres backup/restore tests only when using Linux / Bash, use:

```
mvn clean install -Dinvoker.test=asetup-\*,docker-postgres-backuprestore
```

To run all Postgres collation tests when using Windows:

```
mvn clean install -Dinvoker.test=asetup-*,docker-postgres-collation*
```

