<project xmlns="http://maven.apache.org/POM/4.0.0" xmlns:xsi="http://www.w3.org/2001/XMLSchema-instance" xsi:schemaLocation="http://maven.apache.org/POM/4.0.0 http://maven.apache.org/maven-v4_0_0.xsd">
    <modelVersion>4.0.0</modelVersion>

    <groupId>au.net.causal.maven.plugins</groupId>
    <artifactId>boxdb-maven-plugin</artifactId>
    <version>3.4-SNAPSHOT</version>
    <packaging>maven-plugin</packaging>

    <name>Boxed Database Maven Plugin</name>
    <description>Maven plugin to start databases using Docker and VMs</description>
    <url>https://bitbucket.org/prunge/boxdb-maven-plugin</url>

    <inceptionYear>2016</inceptionYear>

    <properties>
        <project.build.sourceEncoding>UTF-8</project.build.sourceEncoding>
        <maven.version>3.3.9</maven.version>

        <!--
        This setting is passed through to the integration tests and may be needed when running on a platform with an old Docker version that is
        incompatible with newer database images.
        -->
        <boxdb.docker.useUnconfinedSecComp>false</boxdb.docker.useUnconfinedSecComp>

        <!--
        Another setting passed through to the integration tests that may be needed to be set to LEGACY_WINDOWS
        when running on a Windows platform with docker-machine.
        -->
        <boxdb.docker.bindPathTranslationMode>STANDARD</boxdb.docker.bindPathTranslationMode>

        <!-- supportedDatabaseVersions are used in integration tests, configuring lowest/highest supported database versions -->

        <!-- Derby, HSQLDB, H2 latest supported for Java 8, profiles configure tests with later versions depending on JDK version -->
        <boxdb.supportedDatabaseVersions.derby.latest>10.14.2.0</boxdb.supportedDatabaseVersions.derby.latest>
        <boxdb.supportedDatabaseVersions.derby.oldest>10.9.1.0</boxdb.supportedDatabaseVersions.derby.oldest>

        <boxdb.supportedDatabaseVersions.hsql.latest>2.5.1</boxdb.supportedDatabaseVersions.hsql.latest>
        <boxdb.supportedDatabaseVersions.hsql.oldest>2.3.0</boxdb.supportedDatabaseVersions.hsql.oldest>

        <boxdb.supportedDatabaseVersions.h2.latest>2.2.224</boxdb.supportedDatabaseVersions.h2.latest>
        <boxdb.supportedDatabaseVersions.h2.oldest>1.4.177</boxdb.supportedDatabaseVersions.h2.oldest>

        <boxdb.supportedDatabaseVersions.sqlite.latest>3.46.0.0</boxdb.supportedDatabaseVersions.sqlite.latest>
        <boxdb.supportedDatabaseVersions.sqlite.oldest>3.8.6</boxdb.supportedDatabaseVersions.sqlite.oldest>

        <boxdb.supportedDatabaseVersions.mariadb.latest>11.4.2</boxdb.supportedDatabaseVersions.mariadb.latest>
        <boxdb.supportedDatabaseVersions.mariadb.oldest>5.5</boxdb.supportedDatabaseVersions.mariadb.oldest>

        <boxdb.supportedDatabaseVersions.mysql.latest>9.0.0</boxdb.supportedDatabaseVersions.mysql.latest>
        <boxdb.supportedDatabaseVersions.mysql.oldest>5.5</boxdb.supportedDatabaseVersions.mysql.oldest>

        <boxdb.supportedDatabaseVersions.postgres.latest>16.3</boxdb.supportedDatabaseVersions.postgres.latest>
        <boxdb.supportedDatabaseVersions.postgres.oldest>8.4</boxdb.supportedDatabaseVersions.postgres.oldest>

        <boxdb.supportedDatabaseVersions.postgis.latest>16-3.4</boxdb.supportedDatabaseVersions.postgis.latest>
        <boxdb.supportedDatabaseVersions.postgis.oldest>10.0-2.4</boxdb.supportedDatabaseVersions.postgis.oldest>

        <boxdb.supportedDatabaseVersions.sqlserver-linux.latest>2022-CU13-ubuntu-22.04</boxdb.supportedDatabaseVersions.sqlserver-linux.latest>
        <boxdb.supportedDatabaseVersions.sqlserver-linux.oldest>2017-GA</boxdb.supportedDatabaseVersions.sqlserver-linux.oldest>

        <boxdb.supportedDatabaseVersions.cockroachdb.latest>23.1.23</boxdb.supportedDatabaseVersions.cockroachdb.latest>
        <boxdb.supportedDatabaseVersions.cockroachdb.oldest>1.0</boxdb.supportedDatabaseVersions.cockroachdb.oldest>

        <boxdb.supportedDatabaseVersions.db2.latest>11.5.9.0</boxdb.supportedDatabaseVersions.db2.latest>
        <boxdb.supportedDatabaseVersions.db2.oldest>11.5.0.0a</boxdb.supportedDatabaseVersions.db2.oldest>
    </properties>

    <licenses>
        <license>
            <name>The Apache Software License, Version 2.0</name>
            <url>http://www.apache.org/licenses/LICENSE-2.0.txt</url>
            <distribution>repo</distribution>
        </license>
    </licenses>

    <scm>
        <connection>scm:git:ssh://git@bitbucket.org/prunge/boxdb-maven-plugin.git</connection>
        <developerConnection>scm:git:ssh://git@bitbucket.org/prunge/boxdb-maven-plugin.git</developerConnection>
        <url>https://bitbucket.org/prunge/boxdb-maven-plugin/src</url>
        <tag>HEAD</tag>
    </scm>

    <distributionManagement>
        <snapshotRepository>
            <id>ossrh</id>
            <url>https://oss.sonatype.org/content/repositories/snapshots</url>
        </snapshotRepository>
    </distributionManagement>

    <developers>
        <developer>
            <id>prunge</id>
            <name>Peter Runge</name>
            <email>prunge@causal.net.au</email>
            <roles>
                <role>Developer</role>
            </roles>
        </developer>
    </developers>

    <dependencyManagement>
        <dependencies>
            <dependency>
                <groupId>org.junit</groupId>
                <artifactId>junit-bom</artifactId>
                <version>5.10.3</version>
                <type>pom</type>
                <scope>import</scope>
            </dependency>
        </dependencies>
    </dependencyManagement>

    <dependencies>
        <dependency>
            <groupId>org.apache.maven</groupId>
            <artifactId>maven-plugin-api</artifactId>
            <version>${maven.version}</version>
        </dependency>
        <dependency>
            <groupId>org.apache.maven</groupId>
            <artifactId>maven-core</artifactId>
            <version>${maven.version}</version>
        </dependency>

        <dependency>
            <groupId>org.codehaus.plexus</groupId>
            <artifactId>plexus-interpolation</artifactId>
            <version>1.24</version>
        </dependency>

        <dependency>
            <groupId>org.apache.maven.plugin-tools</groupId>
            <artifactId>maven-plugin-annotations</artifactId>
            <version>3.6.0</version>
            <scope>provided</scope>
        </dependency>

        <dependency>
            <groupId>org.apache.maven.shared</groupId>
            <artifactId>maven-filtering</artifactId>
            <version>3.1.1</version>
        </dependency>
        <dependency>
            <groupId>org.codehaus.plexus</groupId>
            <artifactId>plexus-archiver</artifactId>
            <version>3.4</version>
        </dependency>

        <dependency>
            <groupId>io.fabric8</groupId>
            <artifactId>docker-maven-plugin</artifactId>
            <version>0.44.0</version>
            <exclusions>
                <exclusion>
                    <groupId>log4j</groupId>
                    <artifactId>log4j</artifactId>
                </exclusion>
                <exclusion>
                    <groupId>commons-logging</groupId>
                    <artifactId>commons-logging</artifactId>
                </exclusion>
            </exclusions>
        </dependency>

        <dependency>
            <groupId>org.slf4j</groupId>
            <artifactId>log4j-over-slf4j</artifactId>
            <version>1.7.22</version>
        </dependency>
        <dependency>
            <groupId>org.slf4j</groupId>
            <artifactId>jcl-over-slf4j</artifactId>
            <version>1.7.22</version>
        </dependency>
        <dependency>
            <groupId>org.slf4j</groupId>
            <artifactId>slf4j-simple</artifactId>
            <version>1.7.22</version>
            <scope>test</scope>
        </dependency>

        <dependency>
            <groupId>org.twdata.maven</groupId>
            <artifactId>mojo-executor</artifactId>
            <version>2.2.0</version>
        </dependency>

        <dependency>
            <groupId>org.codehaus.mojo</groupId>
            <artifactId>sql-maven-plugin</artifactId>
            <version>1.5</version>
            <exclusions>
                <exclusion>
                    <groupId>junit</groupId>
                    <artifactId>junit</artifactId>
                </exclusion>
            </exclusions>
        </dependency>

        <dependency>
            <groupId>org.junit.jupiter</groupId>
            <artifactId>junit-jupiter-engine</artifactId>
            <scope>test</scope>
        </dependency>
        <dependency>
            <groupId>org.assertj</groupId>
            <artifactId>assertj-core</artifactId>
            <version>3.26.3</version>
            <scope>test</scope>
        </dependency>
        <dependency>
            <groupId>com.github.paweladamski</groupId>
            <artifactId>HttpClientMock</artifactId>
            <version>1.5.0</version>
            <scope>test</scope>
        </dependency>
    </dependencies>

    <build>
        <pluginManagement>
            <plugins>
                <plugin>
                    <groupId>org.apache.maven.plugins</groupId>
                    <artifactId>maven-surefire-plugin</artifactId>
                    <version>3.3.1</version>
                </plugin>
                <plugin>
                    <groupId>org.apache.maven.plugins</groupId>
                    <artifactId>maven-failsafe-plugin</artifactId>
                    <version>3.3.1</version>
                </plugin>
            </plugins>
        </pluginManagement>
        <plugins>
            <plugin>
                <groupId>org.apache.maven.plugins</groupId>
                <artifactId>maven-compiler-plugin</artifactId>
                <version>3.13.0</version>
                <configuration>
                    <source>1.8</source>
                    <target>1.8</target>
                    <parameters>true</parameters>
                </configuration>
            </plugin>
            <plugin>
                <groupId>org.apache.maven.plugins</groupId>
                <artifactId>maven-plugin-plugin</artifactId>
                <version>3.13.1</version>
                <executions>
                    <execution>
                        <id>default-descriptor</id>
                        <phase>process-classes</phase>
                    </execution>
                    <execution>
                        <id>help-goal</id>
                        <goals>
                            <goal>helpmojo</goal>
                        </goals>
                    </execution>
                </executions>
            </plugin>
            <plugin>
                <groupId>org.apache.maven.plugins</groupId>
                <artifactId>maven-invoker-plugin</artifactId>
                <version>3.7.0</version>
                <configuration>
                    <cloneProjectsTo>${project.build.directory}/it</cloneProjectsTo>
                    <settingsFile>src/it/settings.xml</settingsFile>
                    <localRepositoryPath>${project.build.directory}/local-repo</localRepositoryPath>
                    <postBuildHookScript>verify</postBuildHookScript>
                    <pomIncludes>
                        <pomInclude>*</pomInclude>
                    </pomIncludes>
                    <setupIncludes>
                        <setupInclude>asetup-*/pom.xml</setupInclude>
                    </setupIncludes>
                    <goals>
                        <goal>clean</goal>
                        <goal>verify</goal>
                    </goals>
                    <skipInvocation>${skipITs}</skipInvocation>
                    <properties>
                        <user.home>${project.build.directory}/it/userhome</user.home>
                        <boxdb.docker.useUnconfinedSecComp>${boxdb.docker.useUnconfinedSecComp}</boxdb.docker.useUnconfinedSecComp>
                        <boxdb.docker.bindPathTranslationMode>${boxdb.docker.bindPathTranslationMode}</boxdb.docker.bindPathTranslationMode>
                    </properties>
                    <scriptVariables>
                        <boxdb_supportedDatabaseVersions_derby_latest>${boxdb.supportedDatabaseVersions.derby.latest}</boxdb_supportedDatabaseVersions_derby_latest>
                        <boxdb_supportedDatabaseVersions_derby_oldest>${boxdb.supportedDatabaseVersions.derby.oldest}</boxdb_supportedDatabaseVersions_derby_oldest>
                        <boxdb_supportedDatabaseVersions_h2_latest>${boxdb.supportedDatabaseVersions.h2.latest}</boxdb_supportedDatabaseVersions_h2_latest>
                        <boxdb_supportedDatabaseVersions_h2_oldest>${boxdb.supportedDatabaseVersions.h2.oldest}</boxdb_supportedDatabaseVersions_h2_oldest>
                        <boxdb_supportedDatabaseVersions_hsql_latest>${boxdb.supportedDatabaseVersions.hsql.latest}</boxdb_supportedDatabaseVersions_hsql_latest>
                        <boxdb_supportedDatabaseVersions_hsql_oldest>${boxdb.supportedDatabaseVersions.hsql.oldest}</boxdb_supportedDatabaseVersions_hsql_oldest>
                        <boxdb_supportedDatabaseVersions_sqlite_latest>${boxdb.supportedDatabaseVersions.sqlite.latest}</boxdb_supportedDatabaseVersions_sqlite_latest>
                        <boxdb_supportedDatabaseVersions_sqlite_oldest>${boxdb.supportedDatabaseVersions.sqlite.oldest}</boxdb_supportedDatabaseVersions_sqlite_oldest>
                        <boxdb_supportedDatabaseVersions_mariadb_latest>${boxdb.supportedDatabaseVersions.mariadb.latest}</boxdb_supportedDatabaseVersions_mariadb_latest>
                        <boxdb_supportedDatabaseVersions_mariadb_oldest>${boxdb.supportedDatabaseVersions.mariadb.oldest}</boxdb_supportedDatabaseVersions_mariadb_oldest>
                        <boxdb_supportedDatabaseVersions_mysql_latest>${boxdb.supportedDatabaseVersions.mysql.latest}</boxdb_supportedDatabaseVersions_mysql_latest>
                        <boxdb_supportedDatabaseVersions_mysql_oldest>${boxdb.supportedDatabaseVersions.mysql.oldest}</boxdb_supportedDatabaseVersions_mysql_oldest>
                        <boxdb_supportedDatabaseVersions_postgres_latest>${boxdb.supportedDatabaseVersions.postgres.latest}</boxdb_supportedDatabaseVersions_postgres_latest>
                        <boxdb_supportedDatabaseVersions_postgres_oldest>${boxdb.supportedDatabaseVersions.postgres.oldest}</boxdb_supportedDatabaseVersions_postgres_oldest>
                        <boxdb_supportedDatabaseVersions_postgis_latest>${boxdb.supportedDatabaseVersions.postgis.latest}</boxdb_supportedDatabaseVersions_postgis_latest>
                        <boxdb_supportedDatabaseVersions_postgis_oldest>${boxdb.supportedDatabaseVersions.postgis.oldest}</boxdb_supportedDatabaseVersions_postgis_oldest>
                        <boxdb_supportedDatabaseVersions_sqlserver_linux_latest>${boxdb.supportedDatabaseVersions.sqlserver-linux.latest}</boxdb_supportedDatabaseVersions_sqlserver_linux_latest>
                        <boxdb_supportedDatabaseVersions_sqlserver_linux_oldest>${boxdb.supportedDatabaseVersions.sqlserver-linux.oldest}</boxdb_supportedDatabaseVersions_sqlserver_linux_oldest>
                        <boxdb_supportedDatabaseVersions_cockroachdb_latest>${boxdb.supportedDatabaseVersions.cockroachdb.latest}</boxdb_supportedDatabaseVersions_cockroachdb_latest>
                        <boxdb_supportedDatabaseVersions_cockroachdb_oldest>${boxdb.supportedDatabaseVersions.cockroachdb.oldest}</boxdb_supportedDatabaseVersions_cockroachdb_oldest>
                        <boxdb_supportedDatabaseVersions_db2_latest>${boxdb.supportedDatabaseVersions.db2.latest}</boxdb_supportedDatabaseVersions_db2_latest>
                        <boxdb_supportedDatabaseVersions_db2_oldest>${boxdb.supportedDatabaseVersions.db2.oldest}</boxdb_supportedDatabaseVersions_db2_oldest>
                    </scriptVariables>
                </configuration>
                <executions>
                    <execution>
                        <id>integration-test</id>
                        <goals>
                            <goal>install</goal>
                            <goal>run</goal>
                        </goals>
                    </execution>
                </executions>
            </plugin>
            <plugin>
                <groupId>org.apache.maven.plugins</groupId>
                <artifactId>maven-checkstyle-plugin</artifactId>
                <version>3.4.0</version>
                <executions>
                    <execution>
                        <id>verify-style</id>
                        <phase>process-classes</phase>
                        <goals>
                            <goal>check</goal>
                        </goals>
                    </execution>
                </executions>
                <configuration>
                    <configLocation>src/checkstyle/checkstyle.xml</configLocation>
                    <!--
                    <violationSeverity>warning</violationSeverity>
                    -->
                    <consoleOutput>true</consoleOutput>
                    <includeTestSourceDirectory>true</includeTestSourceDirectory>
                </configuration>
            </plugin>

            <plugin>
                <groupId>org.apache.maven.plugins</groupId>
                <artifactId>maven-source-plugin</artifactId>
                <version>3.3.1</version>
                <executions>
                    <execution>
                        <id>attach-sources</id>
                        <goals>
                            <goal>jar-no-fork</goal>
                        </goals>
                    </execution>
                </executions>
            </plugin>
            <plugin>
                <groupId>org.apache.maven.plugins</groupId>
                <artifactId>maven-javadoc-plugin</artifactId>
                <version>3.7.0</version>
                <configuration>
                    <source>1.8</source>
                </configuration>
                <executions>
                    <execution>
                        <id>attach-javadocs</id>
                        <goals>
                            <goal>jar</goal>
                        </goals>
                    </execution>
                </executions>
            </plugin>
            <plugin>
                <groupId>org.sonatype.plugins</groupId>
                <artifactId>nexus-staging-maven-plugin</artifactId>
                <version>1.6.8</version>
                <extensions>true</extensions>
                <configuration>
                    <serverId>ossrh</serverId>
                    <nexusUrl>https://oss.sonatype.org/</nexusUrl>
                    <autoReleaseAfterClose>false</autoReleaseAfterClose>
                </configuration>
            </plugin>
            <plugin>
                <groupId>org.apache.maven.plugins</groupId>
                <artifactId>maven-release-plugin</artifactId>
                <version>2.5.3</version>
                <configuration>
                    <releaseProfiles>release</releaseProfiles>
                </configuration>
            </plugin>
        </plugins>
    </build>

    <profiles>
        <!-- Only make GPG signatures during release -->
        <profile>
            <id>release</id>
            <build>
                <plugins>
                    <plugin>
                        <groupId>org.apache.maven.plugins</groupId>
                        <artifactId>maven-gpg-plugin</artifactId>
                        <version>3.2.4</version>
                        <executions>
                            <execution>
                                <id>sign-artifacts</id>
                                <phase>verify</phase>
                                <goals>
                                    <goal>sign</goal>
                                </goals>
                            </execution>
                        </executions>
                    </plugin>
                </plugins>
            </build>
        </profile>

        <!-- Makes checkstyle warnings fail the build for both project sources and the ITs -->
        <profile>
            <id>checkstyle-errors</id>
            <properties>
                <checkstyle.failOnViolation>true</checkstyle.failOnViolation>
                <checkstyle.violationSeverity>warning</checkstyle.violationSeverity>
            </properties>
            <build>
                <plugins>
                    <plugin>
                        <groupId>org.apache.maven.plugins</groupId>
                        <artifactId>maven-invoker-plugin</artifactId>
                        <version>3.7.0</version>
                        <configuration>
                            <properties>
                                <checkstyle.failOnViolation>true</checkstyle.failOnViolation>
                                <checkstyle.violationSeverity>warning</checkstyle.violationSeverity>
                            </properties>
                        </configuration>
                    </plugin>
                </plugins>
            </build>
        </profile>
        <profile>
            <id>nodocker</id>
            <build>
                <plugins>
                    <plugin>
                        <groupId>org.apache.maven.plugins</groupId>
                        <artifactId>maven-invoker-plugin</artifactId>
                        <version>3.7.0</version>
                        <configuration>
                            <pomExcludes>
                                <pomExclude>docker-*/pom.xml</pomExclude>
                            </pomExcludes>
                        </configuration>
                    </plugin>
                </plugins>
            </build>
        </profile>
        <profile>
            <id>novagrant</id>
            <build>
                <plugins>
                    <plugin>
                        <groupId>org.apache.maven.plugins</groupId>
                        <artifactId>maven-invoker-plugin</artifactId>
                        <version>3.7.0</version>
                        <configuration>
                            <pomExcludes>
                                <pomExclude>vagrant-*/pom.xml</pomExclude>
                            </pomExcludes>
                        </configuration>
                    </plugin>
                </plugins>
            </build>
        </profile>
        <profile>
            <id>nooracle</id>
            <build>
                <plugins>
                    <plugin>
                        <groupId>org.apache.maven.plugins</groupId>
                        <artifactId>maven-invoker-plugin</artifactId>
                        <version>3.7.0</version>
                        <configuration>
                            <pomExcludes>
                                <pomExclude>docker-oracle*/pom.xml</pomExclude>
                            </pomExcludes>
                        </configuration>
                    </plugin>
                </plugins>
            </build>
        </profile>

        <profile>
            <id>java11orlater</id>
            <activation>
                <jdk>[11,17)</jdk>
            </activation>
            <properties>
                <!-- Derby 10.15 needs Java 9+ -->
                <boxdb.supportedDatabaseVersions.derby.latest>10.15.2.0</boxdb.supportedDatabaseVersions.derby.latest>

                <!-- HSQLDB 2.6.x and 2.7.x needs Java 11+ -->
                <boxdb.supportedDatabaseVersions.hsql.latest>2.7.3</boxdb.supportedDatabaseVersions.hsql.latest>

                <!-- H2 2.3.x needs Java 11+ -->
                <boxdb.supportedDatabaseVersions.h2.latest>2.3.230</boxdb.supportedDatabaseVersions.h2.latest>
            </properties>
        </profile>
        <profile>
            <id>java17orlater</id>
            <activation>
                <jdk>[17,21)</jdk>
            </activation>
            <properties>
                <!-- Derby 10.16 needs Java 17+ -->
                <boxdb.supportedDatabaseVersions.derby.latest>10.16.1.1</boxdb.supportedDatabaseVersions.derby.latest>

                <!-- HSQLDB 2.6.x and 2.7.x needs Java 11+ -->
                <boxdb.supportedDatabaseVersions.hsql.latest>2.7.3</boxdb.supportedDatabaseVersions.hsql.latest>

                <!-- H2 2.3.x needs Java 11+ -->
                <boxdb.supportedDatabaseVersions.h2.latest>2.3.230</boxdb.supportedDatabaseVersions.h2.latest>
            </properties>
        </profile>
        <profile>
            <id>java21orlater</id>
            <activation>
                <jdk>[21,)</jdk>
            </activation>
            <properties>
                <!-- Derby 10.17 needs Java 21+ -->
                <boxdb.supportedDatabaseVersions.derby.latest>10.17.1.0</boxdb.supportedDatabaseVersions.derby.latest>

                <!-- HSQLDB 2.6.x and 2.7.x needs Java 11+ -->
                <boxdb.supportedDatabaseVersions.hsql.latest>2.7.3</boxdb.supportedDatabaseVersions.hsql.latest>

                <!-- H2 2.3.x needs Java 11+ -->
                <boxdb.supportedDatabaseVersions.h2.latest>2.3.230</boxdb.supportedDatabaseVersions.h2.latest>
            </properties>
        </profile>
        
        <profile>
            <id>tychotest</id>
            <build>
                <plugins>
                    <plugin>
                        <groupId>org.eclipse.tycho.extras</groupId>
                        <artifactId>tycho-eclipserun-plugin</artifactId>
                        <version>1.2.0</version>
                        <executions>
                            <execution>
                                <id>run-something</id>
                                <phase>initialize</phase>
                                <goals>
                                    <goal>eclipse-run</goal>
                                </goals>
                                <configuration>
                                    <repositories>
                                        <repository>
                                            <id>2018-09</id>
                                            <layout>p2</layout>
                                            <url>http://download.eclipse.org/releases/2018-09/</url>
                                        </repository>
                                        <repository>
                                            <id>dbeaver</id>
                                            <layout>p2</layout>
                                            <url>https://dbeaver.io/update/ce/latest/</url>
                                        </repository>
                                        <repository>
                                            <id>dbeaver-svg</id>
                                            <layout>p2</layout>
                                            <url>https://dbeaver.io/update/svg/latest/</url>
                                        </repository>
                                        <repository>
                                            <id>dbeaver-sshj</id>
                                            <layout>p2</layout>
                                            <url>https://dbeaver.io/update/sshj/latest/</url>
                                        </repository>
                                    </repositories>
                                    <appArgLine>-consoleLog -application org.eclipse.ui.ide.workbench -product org.eclipse.epp.package.standard.product</appArgLine>
                                    <dependencies>
                                        <dependency>
                                            <artifactId>org.jkiss.dbeaver.runtime.feature</artifactId>
                                            <type>eclipse-feature</type>
                                        </dependency>
                                        <!--
                                        <dependency>
                                            <artifactId>org.eclipse.sdk.ide</artifactId>
                                            <type>eclipse-feature</type>
                                        </dependency>
                                        -->
                                        
                                        <!--
                                        <dependency>
                                            <artifactId>org.jkiss.dbeaver.core.application</artifactId>
                                            <type>eclipse-plugin</type>
                                        </dependency>
                                        -->
                                        
                                        <dependency>
                                            <artifactId>javax.annotation</artifactId>
                                            <type>eclipse-plugin</type>
                                        </dependency>
                                        <dependency>
                                            <artifactId>org.eclipse.osgi</artifactId>
                                            <type>eclipse-plugin</type>
                                        </dependency>
                                        <dependency>
                                            <artifactId>org.eclipse.equinox.simpleconfigurator</artifactId>
                                            <type>eclipse-plugin</type>
                                        </dependency>
                                        <dependency>
                                            <artifactId>org.eclipse.core.runtime</artifactId>
                                            <type>eclipse-plugin</type>
                                        </dependency>
                                        <dependency>
                                            <artifactId>org.eclipse.equinox.common</artifactId>
                                            <type>eclipse-plugin</type>
                                        </dependency>
                                        <dependency>
                                            <artifactId>org.eclipse.equinox.ds</artifactId>
                                            <type>eclipse-plugin</type>
                                        </dependency>
                                        <dependency>
                                            <artifactId>org.eclipse.equinox.event</artifactId>
                                            <type>eclipse-plugin</type>
                                        </dependency>
                                        <dependency>
                                            <artifactId>org.eclipse.update.configurator</artifactId>
                                            <type>eclipse-plugin</type>
                                        </dependency>
                                        <dependency>
                                            <artifactId>org.eclipse.ui</artifactId>
                                            <type>eclipse-plugin</type>
                                        </dependency>
                                        <dependency>
                                            <artifactId>org.eclipse.ui.ide</artifactId>
                                            <type>eclipse-plugin</type>
                                        </dependency>
                                        <dependency>
                                            <artifactId>org.eclipse.ui.workbench.texteditor</artifactId>
                                            <type>eclipse-plugin</type>
                                        </dependency>
                                        <dependency>
                                            <artifactId>org.eclipse.ui.workbench</artifactId>
                                            <type>eclipse-plugin</type>
                                        </dependency>
                                        
                                       
                                        
                                        <!--
                                        <plugin id="org.eclipse.osgi" autoStart="true" startLevel="-1" />
                                        <plugin id="org.eclipse.equinox.simpleconfigurator" autoStart="true" startLevel="1" />
                                        <plugin id="org.eclipse.core.runtime" autoStart="true" startLevel="4" />
                                        <plugin id="org.eclipse.equinox.common" autoStart="true" startLevel="2" />
                                        <plugin id="org.eclipse.equinox.ds" autoStart="true" startLevel="2" />
                                        <plugin id="org.eclipse.equinox.event" autoStart="true" startLevel="2" />
                                        <plugin id="org.eclipse.update.configurator" autoStart="false" startLevel="10" />
                                        -->
                                        <!--
                                        <dependency>
                                            <artifactId>org.apache.ant</artifactId>
                                            <type>eclipse-plugin</type>
                                        </dependency>
                                        <dependency>
                                            <artifactId>org.eclipse.help.base</artifactId>
                                            <type>eclipse-plugin</type>
                                        </dependency>
                                        -->
                                    </dependencies>
                                </configuration>
                            </execution>
                        </executions>
                    </plugin>
                </plugins>
            </build>
        </profile>

        <profile>
            <id>tychotest2</id>
            <build>
                <plugins>
                    <plugin>
                        <groupId>org.eclipse.tycho.extras</groupId>
                        <artifactId>tycho-eclipserun-plugin</artifactId>
                        <version>1.2.0</version>
                        <executions>
                            <execution>
                                <id>run-something</id>
                                <phase>initialize</phase>
                                <goals>
                                    <goal>eclipse-run</goal>
                                </goals>
                                <configuration>
                                    <repositories>
                                        <repository>
                                            <id>2018-09</id>
                                            <layout>p2</layout>
                                            <url>http://download.eclipse.org/releases/2018-09/</url>
                                        </repository>
                                    </repositories>
                                    <appArgLine>-consoleLog -application org.eclipse.ui.ide.workbench</appArgLine>
                                    <dependencies>
                                        <dependency>
                                            <artifactId>javax.annotation</artifactId>
                                            <type>eclipse-plugin</type>
                                        </dependency>
                                        <dependency>
                                            <artifactId>org.eclipse.osgi</artifactId>
                                            <type>eclipse-plugin</type>
                                        </dependency>
                                        <dependency>
                                            <artifactId>org.eclipse.equinox.simpleconfigurator</artifactId>
                                            <type>eclipse-plugin</type>
                                        </dependency>
                                        <dependency>
                                            <artifactId>org.eclipse.core.runtime</artifactId>
                                            <type>eclipse-plugin</type>
                                        </dependency>
                                        <dependency>
                                            <artifactId>org.eclipse.equinox.common</artifactId>
                                            <type>eclipse-plugin</type>
                                        </dependency>
                                        <dependency>
                                            <artifactId>org.eclipse.equinox.ds</artifactId>
                                            <type>eclipse-plugin</type>
                                        </dependency>
                                        <dependency>
                                            <artifactId>org.eclipse.equinox.event</artifactId>
                                            <type>eclipse-plugin</type>
                                        </dependency>
                                        <dependency>
                                            <artifactId>org.eclipse.update.configurator</artifactId>
                                            <type>eclipse-plugin</type>
                                        </dependency>
                                        <dependency>
                                            <artifactId>org.eclipse.ui</artifactId>
                                            <type>eclipse-plugin</type>
                                        </dependency>
                                        <dependency>
                                            <artifactId>org.eclipse.ui.ide</artifactId>
                                            <type>eclipse-plugin</type>
                                        </dependency>
                                        <dependency>
                                            <artifactId>org.eclipse.ui.workbench.texteditor</artifactId>
                                            <type>eclipse-plugin</type>
                                        </dependency>
                                        <dependency>
                                            <artifactId>org.eclipse.ui.workbench</artifactId>
                                            <type>eclipse-plugin</type>
                                        </dependency>
                                    </dependencies>
                                </configuration>
                            </execution>
                        </executions>
                    </plugin>
                </plugins>
            </build>
        </profile>
    </profiles>
</project>
