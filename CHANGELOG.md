# Change Log

### 3.3
2024-07-21

- Upgrade Docker plugin to 0.44.0
- Fixed UnsatisfiedLinkError issues from JFFI running on Apple Silicon Macs
- Upgrade plugins/build so that BoxDB can be built on Java 21
- Support latest version of H2, use latest JDK8 compatible version as default
- Use latest version of SQLite as default
- Support/test latest versions of HSQLDB and Derby
- Support latest version of Postgres, use latest JDBC driver version
- Support latest version of CockroachDB, use latest JDBC driver version
- Support latest version of PostGIS, use latest JDBC driver version
- Support/test latest version of SQLServer, use latest version as default
- Support latest version of DB2 and use latest version as default
- Support latest version of MariaDB, fix problems with versions 11 and later 
and using the wrong binary name, use latest JDBC driver
- Support latest version of MySQL, fix problems with version 8.4+ by using 
sha2_password instead of now-removed mysql_native_password plugin, use latest JDBC driver
- Support latest version of Oracle (23), use latest JDBC driver
- Finally fixed the flakiness issues on Oracle 12 for good this time by recreating the 
container DB with CPU count of 2.  Oracle 12 would previously fail with memory issues when host
had high number of CPUs.
- Fixed BoxDB plugin breaking ANSI/color in Maven after its execution
- Fixed flakiness in JDBC driver classloading when running Vagrant integration tests
- Add feature to allow customization of Docker environment variables for Docker databases
- Fixed support for running with Rancher Desktop and Podman on Windows (due to upgrade of Docker plugin)
- Add property `boxdb.docker.bindPathTranslationMode` that may be needed on some old Docker platforms
(e.g. docker machine) on Windows.  Set this property to 'LEGACY_WINDOWS' if you are getting path binding errors.

### 3.2
2022-09-11

- Use Oracle JDBC driver that is in Maven central now
- Improve flakiness of Oracle 12 on some systems (but not solved completely) by
repeatedly trying to do a backup of a small database until it succeeds - previously there were
some issues where the database was responding to JDBC commands but still wasn't fully ready
- Use some different memory settings for Oracle to improve stability on some systems
- Support latest version of SQL Server/Linux, use latest JDBC driver version
- Support latest version of MariaDB, use latest JDBC driver version
- Support latest version of MySQL, use latest JDBC driver version
- Support latest version of Postgres, use latest JDBC driver version
- Support latest version of PostGIS, use latest JDBC driver version
- Support latest version of CockroachDB, use latest JDBC driver version
- Support Oracle versions 18 and 21
- Fix DB2 always picking the 'latest' Docker tag and not using the version specified.  Now it is 
possible to use other DB2 versions.
- Support latest versions of HSQL, H2 and Derby.
- Allow the names of system properties that are exported after a database starts up to be
customized (previously was hardcoded to db.url.property, etc.).  This is handy when
starting up multiple databases as part of a build.
- Upgrade Docker plugin to 0.40.1
- Many databases now have upgraded default versions
- Add property `boxdb.docker.useUnconfinedSecComp` that can be used to work around bugs when older
Docker versions attempt to use some more recent database images
- The plugin's own integration tests can now be run using JDK 8.  Build profiles detect
more recent JDKs that allow testing with some recent database versions that need later JDK versions

### 3.1
2019-10-27

- Use `psql` from PostGIS image instead of spinning up a different 
vanilla Postgres container's `psql`
- Add [CockroachDB](https://www.cockroachlabs.com) support
- Upgrade Docker plugin to 0.31.0
- Use TCP password on shutdown port of H2 database since the latest release seems to
require it

### 3.0
2019-08-31

- JDBC waiting is now turned on by default - there are too many situations
and Docker implementations that made pure-socket waiting unreliable.  It 
can still be turned off through configuration.
- New `versions` goal that lists available versions of each database type
- New `prepare` goal that downloads all dependencies of a database without
actually running it.  Useful to run when going offline.
- Fixed Derby JDBC waiting not working by checking exception details
- Fixed Derby race errors when running scripts and the database had not restarted yet
- Fixed broken networking in Vagrant 2.2.1 by explicitly configuring NIC type
- Fixed SQL Server for Linux and Oracle backup permission problem on some Docker platforms
- Improved detection of not-yet-started SQL Server for Linux database
- Reduce log spam when using JDBC waiting
- Use new Microsoft SQL Server Docker (MCR) repository.  This gives access to 
the new 2019 preview versions.
- Use different Oracle Docker images since the old ones have been removed
- Moved from Mercurial to Git
- Integration testing - test current, oldest supported and newest supported
versions of many databases.  This should catch more database version 
compatibility problems.
- Integration testing - validate the existence of remote Docker and Vagrant
images.  This should catch problems where Docker images that the plugin
relies upon have been removed.
- Upgrade Docker plugin to 0.30.0
- Change to new DB2 Docker image since the old one is no longer available
- Added support for latest release versions of Apache Derby and H2.
- Upgrade default versions of most supported database types to their
latest release versions.
- Add ability to configure database port and setup (pre-database-create) 
SQL files using properies on the command line.
- Add PostGIS support
 
### 2.4
2018-11-11

- Fix Squirrel goal compatibility with Java 11
- Upgrade default versions of databases to latest versions
- Fixed some issues with reading Vagrant state, including handling new version availability messages
- Fix create script regression with MySQL < 5.7 introduced in previous version 2.3
- Upgrade Docker plugin to 0.27.2

### 2.3
2018-11-04

- Compatibilty fixes for Java 11 (thanks Manish Goyal)
- Workaround failing backup/restore with SQL Server for Linux due to Docker/filesystem issues
- Fix Vagrant box list parse failures when there are no installed boxes

### 2.2
2017-11-29

- Fixed MySQL 8 compatibility problem which resulted in `Unknown system variable 'query_cache_size'`
when using JDBC via Squirrel goal or JDBC waiting by upgrading to MySQL JDBC driver version 5.1.44.
Additional information in the [MySQL Connector/J release notes](https://dev.mysql.com/doc/relnotes/connector-j/5.1/en/news-5-1-43.html). 
- Increase default startup timeout of databases from 10 minutes to 30 minutes since some database types
could take a long time to start up on slower machines.
- Cause for startup timeout errors is now displayed in stack traces.
- Fixed issue with Oracle database creation where the system account could expire.

### 2.1
2017-10-03

- Fixed SQL Server for Linux Docker tag selection after Microsoft blew away all the existing tags and dropped `:latest`.
The database version is now translated properly into tag selection now.

### 2.0
2017-07-02

- Added first-class support for setting collation and encoding for databases that support them.
These can be set through box configuration with e.g. `<databaseCollation>case-insensitive</databaseCollation` or
through system property `db.collation`.  Most databases support the general collation types 'binary' and 
'case-insensitive' but some have support for custom values.  See
[integration tests](https://bitbucket.org/prunge/boxdb-maven-plugin/src/99ded9601e6804fe6664aaa36d8a881a66d867e1/src/it/vagrant-sqlserver-collation-ci/?at=default) for more examples. 
- Fixed auth configuration for Postgres and Derby - previously did not require login, 
now they do
- Native scripts are now supported for SQL Server for Linux - they will be run 
using the `sqlcmd` tool like SQL Server for Windows.  Previously all scripts ran through
JDBC
- Upgraded Docker plugin to 0.21.0

### 1.10
2017-05-15

- Added DB2 support
- Fixed large number of BlockReleaser threads being created when using Oracle and JDBC waiting mode was
activated by re-using classloaders

### 1.9
2017-04-25

- Now default script names include one that just has the major database version, so it will 
  look at, e.g. `[create-postgres-9.4.sql, create-postgres-9.sql, create-postgres.sql, create.sql]`
- SQL Server databases use Microsoft's JDBC driver by default now, JTDS can still be selected
  using box configuration `<jdbc.driver>jtds</jdbc.driver>` parameter
- Added `DataSourceBuilder` to make it easier to extend existing databases and change data source configuration

### 1.8
2017-04-18

- Fixed another potential NullPointerException when dumping logs with a failed container
- Upgraded Docker plugin to 0.20.1
- Added ability to skip execution with configuration/property
- Workaround for Vagrant bug #8395 (https://github.com/mitchellh/vagrant/issues/8395)
  "The requested address is not valid in its context" error with SQL Server
- Added configurable kill timeout to kill Docker databases when they don't shut down 
  gracefully within a certain amount of time

### 1.7
2017-02-14

- Added SQLite support
- Fixed status display to not display 'null/0' host/port for databases that don't have them
- Fixed crash when Vagrant container has an aborted state, will not be treated as not started
- Fixed NullPointerException when attempting to dump logs on error when the container has not started up yet
- Fixed dump logs on error to not swallow original exception if the log dumping fails as well
- Made Oracle 12 wait more resilient to socket oddities by using HTTP ping instead of TCP check, fixes waiting problems 
  on non-docker-machine versions of Windows and Mac
- Upgraded Docker plugin to 0.19.1

### 1.6.1
2017-01-17

- Fix NullPointerException bug when there is no existing docker auth configuration

### 1.6
2017-01-16

- Made database startup poll time configurable with a property
- Added Postgres 8.x support
- Fixed Postgres dump save/restore compatibility with early Postgres 9.x versions
- Added ability to dump database log files with a goal or when database startup fails
- Changed Oracle 12 database to use stable tag in Docker registry instead of latest version
- Updated default Postgres version to 9.6
- Updated default Derby/H2 versions to their latest stable versions
- Upgraded Docker plugin to 0.19.0

### 1.5
2016-11-22

- Fixed JDBC waiting workaround not working with MySQL and MariaDB
- Added SQL Server for Linux support
- Upgraded Docker plugin to 0.18.1

### 1.4
2016-10-13

- Added ability to load additional artifacts for loading box factories at runtime through system property
- Upgraded Docker plugin from 0.15.16 to 0.16.7
- Added Oracle 12 support
- Stop printing out every line of PostgreSQL text dumps

### 1.3
2016-09-07

- Added HSQLDB support
- Upgraded Docker plugin from 0.5.9 to 0.15.16
- Added JDBC waiting workaround for when socket waiting is unreliable (e.g. newest Docker/Mac)

### 1.2
2016-08-11

- Fixed incorrectly hardcoded database name in SQL Server backup/restore

### 1.1
2016-08-11

- SQLServer: install required Vagrant plugins if needed
- Fixed file-based databases now run properly in projectless environment
- Fixed stop goal to not require project
- Fixed finding containers when stopping in some cases was failing due to Docker plugin substring issues
- Added ability to set database user and password through system properties
- Changed Squirrel goal to not shut down container if it was already running before it started

### 1.0
2016-08-07

- Initial version
