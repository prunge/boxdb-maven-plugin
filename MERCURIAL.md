The boxdb-maven-plugin repository has been converted from Mercurial to
Git.

The old Mercurial repository is still available at 
<https://bitbucket.org/prunge/boxdb-maven-plugin-hg> but will
no longer be updated.  
