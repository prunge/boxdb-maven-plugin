# boxdb-maven-plugin
The Boxed Database Maven Plugin provides a way to launch and interact with different 
types of databases easily.  


- PostgreSQL (and PostGIS)
- MySQL
- MariaDB
- Oracle XE
- MS SQL Server
- IBM DB2
- Derby
- H2
- HSQL-DB
- SQLite
- CockroachDB

These databases do not need to be installed on your local machine at all - Docker and
Vagrant are used to download and launch them in isolated containers.

Typically this plugin is used to spin up a database before integration tests and then 
tear it down again afterwards.

## Setup and Requirements

#### Plugin setup

This plugin exists in Maven Central so Maven will download it when first invoked.
No separate download required.

#### File based databases

For Derby, HSQL-DB, H2 and SQLite databases, nothing else is required to be installed.  When launched,
all necessary dependencies are downloaded through Maven when needed.

#### Docker container based databases

PostgreSQL, MySQL/MariaDB, DB2, Oracle and SQL Server for Linux databases are launched using Docker containers.  
Docker must be installed to use these database types.

#### Vagrant container based databases

MS SQL Server for Windows is launched using Vagrant.  You will need Vagrant and VirtualBox installed
to use this database type.

## Usage

#### Command line

------

Start a PostgreSQL database and leave it running until control+c is pressed:

    mvn au.net.causal.maven.plugins:boxdb-maven-plugin:startwait -Ddb.type=postgres
    
If running for the first time, dependencies will be downloaded - in this case Postgres
container will be downloaded from Docker Hub.

When terminated with control+c, the container is stopped but the data remains.  To 
delete it when stopped, add the `-Ddb.deleteOnStop=true` parameter.    
    
------

Launch a specific version of a database:

    mvn au.net.causal.maven.plugins:boxdb-maven-plugin:startwait -Ddb.type=mariadb -Ddb.version=10.1
    
------

Run a UI for interacting with a database:

    mvn au.net.causal.maven.plugins:boxdb-maven-plugin:squirrel -Ddb.type=postgres
    
This will launch SquirrelSQL and you'll be able to view and interact with database 
through it.

------

Stop and delete a database that was previously executed:

    mvn au.net.causal.maven.plugins:boxdb-maven-plugin:stop -Ddb.type=postgres -Ddb.deleteOnStop=true
    
You might want to do this after your experiments.

#### As part of a Maven project

See the 
[integration tests](https://bitbucket.org/prunge/boxdb-maven-plugin/src/8970b1a52ec8549301339391dff0c3378f709183/src/it/?at=default) 
for examples of usage within Maven.

Generally use the following configuration in your project:

    <plugin>
        <groupId>au.net.causal.maven.plugins</groupId>
        <artifactId>boxdb-maven-plugin</artifactId>
        <version>3.1</version>
        <configuration>
            <!-- Define the database configuration -->
            <box>
                <databaseType>postgres</databaseType>
                
                <!-- If version is omitted, the default is used -->
                <databaseVersion>11.4</databaseVersion>
                
                <!-- Credentials to use for setting up the database - plugin will initialize the database with this user for you -->
                <!-- Can also be omitted in which case the defaults are used -->
                <databaseUser>boss</databaseUser>
                <databasePassword>galah</databasePassword>
            </box>
            <deleteOnStop>true</deleteOnStop>
        </configuration>
        <executions>
            <execution>
                <id>db-start</id>
                <goals>
                    <goal>start</goal>
                </goals>
            </execution>
            <execution>
                <id>db-stop</id>
                <goals>
                    <goal>stop</goal>
                </goals>
            </execution>
        </executions>
    </plugin>
    
This will start the database in a container before integration tests and then stop and delete it afterwards.  Use
in combination with the failsafe plugin.

#### Get a list of supported databases / versions

To list supported databases:

```
mvn au.net.causal.maven.plugins:boxdb-maven-plugin:start
```

without the `db.type` property set.  The error message from the plugin
show a list of all supported database types that can be used.  

To list supported versions for a particular database type (e.g. Postgres):

```
mvn au.net.causal.maven.plugins:boxdb-maven-plugin:versions -Ddb.type=postgres
```

When using the plugin, a particular version of a database may be
specified by using `-Ddb.version=<version>` or through POM configuration.

#### Customize Docker environment variables

For docker-based databases, [environment variables](https://docs.docker.com/reference/dockerfile/#env)
can be passed through to Docker containers with an `<environment>` element in the
`<box>` configuration.

For example, to set the timezone to Adelaide for a MySQL database:

```
<configuration>
    <box>
        <databaseType>mysql</databaseType>
        <databaseUser>boss</databaseUser>
        <databasePassword>galah</databasePassword>
        <environment>
            <TZ>Australia/Adelaide</TZ>
        </environment>
    </box>
    <deleteOnStop>true</deleteOnStop>
</configuration>
```

This can also be configured from the command line with `db.environment`, e.g.

```
mvn au.net.causal.maven.plugins:boxdb-maven-plugin:startwait -Ddb.type=mysql -Ddb.deleteOnStop -Ddb.environment=TZ=Australia/Adelaide
```

Multiple environment variable definitions on the command line can be separated by comma.

## Workarounds

#### Errors when starting more recent Docker container databases when using an old version of Docker

Some newer database images do not work with older version of Docker out of the box.  Symptoms
are permission errors in the database logs when starting up.  For example 
[on Postgres](https://github.com/docker-library/postgres/issues/884):

```
cannot access '/docker-entrypoint-initdb.d/': Operation not permitted
```

Solutions include using an older Postgres version or upgrade your Docker version, but if neither 
of these is possible you can set the `useDockerUnconfinedSecComp` property to true in project 
configuration or set the system property `boxdb.docker.useUnconfinedSecComp` to true when running
your builds.  This will disable some Docker security settings, but should allow the databases to run.
This can also be configured for all your builds by configuring the property in your 
Maven `settings.xml`.

#### Binding errors when running on old Docker platforms on Windows

As of BoxDB version 3.3, the default path binding code has changed
to support the standard Windows Docker platforms such as Docker Desktop, Rancher, and Podman.
Some old Docker platforms such as docker-machine might give errors such as
`invalid volume specification` when trying to bind host paths to Docker.

This can be fixed through configuration, with the setting
`bindPathTranslationMode` in project configuration or the system property
`boxdb.docker.bindPathTranslationMode`.  Set it to `LEGACY_WINDOWS` to 
restore old behaviour if you encounter this error.

To configure this globally, configure this property in your Maven `settings.xml`.
