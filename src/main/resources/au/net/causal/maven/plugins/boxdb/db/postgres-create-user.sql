\set ON_ERROR_STOP 1

DO
$setupDb$
BEGIN
    IF NOT EXISTS (SELECT * FROM pg_catalog.pg_user where usename = '${box.databaseUser}') THEN
        CREATE USER ${box.databaseUser} PASSWORD '${box.databasePassword}';
    ELSE
        ALTER USER ${box.databaseUser} PASSWORD '${box.databasePassword}';
    END IF;
END

$setupDb$

