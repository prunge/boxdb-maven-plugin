\set ON_ERROR_STOP 1

CREATE DATABASE ${box.databaseName}
        WITH ENCODING='${box.databaseEncoding}'
        OWNER=${box.databaseUser}
        TEMPLATE=template0
        LC_COLLATE='${box.databaseCollation}'
        LC_CTYPE='${box.databaseCollation}';
