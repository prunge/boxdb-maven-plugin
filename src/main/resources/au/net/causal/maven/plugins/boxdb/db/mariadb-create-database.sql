CREATE DATABASE IF NOT EXISTS ${box.databaseName} CHARACTER SET '${box.databaseEncoding}' COLLATE '${box.databaseCollation}';
GRANT USAGE ON *.* TO '${box.databaseUser}'@'%' IDENTIFIED BY '${box.databasePassword}';
GRANT USAGE ON *.* TO '${box.databaseUser}'@'localhost' IDENTIFIED BY '${box.databasePassword}';
GRANT ALL PRIVILEGES ON ${box.databaseName}.* TO '${box.databaseUser}'@'%';
GRANT ALL PRIVILEGES ON ${box.databaseName}.* TO '${box.databaseUser}'@'localhost';
