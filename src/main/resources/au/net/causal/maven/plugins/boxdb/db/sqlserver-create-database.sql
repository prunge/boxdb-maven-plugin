use master;
create login ${box.databaseUser} with password='${box.databasePassword}';
create database ${box.databaseName} COLLATE ${box.databaseCollation};
use ${box.databaseName};
create user ${box.databaseUser} for login ${box.databaseUser};
exec sp_addrolemember 'db_owner', '${box.databaseUser}';
