\set ON_ERROR_STOP 1

create language plpgsql;

create or replace function createOrUpdateBoxUser() returns void as $$
begin
    IF NOT EXISTS (SELECT * FROM pg_catalog.pg_user where usename = '${box.databaseUser}') THEN
        CREATE USER ${box.databaseUser} PASSWORD '${box.databasePassword}';
    ELSE
        ALTER USER ${box.databaseUser} PASSWORD '${box.databasePassword}';
    END IF;
end;
$$ LANGUAGE plpgsql;

select createOrUpdateBoxUser();

drop function if exists createOrUpdateBoxUser();
