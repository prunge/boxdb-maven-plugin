use master;
go
create login ${box.databaseUser} with password='${box.databasePassword}', check_expiration=off, check_policy=off;
go
create database ${box.databaseName} COLLATE ${box.databaseCollation};
go
use ${box.databaseName};
go
create user ${box.databaseUser} for login ${box.databaseUser};
go
exec sp_addrolemember 'db_owner', '${box.databaseUser}';
go
