#!/bin/bash
set -e

#Recreate database with 2 CPU configuration
if [ ! -f /etc/boxdb-oracle-init ]; then
    echo "BoxDB reinitializing Oracle 12 database"
	  sed -i 's|<initParam name="db_name" value=""/>|<initParam name="db_name" value=""/><initParam name="cpu_count" value="2"/>|' /u01/app/oracle/product/12.1.0/xe/assistants/dbca/templates/General_Purpose.dbc
	  rm /u01/app/oracle/oradata/xe/*
	  rmdir /u01/app/oracle/oradata/xe
	  rm /etc/oratab
	  touch /etc/oratab
    touch /etc/boxdb-oracle-init
    su oracle -c "$ORACLE_HOME/bin/dbca -silent -createDatabase -templateName General_Purpose.dbc -gdbname xe.oracle.docker -sid xe -responseFile NO_VALUE -characterSet AL32UTF8 -totalMemory $DBCA_TOTAL_MEMORY -emConfiguration LOCAL -pdbAdminPassword oracle -sysPassword oracle -systemPassword oracle"
fi

echo "BoxDB performing standard Oracle 12 startup"
/scripts/startup.sh
