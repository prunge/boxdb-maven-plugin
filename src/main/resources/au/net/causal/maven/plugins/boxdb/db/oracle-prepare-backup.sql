whenever sqlerror exit 1;
CREATE OR REPLACE DIRECTORY backup_dir AS '/data/scripts/';
GRANT READ, WRITE ON DIRECTORY backup_dir TO ${box.databaseUser};
exit;
