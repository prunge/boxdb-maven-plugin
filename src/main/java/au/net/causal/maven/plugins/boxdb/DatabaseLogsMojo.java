package au.net.causal.maven.plugins.boxdb;

import au.net.causal.maven.plugins.boxdb.db.BoxDatabase;
import au.net.causal.maven.plugins.boxdb.db.BoxDatabaseException;
import au.net.causal.maven.plugins.boxdb.db.DatabaseLog;
import au.net.causal.maven.plugins.boxdb.db.DockerService;
import io.fabric8.maven.docker.access.DockerAccessException;
import org.apache.maven.plugin.MojoExecutionException;
import org.apache.maven.plugin.MojoFailureException;
import org.apache.maven.plugins.annotations.Mojo;
import org.apache.maven.plugins.annotations.Parameter;

import java.io.IOException;
import java.io.StringWriter;
import java.util.Collections;
import java.util.List;

/**
 * Display logs from the database.
 */
@Mojo(name="logs", requiresProject = false)
public class DatabaseLogsMojo extends AbstractDatabaseMojo 
{
    /**
     * If true, display all database logs instead of just the main one.
     */
    @Parameter(property = "boxdb.allLogs")
    private boolean allLogs;
    
    @Override
    protected void executeInternal(ExceptionalSupplier<DockerService, BoxDatabaseException> service) 
    throws MojoExecutionException, MojoFailureException, DockerAccessException 
    {
        try 
        {
            BoxDatabase database = database(service);
            List<? extends DatabaseLog> dbLogs;
            if (allLogs)
                dbLogs = database.logFiles();
            else 
            {
                DatabaseLog dbLog = database.bestLogFile();
                if (dbLog != null)
                    dbLogs = Collections.singletonList(dbLog);
                else
                    dbLogs = Collections.emptyList();
            }
            if (dbLogs.isEmpty())
            {
                getLog().info("No database logs available");
                return;
            }
            
            for (DatabaseLog dbLog : dbLogs) 
            {
                getLog().info(dbLog.getName() + ":");
                StringWriter w = new StringWriter();

                dbLog.save(w);
                getLog().info(w.getBuffer());
            }
        }
        catch (BoxDatabaseException | IOException e)
        {
            throw new MojoExecutionException("Error reading logs: " + e, e);
        }
    }
}
