package au.net.causal.maven.plugins.boxdb.db;

import com.google.common.collect.ImmutableSet;
import com.google.gson.JsonArray;
import com.google.gson.JsonElement;
import com.google.gson.JsonObject;
import com.google.gson.JsonPrimitive;
import io.fabric8.maven.docker.access.DockerAccess;
import io.fabric8.maven.docker.access.DockerAccessException;
import io.fabric8.maven.docker.access.UrlBuilder;
import io.fabric8.maven.docker.access.hc.ApacheHttpClientDelegate;
import io.fabric8.maven.docker.access.hc.ApacheHttpClientDelegate.HttpBodyAndStatus;
import io.fabric8.maven.docker.access.hc.DockerAccessWithHcClient;
import io.fabric8.maven.docker.config.Arguments;
import io.fabric8.maven.docker.util.JsonFactory;
import io.fabric8.maven.docker.util.Logger;

import java.io.IOException;
import java.lang.reflect.Field;
import java.lang.reflect.Method;
import java.util.Collection;
import java.util.Set;
import java.util.StringJoiner;
import java.util.stream.Collectors;

import static java.net.HttpURLConnection.HTTP_CREATED;
import static java.net.HttpURLConnection.HTTP_NOT_FOUND;

/**
 * Terrible hacks to get additional functionality with the existing docker client from the docker maven plugin.
 */
public class DockerHacks 
{
    private static final String API_LOG_FORMAT_POST_WITH_REQUEST = "POST to %s with %s";

    /**
     * Retrieves details about a local Docker image.
     *
     * @param docker the docker client.
     * @param imageName the name of the image to get details of.
     *
     * @return details of the image, or null if the image does not exist.
     *
     * @throws DockerAccessException if an error occurs.
     */
    public ImageDetails inspectImage(DockerAccess docker, String imageName)
    throws DockerAccessException
    {
        DockerAccessWithHcClient client = (DockerAccessWithHcClient)docker;

        try
        {
            Method inspectImageMethod = DockerAccessWithHcClient.class.getDeclaredMethod("inspectImage", String.class);
            inspectImageMethod.setAccessible(true);
            HttpBodyAndStatus response = (HttpBodyAndStatus)inspectImageMethod.invoke(client, imageName);

            if (response.getStatusCode() == HTTP_NOT_FOUND)
                return null;

            JsonObject imageDetails = JsonFactory.newJsonObject(response.getBody());
            JsonPrimitive fullImageIdPrim = imageDetails.getAsJsonPrimitive("Id");
            if (fullImageIdPrim == null)
                throw new DockerAccessException("Id property not found in image");

            String fullImageId = fullImageIdPrim.getAsString();

            //Also save the digests, might not have any though
            JsonArray repoDigestsJson = imageDetails.getAsJsonArray("RepoDigests");
            Set<String> repoDigests;
            if (repoDigestsJson == null)
                repoDigests = ImmutableSet.of();
            else
            {
                ImmutableSet.Builder<String> rdb = ImmutableSet.builder();
                for (JsonElement repoDigest : repoDigestsJson)
                {
                    rdb.add(repoDigest.getAsString());
                }
                repoDigests = rdb.build();
            }

            return new ImageDetails(fullImageId, repoDigests);

        }
        catch (ReflectiveOperationException e)
        {
            throw new DockerAccessException(e, e.getMessage());
        }
    }

    public String createExecContainerWithUser(DockerAccess docker, String containerId, Arguments arguments, String user)
    throws DockerAccessException
    {
        DockerAccessWithHcClient client = (DockerAccessWithHcClient)docker;

        try
        {
            //We can reuse the URLBuilder for the other copyArchive because the only difference
            //is the HTTP method (GET vs POST)
            Field urlBuilderField = DockerAccessWithHcClient.class.getDeclaredField("urlBuilder");
            urlBuilderField.setAccessible(true);
            UrlBuilder urlBuilder = (UrlBuilder)urlBuilderField.get(client);

            Field delegateField = DockerAccessWithHcClient.class.getDeclaredField("delegate");
            delegateField.setAccessible(true);
            ApacheHttpClientDelegate delegate = (ApacheHttpClientDelegate)delegateField.get(client);

            Field logField = DockerAccessWithHcClient.class.getDeclaredField("log");
            logField.setAccessible(true);
            Logger log = (Logger)logField.get(client);

            //Following code is copied mostly from DockerAccessWithHcClient.createExecContainer()

            String url = urlBuilder.createExecContainer(containerId);
            JsonObject request = new JsonObject();
            request.addProperty("Tty", true);
            request.addProperty("AttachStdin", false);
            request.addProperty("AttachStdout", true);
            request.addProperty("AttachStderr", true);
            if (user != null) //This one added
                request.addProperty("User", user);
            request.add("Cmd", JsonFactory.newJsonArray(arguments.getExec()));

            String execJsonRequest = request.toString();
            log.verbose(Logger.LogVerboseCategory.API, API_LOG_FORMAT_POST_WITH_REQUEST, url, execJsonRequest);
            try
            {
                String response = delegate.post(url, execJsonRequest, new ApacheHttpClientDelegate.BodyResponseHandler(), HTTP_CREATED);
                JsonObject json = JsonFactory.newJsonObject(response);
                if (json.has("Warnings"))
                {
                    //logWarnings(json); //need reflection to access this, let's just not worry about it for now
                }

                return json.get("Id").getAsString();
            }
            catch (IOException e)
            {
                throw new DockerAccessException(e, "Unable to exec [%s] on container [%s]", request.toString(),
                        containerId);
            }
        }
        catch (ReflectiveOperationException e)
        {
            throw new DockerAccessException(e, e.getMessage());
        }
    }

    public static class ImageDetails
    {
        private final String id;
        private final Collection<String> repoDigests;

        public ImageDetails(String id, Collection<String> repoDigests)
        {
            this.id = id;
            this.repoDigests = ImmutableSet.copyOf(repoDigests);
        }

        /**
         * @return the image ID, including the digest algorithm.  e.g. sha256:123456.
         */
        public String getId()
        {
            return id;
        }

        /**
         * @return full repo digests, including the reponame@ prefix.  e.g. 'postgres@sha256:123456'.
         */
        public Collection<String> getRepoDigests()
        {
            return repoDigests;
        }

        /**
         * @return repo digests without the 'reponame@' prefix.  Returned strings should just contain algorithm:hash,
         * e.g. 'sha256:123456'.
         */
        public Collection<String> getUnprefixedRepoDigests()
        {
            return getRepoDigests().stream()
                                    .map(ImageDetails::removeRepoDigestRepositoryPrefix)
                                    .collect(Collectors.toSet());
        }

        private static String removeRepoDigestRepositoryPrefix(String fullRepoDigest)
        {
            int separatorIndex = fullRepoDigest.indexOf('@');
            if (separatorIndex < 0)
                return fullRepoDigest;

            return fullRepoDigest.substring(separatorIndex + 1);
        }

        @Override
        public String toString()
        {
            return new StringJoiner(", ", ImageDetails.class.getSimpleName() + "[", "]")
                    .add("id=" + id)
                    .add("repoDigests=" + repoDigests)
                    .toString();
        }
    }
}
