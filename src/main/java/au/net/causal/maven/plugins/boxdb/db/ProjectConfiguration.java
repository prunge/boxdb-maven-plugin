package au.net.causal.maven.plugins.boxdb.db;

import io.fabric8.maven.docker.util.GavLabel;
import org.apache.maven.project.MavenProject;
import org.apache.maven.settings.Settings;

import java.io.File;
import java.time.Duration;
import java.util.Properties;

/**
 * Project and global configuration that is not specific database configuration.
 */
public class ProjectConfiguration
{
    private final MavenProject project;
    private final Settings settings;
    private final Duration scriptTimeout;
    private final Duration backupTimeout;
    private final Duration pollTime;
    private final Duration killTimeout;
    private final boolean dockerUseUnconfinedSecComp;
    private final DockerBindPathTranslationMode dockerBindPathTranslationMode;

    /**
     * @param project the Maven project.
     * @param settings Maven global settings.
     * @param scriptTimeout timeout for executing scripts.
     * @param backupTimeout timeout for running backups and restores.
     * @param pollTime time to wait between polling for checking database operations, executions, etc.
     * @param killTimeout time to wait for graceful database stop before doing a forced kill.
     * @param dockerUseUnconfinedSecComp if true, use unconfined seccomp security setting for Docker containers.
     * @param dockerBindPathTranslationMode used to translate Docker bind paths.
     */
    public ProjectConfiguration(MavenProject project, Settings settings, Duration scriptTimeout, Duration backupTimeout, 
                                Duration pollTime, Duration killTimeout, boolean dockerUseUnconfinedSecComp,
                                DockerBindPathTranslationMode dockerBindPathTranslationMode)
    {
        this.project = project;
        this.settings = settings;
        this.scriptTimeout = scriptTimeout;
        this.backupTimeout = backupTimeout;
        this.pollTime = pollTime;
        this.killTimeout = killTimeout;
        this.dockerUseUnconfinedSecComp = dockerUseUnconfinedSecComp;
        this.dockerBindPathTranslationMode = dockerBindPathTranslationMode;
    }

    /**
     * @return Maven project configuration.  Returns null if there is no project.
     */
    public MavenProject getProject()
    {
        return project;
    }

    /**
     * @return Maven global configuration.
     */
    public Settings getSettings()
    {
        return settings;
    }

    /**
     * @return timeout hint for executing SQL scripts.  May not be honoured by all database types.
     */
    public Duration getScriptTimeout()
    {
        return scriptTimeout;
    }

    /**
     * @return timeout hint for running database backups and restores.  May not be honoured by all database types.
     */
    public Duration getBackupTimeout()
    {
        return backupTimeout;
    }

    /**
     * @return Maven project properties.  Returns null if there is no project.
     */
    public Properties getProjectProperties()
    {
        if (getProject() == null)
            return null;

        return getProject().getProperties();
    }

    /**
     * @return POM label.
     */
    public GavLabel getPomLabel()
    {
        return new GavLabel(getProject().getGroupId(), getProject().getArtifactId(), getProject().getVersion());
    }

    /**
     * @return time to wait between polling for checking database operations, executions, etc.
     */
    public Duration getPollTime() 
    {
        return pollTime;
    }

    /**
     * @return time to wait for graceful database stop before doing a forced kill.
     */
    public Duration getKillTimeout()
    {
        return killTimeout;
    }

    /**
     * @return the project base directory.
     */
    public File getBaseDirectory()
    {
        File baseDirectory = null;
        if (getProject() != null)
            baseDirectory = getProject().getBasedir();

        if (baseDirectory == null)
            baseDirectory = new File(".");

        return baseDirectory;
    }

    /**
     * @return true to use unconfined seccomp setting for Docker.
     */
    public boolean isDockerUseUnconfinedSecComp()
    {
        return dockerUseUnconfinedSecComp;
    }

    /**
     * @return the docker bind path translator that should be used for generating bind paths.
     *
     * @since 3.3
     */
    public DockerBindPathTranslationMode getDockerBindPathTranslationMode()
    {
        return dockerBindPathTranslationMode;
    }
}
