package au.net.causal.maven.plugins.boxdb;

/**
 * Controls which database logs, if any, are dumped to user output when a boxed database fails to start.
 */
public enum DumpLogsOnErrorOption 
{
    /**
     * Do not dump logs.
     */
    OFF,

    /**
     * Dump the main log only if it exists.
     */
    MAIN,

    /**
     * Dump all log files.
     */
    ALL
}