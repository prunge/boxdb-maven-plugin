package au.net.causal.maven.plugins.boxdb.db;

import au.net.causal.maven.plugins.boxdb.Versions;

import java.io.File;
import java.nio.file.Path;
import java.util.ArrayList;
import java.util.LinkedHashMap;
import java.util.List;
import java.util.Locale;
import java.util.Map;
import java.util.Properties;
import java.util.stream.Collectors;

/**
 * Configures a boxed database.
 * <p>
 *
 * Depending on the {@linkplain #setDatabaseType(String) database type}, some settings may be overridden.
 * For example, some container databases might not allow the port to be changed.
 */
public class BoxConfiguration
{
    private String databaseType;
    private String databaseVersion;
    private int databasePort;
    private String adminUser;
    private String adminPassword;
    private String databaseName;
    private String databaseUser;
    private String databasePassword;
    private String containerName;
    private Path databaseFile;
    private Path restoreFile;
    private boolean initializeDatabase = true;
    private String databaseCollation;
    private String databaseEncoding;
    private boolean updateImage;
    private final List<ScriptExecution> scriptExecutions = new ArrayList<>();
    private final Map<String, String> configuration = new LinkedHashMap<>();
    private final Map<String, String> environment = new LinkedHashMap<>();

    /**
     * The password for the admin or system database, used typically to create additional databases
     * and users.
     *
     * @see #getAdminUser()
     */
    public String getAdminPassword()
    {
        return adminPassword;
    }

    public void setAdminPassword(String adminPassword)
    {
        this.adminPassword = adminPassword;
    }

    /**
     * The name of the admin or system user, used typically to create additional databases and users.
     *
     * @see #getAdminPassword()
     */
    public String getAdminUser()
    {
        return adminUser;
    }

    public void setAdminUser(String adminUser)
    {
        this.adminUser = adminUser;
    }

    /**
     * The name of the database container.  Depending on the database type, this could become the name
     * of a docker container or the name of a directory containing a file-based database.
     */
    public String getContainerName()
    {
        return containerName;
    }

    public void setContainerName(String containerName)
    {
        this.containerName = containerName;
    }

    /**
     * The database port that will be exposed on the host system.  This is not necessarily the port the
     * database runs on inside the container.
     */
    public int getDatabasePort()
    {
        return databasePort;
    }

    public void setDatabasePort(int databasePort)
    {
        this.databasePort = databasePort;
    }

    /**
     * The database type.  For example, 'postgres', 'oracle'.  Required.
     */
    public String getDatabaseType()
    {
        return databaseType;
    }

    public void setDatabaseType(String databaseType)
    {
        this.databaseType = databaseType;
    }

    /**
     * The version of the database to use.  If not specified, a default version will be used.
     */
    public String getDatabaseVersion()
    {
        return databaseVersion;
    }

    public void setDatabaseVersion(String databaseVersion)
    {
        this.databaseVersion = databaseVersion;
    }

    /**
     * The name of the target database to create.  When creating the boxed database, this database is created, owned
     * by the {@linkplain #getDatabaseUser() database user}.
     * <p>
     *
     * Typically this database is created with a <code>CREATE DATABASE</code> SQL statement.
     */
    public String getDatabaseName()
    {
        return databaseName;
    }

    public void setDatabaseName(String databaseName)
    {
        this.databaseName = databaseName;
    }

    /**
     * The password to use for the user that will own the target database.  This user will typically only
     * have access to the target database - for system access use the {@linkplain #getAdminUser() admin user}.
     *
     * @see #getDatabaseUser()
     */
    public String getDatabasePassword()
    {
        return databasePassword;
    }

    public void setDatabasePassword(String databasePassword)
    {
        this.databasePassword = databasePassword;
    }

    /**
     * The name of the user that will own the target database.  This user will typically only
     * have access to the target database - for system access use the {@linkplain #getAdminUser() admin user}.
     * This user is created when setting up the database.
     */
    public String getDatabaseUser()
    {
        return databaseUser;
    }

    public void setDatabaseUser(String databaseUser)
    {
        this.databaseUser = databaseUser;
    }

    /**
     * SQL scripts to execute when setting up the database.
     */
    public List<ScriptExecution> getScriptExecutions()
    {
        return scriptExecutions;
    }

    public void setScriptExecutions(List<ScriptExecution> scriptExecutions)
    {
        this.scriptExecutions.clear();
        this.scriptExecutions.addAll(scriptExecutions);
    }

    /**
     * Returns all the script executions to execute at a particular state.
     *
     * @param stage the stage.
     */
    public List<ScriptExecution> scriptExecutions(DatabaseStage stage)
    {
        return getScriptExecutions().stream()
                .filter(execution -> execution.getStage() == stage)
                .collect(Collectors.toList());
    }

    /**
     * @return true if the database will be setup by the plugin, false if all setup should be performed by user scripts.
     */
    public boolean isInitializeDatabase()
    {
        return initializeDatabase;
    }

    public void setInitializeDatabase(boolean initializeDatabase)
    {
        this.initializeDatabase = initializeDatabase;
    }

    /**
     * Returns whether the database image and binaries are checked for updates in remote repositories.
     * Some database images, such as Docker images, may be updated remotely.  When this option is turned on, the plugin
     * will check for updates and download them if available.
     * <p>
     *
     * Defaults to false.
     *
     * @return true if images should be checked for updates in remotes and updated when needed, false to only download
     *         images when they do not exist locally.
     *
     * @since 3.0
     */
    public boolean isUpdateImage()
    {
        return updateImage;
    }

    public void setUpdateImage(boolean updateImage)
    {
        this.updateImage = updateImage;
    }

    /**
     * @return additional database-specific configuration.
     */
    public Map<String, String> getConfiguration()
    {
        return configuration;
    }

    public void setConfiguration(Map<String, String> configuration)
    {
        this.configuration.clear();
        this.configuration.putAll(configuration);
    }

    /**
     * @return additional environment configuration.  For docker databases, this configures additional environment variables with ENV.
     *
     * @since 3.3
     */
    public Map<String, String> getEnvironment()
    {
        return environment;
    }

    public void setEnvironment(Map<String, String> environment)
    {
        this.environment.clear();
        this.environment.putAll(environment);
    }

    /**
     * @return the file or directory to use for file-based databases.  Ignored for other types of databases.
     */
    public Path getDatabaseFile()
    {
        return databaseFile;
    }

    public void setDatabaseFile(Path databaseFile)
    {
        this.databaseFile = databaseFile;
    }

    public Path getRestorePath()
    {
        return restoreFile;
    }

    /**
     * If specified, restores the database from a backup file.
     */
    public void setRestoreFile(File restoreFile)
    {
        this.restoreFile = restoreFile.toPath();
    }

    /**
     * @return the name of the collation new databases will be configured with.
     * 
     * @since 2.0
     * 
     * @see CommonCollation
     */
    public String getDatabaseCollation() 
    {
        return databaseCollation;
    }

    public void setDatabaseCollation(String databaseCollation) 
    {
        this.databaseCollation = databaseCollation;
    }

    /**
     * @return the name of the encoding to use for database string data.
     * 
     * @since 2.0
     * 
     * @see CommonEncoding
     */
    public String getDatabaseEncoding() 
    {
        return databaseEncoding;
    }

    public void setDatabaseEncoding(String databaseEncoding) 
    {
        this.databaseEncoding = databaseEncoding;
    }

    /**
     * Saves configuration properties to a properties object.
     *
     * @param properties the properties object to save properties to.
     */
    public void toProperties(Properties properties)
    {
        if (getDatabaseName() != null)
        {
            properties.setProperty("box.databaseName", getDatabaseName());
            properties.setProperty("box.databaseName.upperCase", getDatabaseName().toUpperCase(Locale.ENGLISH));
        }
        if (getDatabaseUser() != null)
        {
            properties.setProperty("box.databaseUser", getDatabaseUser());
            properties.setProperty("box.databaseUser.upperCase", getDatabaseUser().toUpperCase(Locale.ENGLISH));
        }
        if (getDatabasePassword() != null)
            properties.setProperty("box.databasePassword", getDatabasePassword());
        if (getAdminUser() != null)
        {
            properties.setProperty("box.adminUser", getAdminUser());
            properties.setProperty("box.adminUser.upperCase", getAdminUser().toUpperCase(Locale.ENGLISH));
        }
        if (getAdminPassword() != null)
            properties.setProperty("box.adminPassword", getAdminPassword());
        if (getDatabaseVersion() != null)
        {
            properties.setProperty("box.databaseVersion", getDatabaseVersion());
            properties.setProperty("box.databaseVersion.major", Versions.majorVersion(getDatabaseVersion()));
        }
        if (getDatabaseFile() != null)
            properties.setProperty("box.databaseFile", getDatabaseFile().toAbsolutePath().toString());

        properties.setProperty("box.databasePort", String.valueOf(getDatabasePort()));
        
        if (getDatabaseCollation() != null)
            properties.setProperty("box.databaseCollation", getDatabaseCollation());
        if (getDatabaseEncoding() != null)
            properties.setProperty("box.databaseEncoding", getDatabaseEncoding());

        configuration.forEach((k, v) -> properties.setProperty("box.configuration." + k, v));
        environment.forEach((k, v) -> properties.setProperty("box.configuration.environment." + k, v));
    }
}
