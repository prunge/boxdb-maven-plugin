package au.net.causal.maven.plugins.boxdb.db;

import java.util.Arrays;

@BoxDbBundled
public class OracleVersionSwitchingFactory extends VersionRangeSwitchingDatabaseFactory
{
    public OracleVersionSwitchingFactory()
    {
        super("oracle", OracleFactory::new, OracleFreeFactory::new, Arrays.asList(
                new Specifier("[11,12)", OracleFactory::new, "11", "11g"),
                new Specifier("[12,13)", Oracle12Factory::new, "12", "12c"),
                new Specifier("[18,23)", OracleModernFactory::new),
                new Specifier("[23,)", OracleFreeFactory::new)
        ));
    }
}
