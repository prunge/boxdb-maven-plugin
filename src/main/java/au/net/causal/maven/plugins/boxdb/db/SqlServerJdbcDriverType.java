package au.net.causal.maven.plugins.boxdb.db;

import java.util.Locale;

public enum SqlServerJdbcDriverType
{
    MICROSOFT
    {
        @Override
        public JdbcDriverInfo jdbcDriver()
        {
            return new JdbcDriverInfo(
                    new RunnerDependency("com.microsoft.sqlserver", "mssql-jdbc", "12.6.3.jre8"),
                    "com.microsoft.sqlserver.jdbc.SQLServerDriver");

        }

        @Override
        public void configureDataSourceBuilder(DataSourceBuilder dataSourceBuilder, DatabaseTarget target, BoxConfiguration box, String host)
        {
            String databaseName;
            if (target == DatabaseTarget.ADMIN)
                databaseName = "master";
            else
                databaseName = box.getDatabaseName();

            JdbcConnectionInfo jdbcInfo = jdbcConnectionInfo(target, box, host);
            dataSourceBuilder.dataSourceClassName("com.microsoft.sqlserver.jdbc.SQLServerDataSource")
                             .dependencies(jdbcDriver().getDependencies())
                             .configureDataSource("setServerName", String.class, jdbcInfo.getHost())
                             .configureDataSource("setPortNumber", int.class, jdbcInfo.getPort())
                             .configureDataSource("setDatabaseName", String.class, databaseName)
                             .configureDataSource("setTrustServerCertificate", boolean.class, true)
                             .configureDataSource("setUser", String.class, jdbcInfo.getUser())
                             .configureDataSource("setPassword", String.class, jdbcInfo.getPassword());
        }

        @Override
        public JdbcConnectionInfo jdbcConnectionInfo(DatabaseTarget target, BoxConfiguration box, String host)
        {
            return new JdbcConnectionInfo("jdbc:sqlserver://" + host + ":" + box.getDatabasePort() + ";databaseName=" + box.getDatabaseName() + ";trustServerCertificate=true",
                                          target.user(box),
                                          target.password(box),
                                          host,
                                          1433);
        }
    }, 
    
    JTDS
    {
        @Override
        public JdbcDriverInfo jdbcDriver()
        {
            return new JdbcDriverInfo(
                    new RunnerDependency("net.sourceforge.jtds", "jtds", "1.3.1"),
                    "net.sourceforge.jtds.jdbc.Driver");
        }

        @Override
        public JdbcConnectionInfo jdbcConnectionInfo(DatabaseTarget target, BoxConfiguration box, String host)
        {
            return new JdbcConnectionInfo("jdbc:jtds:sqlserver://" + host + ":" + box.getDatabasePort() + "/" + box.getDatabaseName(),
                                          target.user(box),
                                          target.password(box),
                                          host,
                                          1433);
        }

        @Override
        public void configureDataSourceBuilder(DataSourceBuilder dataSourceBuilder, DatabaseTarget target, BoxConfiguration box, String host)
        {
            String databaseName;
            if (target == DatabaseTarget.ADMIN)
                databaseName = "master";
            else
                databaseName = box.getDatabaseName();

            JdbcConnectionInfo jdbcInfo = jdbcConnectionInfo(target, box, host);
            dataSourceBuilder.dataSourceClassName("net.sourceforge.jtds.jdbcx.JtdsDataSource")
                                .dependencies(jdbcDriver().getDependencies())
                                .configureDataSource("setServerName", String.class, jdbcInfo.getHost())
                                .configureDataSource("setPortNumber", int.class, jdbcInfo.getPort())
                                .configureDataSource("setDatabaseName", String.class, databaseName)
                                .configureDataSource("setUser", String.class, jdbcInfo.getUser())
                                .configureDataSource("setPassword", String.class, jdbcInfo.getPassword());
        }
    };
    
    public static final String CONFIGURATION_PROPERTY = "jdbc.driver";
    
    public abstract JdbcDriverInfo jdbcDriver();
    public abstract JdbcConnectionInfo jdbcConnectionInfo(DatabaseTarget target, BoxConfiguration box, String host);
    public abstract void configureDataSourceBuilder(DataSourceBuilder dataSourceBuilder, DatabaseTarget target, BoxConfiguration box, String host);
    
    public static SqlServerJdbcDriverType fromBoxConfiguration(BoxConfiguration box)
    {
        String typeString = box.getConfiguration().get(CONFIGURATION_PROPERTY);
        if (typeString == null)
            return null;
        
        return SqlServerJdbcDriverType.valueOf(typeString.trim().toUpperCase(Locale.ENGLISH));
    }
    
}
