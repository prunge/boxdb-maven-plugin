package au.net.causal.maven.plugins.boxdb.db;

import java.io.IOException;
import java.io.Writer;

/**
 * Represents a log file for a database that contains diagnostic and startup information in text format.
 * 
 * @since 1.6
 */
public interface DatabaseLog 
{
    /**
     * @return the name of the log file.  Log file names are database specific.
     * 
     * @throws BoxDatabaseException if an error occurs.
     */
    public String getName()
    throws BoxDatabaseException;

    /**
     * Retrieves contents of the log file, saving it to a writer.
     * 
     * @param w where to write the log file contents.
     *          
     * @throws BoxDatabaseException if an error occurs communicating with the database or container.
     * @throws IOException if an I/O error occurs.
     */
    public void save(Writer w)
    throws BoxDatabaseException, IOException;
}
