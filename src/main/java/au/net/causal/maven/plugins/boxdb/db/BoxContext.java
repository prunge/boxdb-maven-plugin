package au.net.causal.maven.plugins.boxdb.db;

import au.net.causal.maven.plugins.boxdb.ClassLoaderCache;
import au.net.causal.maven.plugins.boxdb.ExceptionalSupplier;
import au.net.causal.maven.plugins.boxdb.JavaRunner;
import au.net.causal.maven.plugins.boxdb.ScriptReaderRunner;
import au.net.causal.maven.plugins.boxdb.ScriptRunner;
import io.fabric8.maven.docker.config.RegistryAuthConfiguration;
import io.fabric8.maven.docker.log.LogOutputSpecFactory;
import io.fabric8.maven.docker.service.ImagePullManager;
import io.fabric8.maven.docker.service.ServiceHub;
import io.fabric8.maven.docker.util.AuthConfigFactory;
import io.fabric8.maven.docker.util.Logger;
import org.apache.maven.execution.MavenSession;
import org.apache.maven.plugin.logging.Log;
import org.apache.maven.shared.filtering.MavenReaderFilter;
import org.apache.maven.shared.filtering.MavenResourcesFiltering;
import org.codehaus.plexus.archiver.manager.ArchiverManager;
import org.eclipse.aether.RepositorySystem;
import org.eclipse.aether.RepositorySystemSession;
import org.eclipse.aether.repository.RemoteRepository;
import org.eclipse.aether.resolution.DependencyResolutionException;

import java.io.IOException;
import java.nio.file.Files;
import java.nio.file.Path;
import java.util.ArrayList;
import java.util.Arrays;
import java.util.List;
import java.util.Objects;

public class BoxContext
{
    private final ExceptionalSupplier<DockerService, BoxDatabaseException> dockerService;
    private final AuthConfigFactory authConfigFactory;
    private final Path tempDirectory;
    private final Path globalConfigDirectory;
    private final Log log;
    private final LogOutputSpecFactory logSpecFactory;
    private final MavenSession session;
    private final MavenResourcesFiltering resourcesFiltering;
    private final MavenReaderFilter readerFilter;
    private final RepositorySystem repositorySystem;
    private final RepositorySystemSession repositorySystemSession;
    private final List<RemoteRepository> remoteRepositories;
    private final ArchiverManager archiverManager;
    private final ClassLoaderCache classLoaderCache;

    /**
     * Image pull manager that always updates if the remote image is newer than the local one.
     */
    private final ImagePullManager updatingImagePullManager;

    /**
     * Image pull manager that does not update from the remote if the remote image is newer than the local one.
     */
    private final ImagePullManager nonUpdatingImagePullManager;

    private final RegistryAuthConfiguration dockerAuthConfiguration;
    private final boolean dockerSkipExtendedAuth;

    private final Logger dockerLogger;

    //Toolchain, etc. goes here

    public BoxContext(ExceptionalSupplier<DockerService, BoxDatabaseException> dockerService, AuthConfigFactory authConfigFactory,
                      Path tempDirectory, Path globalConfigDirectory, Log log,
                      LogOutputSpecFactory logSpecFactory,
                      MavenSession session, MavenResourcesFiltering resourcesFiltering,
                      MavenReaderFilter readerFilter, RepositorySystem repositorySystem,
                      RepositorySystemSession repositorySystemSession, List<RemoteRepository> remoteRepositories,
                      ArchiverManager archiverManager, ClassLoaderCache classLoaderCache,
                      ImagePullManager updatingImagePullManager, ImagePullManager nonUpdatingImagePullManager,
                      RegistryAuthConfiguration dockerAuthConfiguration,
                      boolean dockerSkipExtendedAuth,
                      Logger dockerLogger)
    {
        Objects.requireNonNull(dockerService, "dockerService == null");
        Objects.requireNonNull(authConfigFactory, "authConfigFactory == null");
        Objects.requireNonNull(tempDirectory, "tempDirectory == null");
        Objects.requireNonNull(globalConfigDirectory, "globalConfigDirectory == null");
        Objects.requireNonNull(log, "log == null");
        Objects.requireNonNull(logSpecFactory, "logSpecFactory == null");
        Objects.requireNonNull(session, "session == null");
        Objects.requireNonNull(resourcesFiltering, "resourcesFiltering == null");
        Objects.requireNonNull(readerFilter, "readerFilter == null");
        Objects.requireNonNull(repositorySystem, "repositorySystem == null");
        Objects.requireNonNull(repositorySystemSession, "repositorySystemSession == null");
        Objects.requireNonNull(remoteRepositories, "remoteRepositories == null");
        Objects.requireNonNull(archiverManager, "archiverManager == null");
        Objects.requireNonNull(classLoaderCache, "clasLoaderCache == null");
        Objects.requireNonNull(updatingImagePullManager, "updatingImagePullManager == null");
        Objects.requireNonNull(nonUpdatingImagePullManager, "nonUpdatingImagePullManager == null");
        Objects.requireNonNull(dockerLogger, "dockerLogger == null");

        this.dockerService = dockerService;
        this.authConfigFactory = authConfigFactory;
        this.tempDirectory = tempDirectory;
        this.globalConfigDirectory = globalConfigDirectory;
        this.log = log;
        this.logSpecFactory = logSpecFactory;
        this.session = session;
        this.resourcesFiltering = resourcesFiltering;
        this.readerFilter = readerFilter;
        this.repositorySystem = repositorySystem;
        this.repositorySystemSession = repositorySystemSession;
        this.remoteRepositories = remoteRepositories;
        this.archiverManager = archiverManager;
        this.classLoaderCache = classLoaderCache;
        this.updatingImagePullManager = updatingImagePullManager;
        this.nonUpdatingImagePullManager = nonUpdatingImagePullManager;
        this.dockerAuthConfiguration = dockerAuthConfiguration;
        this.dockerSkipExtendedAuth = dockerSkipExtendedAuth;
        this.dockerLogger = dockerLogger;
    }

    public ServiceHub getDockerServiceHub()
    throws BoxDatabaseException
    {
        return dockerService.get().getServiceHub();
    }

    /**
     * @return the network address of the Docker machine which should be used for accessing network resources from
     *          Docker instances.
     */
    public String getDockerHostAddress()
    throws BoxDatabaseException
    {
        return dockerService.get().getDockerHostAddress();
    }

    public AuthConfigFactory getAuthConfigFactory()
    {
        return authConfigFactory;
    }

    public Path getGlobalConfigDirectory()
    throws IOException
    {
        if (!Files.exists(globalConfigDirectory))
            Files.createDirectories(globalConfigDirectory);

        return globalConfigDirectory;
    }

    /**
     * @return a directory for the current build that can be used for temporary files.
     *
     * @throws IOException if an I/O error occurs.
     */
    public Path getTempDirectory()
    throws IOException
    {
        if (!Files.exists(tempDirectory))
            Files.createDirectories(tempDirectory);

        return tempDirectory;
    }

    public Log getLog()
    {
        return log;
    }

    public LogOutputSpecFactory getLogSpecFactory()
    {
        return logSpecFactory;
    }

    public MavenSession getSession()
    {
        return session;
    }

    public RepositorySystem getRepositorySystem()
    {
        return repositorySystem;
    }

    public RepositorySystemSession getRepositorySystemSession()
    {
        return repositorySystemSession;
    }

    public List<RemoteRepository> getRemoteRepositories()
    {
        return remoteRepositories;
    }

    public ScriptRunner createScriptRunner(BoxDatabase boxDatabase, BoxConfiguration boxConfiguration, ProjectConfiguration projectConfiguration)
    {
        return new ScriptRunner(boxDatabase, boxConfiguration, projectConfiguration, this, resourcesFiltering);
    }

    public ScriptReaderRunner createScriptReaderRunner(BoxDatabase boxDatabase, BoxConfiguration boxConfiguration, ProjectConfiguration projectConfiguration)
    {
        return new ScriptReaderRunner(boxDatabase, boxConfiguration, projectConfiguration, this, readerFilter);
    }

    public JavaRunner createJavaRunner(String mainClassName, List<? extends RunnerDependency> dependencies)
    throws DependencyResolutionException
    {
        return JavaRunner.createFromDependencies(mainClassName, dependencies, repositorySystem, 
                            repositorySystemSession, remoteRepositories, classLoaderCache);
    }

    public JavaRunner createJavaRunner(String mainClassName, RunnerDependency dependency, RunnerDependency... otherDependencies)
    throws DependencyResolutionException
    {
        List<RunnerDependency> dependencies = new ArrayList<>(otherDependencies.length + 1);
        dependencies.add(dependency);
        dependencies.addAll(Arrays.asList(otherDependencies));
        return createJavaRunner(mainClassName, dependencies);
    }

    public ArchiverManager getArchiverManager()
    {
        return archiverManager;
    }

    /**
     * Returns a Docker image pull manager that can be used to retrieve/update images for a box database.
     *
     * @param boxConfiguration the box database configuration to retrieve images for.
     *
     * @return a pull manager for retrieving and updating Docker images.
     */
    public ImagePullManager imagePullManager(BoxConfiguration boxConfiguration)
    {
        if (boxConfiguration.isUpdateImage())
            return updatingImagePullManager;
        else
            return nonUpdatingImagePullManager;
    }

    public RegistryAuthConfiguration getDockerAuthConfiguration()
    {
        return dockerAuthConfiguration;
    }

    public boolean isDockerSkipExtendedAuth()
    {
        return dockerSkipExtendedAuth;
    }

    public Logger getDockerLogger()
    {
        return dockerLogger;
    }
}
