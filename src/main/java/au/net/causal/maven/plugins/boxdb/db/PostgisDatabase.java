package au.net.causal.maven.plugins.boxdb.db;

import au.net.causal.maven.plugins.boxdb.ImageCheckerUtils;
import com.google.common.collect.ImmutableList;
import com.google.common.collect.ImmutableMap;
import io.fabric8.maven.docker.config.Arguments;
import io.fabric8.maven.docker.config.RunImageConfiguration;
import io.fabric8.maven.docker.config.RunImageConfiguration.Builder;

import java.util.Collection;
import java.util.List;

public class PostgisDatabase extends PostgresDatabase
{
    public PostgisDatabase(BoxConfiguration boxConfiguration,
                           ProjectConfiguration projectConfiguration,
                           BoxContext context, DockerRegistry dockerRegistry)
    {
        super(boxConfiguration, projectConfiguration, context, dockerRegistry);
    }

    @Override
    public Collection<? extends ImageComponent> checkImage()
    throws BoxDatabaseException
    {
        ImageComponent jdbcDriverComponent = ImageCheckerUtils.checkImageUsingMavenDependencies("JDBC driver",
                                                                                                getContext(),
                                                                                                jdbcDriverInfo().getDependencies());
        ImageComponent dockerDatabaseComponent = checkDockerDatabaseImage(PostgisFactory.POSTGIS_DOCKER_REPOSITORY);

        return ImmutableList.of(jdbcDriverComponent, dockerDatabaseComponent);
    }

    @Override
    protected String dockerImageName()
    {
        return PostgisFactory.POSTGIS_DOCKER_REPOSITORY + ":" +  getBoxConfiguration().getDatabaseVersion();
    }

    @Override
    protected void configureToolsRunImage(Builder builder)
    {
        super.configureToolsRunImage(builder);

        //Override the entrypoint of Postgis image - since that one actually starts the database which fails if it
        //hasn't been configured properly.  We just want to run psql in a shell.
        builder.entrypoint(new Arguments(ImmutableList.of("")));
    }

    @Override
    protected void configureRunImage(RunImageConfiguration.Builder builder)
    {
        super.configureRunImage(builder);
        appendEnvToRunImage(builder, ImmutableMap.of("POSTGRES_PASS", getBoxConfiguration().getAdminPassword(),
                                                     "POSTGRES_USER", getBoxConfiguration().getAdminUser(),
                                                     "ALLOW_IP_RANGE", "0.0.0.0/0"));
    }

    @Override
    public JdbcDriverInfo jdbcDriverInfo()
    throws BoxDatabaseException
    {
        //Add PostGIS artifacts to the existing Postgres ones
        JdbcDriverInfo postgresJdbc = super.jdbcDriverInfo();

        List<RunnerDependency> dependencies = ImmutableList.<RunnerDependency>builder()
                                                           .addAll(postgresJdbc.getDependencies())
                                                           .add(new RunnerDependency("net.postgis", "postgis-jdbc", "2023.1.0"))
                                                           .build();

        return new JdbcDriverInfo(dependencies, postgresJdbc.getDriverClassName(), postgresJdbc.getDriverClassName());
    }
}
