package au.net.causal.maven.plugins.boxdb.db;

/**
 * A hint for the format of making database backup files.
 * <p>
 *
 * Not all database types may support these hints, and they may be ignored.
 */
public enum BackupFileTypeHint
{
    /**
     * Use a compact, typically binary format that uses less space.
     */
    COMPACT,

    /**
     * Use a text format that is more suitable for checking into source control and diffing.
     */
    TEXT
}
