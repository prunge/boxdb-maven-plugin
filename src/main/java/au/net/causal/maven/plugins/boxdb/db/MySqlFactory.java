package au.net.causal.maven.plugins.boxdb.db;

@BoxDbBundled
public class MySqlFactory extends BaseMySqlFactory
{
    public MySqlFactory()
    {
        super("mysql");
    }

    @Override
    protected void initializeDefaults(BoxConfiguration boxConfiguration)
    {
        if (boxConfiguration.getDatabaseVersion() == null)
            boxConfiguration.setDatabaseVersion("8.0.28"); //Latest version that does not have compatibility issues with old Docker versions

        super.initializeDefaults(boxConfiguration);
    }

    @Override
    protected MySqlDatabase createDockerDatabase(BoxConfiguration boxConfiguration, ProjectConfiguration projectConfiguration, BoxContext context)
    throws BoxDatabaseException
    {
        return new MySqlDatabase(boxConfiguration, projectConfiguration, context, dockerRegistry(), dockerRepositoryName());
    }
}
