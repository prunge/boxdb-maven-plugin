package au.net.causal.maven.plugins.boxdb;

import au.net.causal.maven.plugins.boxdb.db.BoxDatabaseException;
import au.net.causal.maven.plugins.boxdb.db.RunnerDependency;
import org.eclipse.aether.RepositorySystem;
import org.eclipse.aether.RepositorySystemSession;
import org.eclipse.aether.artifact.Artifact;
import org.eclipse.aether.artifact.DefaultArtifact;
import org.eclipse.aether.collection.CollectRequest;
import org.eclipse.aether.graph.Dependency;
import org.eclipse.aether.repository.RemoteRepository;
import org.eclipse.aether.resolution.ArtifactDescriptorException;
import org.eclipse.aether.resolution.ArtifactDescriptorRequest;
import org.eclipse.aether.resolution.ArtifactDescriptorResult;
import org.eclipse.aether.resolution.ArtifactResult;
import org.eclipse.aether.resolution.DependencyRequest;
import org.eclipse.aether.resolution.DependencyResolutionException;
import org.eclipse.aether.resolution.DependencyResult;
import org.eclipse.aether.resolution.VersionRangeRequest;
import org.eclipse.aether.resolution.VersionRangeResolutionException;
import org.eclipse.aether.resolution.VersionRangeResult;
import org.eclipse.aether.resolution.VersionRequest;
import org.eclipse.aether.resolution.VersionResolutionException;
import org.eclipse.aether.resolution.VersionResult;
import org.eclipse.aether.version.Version;

import java.io.File;
import java.nio.file.Path;
import java.util.ArrayList;
import java.util.Collection;
import java.util.Collections;
import java.util.List;
import java.util.stream.Collectors;

public class DependencyUtils
{
    public static List<Path> resolveDependenciesFromStrings(List<String> dependencySpecs,
                                                            RepositorySystem repositorySystem,
                                                            RepositorySystemSession repositorySystemSession,
                                                            List<RemoteRepository> remoteRepositories)
    throws DependencyResolutionException
    {
        List<Dependency> dependencyList = dependencySpecs.stream()
                .map(DefaultArtifact::new)
                .map(artifact -> new Dependency(artifact, org.apache.maven.artifact.Artifact.SCOPE_COMPILE))
                .collect(Collectors.toList());

        DependencyRequest dRequest = new DependencyRequest();
        dRequest.setCollectRequest(new CollectRequest(dependencyList, Collections.emptyList(), remoteRepositories));

        DependencyResult dResult = repositorySystem.resolveDependencies(repositorySystemSession, dRequest);

        return dResult.getArtifactResults().stream()
                .map(ArtifactResult::getArtifact)
                .map(Artifact::getFile)
                .map(File::toPath)
                .collect(Collectors.toList());
    }

    public static List<Path> resolveDependencies(List<? extends RunnerDependency> dependencies,
                                                 RepositorySystem repositorySystem,
                                                 RepositorySystemSession repositorySystemSession,
                                                 List<RemoteRepository> remoteRepositories)
    throws DependencyResolutionException
    {
        List<Dependency> dependencyList = dependencies.stream()
                .map(rd -> new DefaultArtifact(rd.getGroupId(), rd.getArtifactId(), rd.getClassifier(), rd.getType(), rd.getVersion()))
                .map(artifact -> new Dependency(artifact, org.apache.maven.artifact.Artifact.SCOPE_COMPILE))
                .collect(Collectors.toList());

        DependencyRequest dRequest = new DependencyRequest();
        dRequest.setCollectRequest(new CollectRequest(dependencyList, Collections.emptyList(), remoteRepositories));

        DependencyResult dResult = repositorySystem.resolveDependencies(repositorySystemSession, dRequest);

        return dResult.getArtifactResults().stream()
                .map(ArtifactResult::getArtifact)
                .map(Artifact::getFile)
                .map(File::toPath)
                .collect(Collectors.toList());
    }

    public static List<? extends Version> findAvailableVersions(String groupId, String artifactId, String extension,
                                                                RepositorySystem repositorySystem,
                                                                RepositorySystemSession repositorySystemSession,
                                                                List<RemoteRepository> remoteRepositories)
    throws BoxDatabaseException
    {
        //Find latest version
        Artifact artifact = new DefaultArtifact(groupId, artifactId, extension, "[0.0,)");
        VersionRangeRequest versionRequest = new VersionRangeRequest(artifact, remoteRepositories, null);
        try
        {
            VersionRangeResult result = repositorySystem.resolveVersionRange(repositorySystemSession, versionRequest);
            return result.getVersions();
        }
        catch (VersionRangeResolutionException e)
        {
            throw new BoxDatabaseException(e.getMessage(), e);
        }
    }

    public static Collection<? extends RunnerDependency> findMissingRemoteDependencies(List<? extends RunnerDependency> dependencies,
                                                                                       RepositorySystem repositorySystem,
                                                                                       RepositorySystemSession repositorySystemSession,
                                                                                       List<RemoteRepository> remoteRepositories)
    throws BoxDatabaseException
    {
        List<RunnerDependency> missingDependencies = new ArrayList<>();

        //Only check the version / metadata, don't actually download/resolve the whole JAR
        for (RunnerDependency dependency : dependencies)
        {
            Artifact artifact = new DefaultArtifact(dependency.getGroupId(), dependency.getArtifactId(),
                                                    dependency.getClassifier(), dependency.getType(),
                                                    dependency.getVersion());
            try
            {
                ArtifactDescriptorResult result = repositorySystem.readArtifactDescriptor(repositorySystemSession,
                                                                                          new ArtifactDescriptorRequest(artifact, remoteRepositories, null));

                //Bail out if at least one artifact could not be resolved
                if (result.getRepository() == null)
                    missingDependencies.add(dependency);
            }
            catch (ArtifactDescriptorException e)
            {
                throw new BoxDatabaseException(e);
            }
        }

        //If we get here all dependency metadata was resolved
        return missingDependencies;
    }
}
