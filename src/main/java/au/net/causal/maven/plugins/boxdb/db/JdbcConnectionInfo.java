package au.net.causal.maven.plugins.boxdb.db;

import java.util.Objects;

/**
 * Holder for JDBC connection details, such as URL and user credentials.
 */
public class JdbcConnectionInfo
{
    private final String uri;
    private final String user;
    private final String password;

    private final String host;
    private final int port;

    /**
     * @param uri the JDBC URL.
     * @param user the username to log on to the database with.  May be null.
     * @param password the password to log on to the database with.  May be null.
     * @param host hostname of the database for network JDBC connections.  May be null.
     * @param port port the database is running on for network JDBC connections.  May be zero when unused.
     */
    public JdbcConnectionInfo(String uri, String user, String password, String host, int port)
    {
        Objects.requireNonNull(uri, "uri == null");

        this.uri = uri;
        this.user = user;
        this.password = password;
        this.host = host;
        this.port = port;
    }

    /**
     * @return password to log on to the database with.
     */
    public String getPassword()
    {
        return password;
    }

    /**
     * @return the JDBC URL.
     */
    public String getUri()
    {
        return uri;
    }

    /**
     * @return the user to log on to the database with.
     */
    public String getUser()
    {
        return user;
    }

    /**
     * @return the hostname the database is running on for a network JDBC connection.
     */
    public String getHost()
    {
        return host;
    }

    /**
     * @return the port the database is running on for a network JDBC connection.
     */
    public int getPort()
    {
        return port;
    }

    @Override
    public String toString() {
        final StringBuilder sb = new StringBuilder("JdbcConnectionInfo{");
        sb.append("uri='").append(uri).append('\'');
        sb.append(", user='").append(user).append('\'');
        sb.append(", password='").append(password).append('\'');
        sb.append('}');
        return sb.toString();
    }
}
