package au.net.causal.maven.plugins.boxdb.db;

/**
 * When a script should be executed.
 */
public enum DatabaseStage
{
    /**
     * Runs before any standard database creation logic.  Typically used to create databases, tablespaces, etc.
     * This is run against the system database.
     */
    CREATE,

    /**
     * Runs after standard database creation logic.  Use to create tables, etc.  Runs against the
     * target database.
     */
    SETUP,

    /**
     * Runs after database has been restored from a backup image.  Run against the target database.
     */
    POST_RESTORE
}