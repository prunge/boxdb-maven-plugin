package au.net.causal.maven.plugins.boxdb.db;

/**
 * A target database to execute scripts against.
 */
public enum DatabaseTarget
{
    /**
     * Target the admin or root database.  This is typically used to create additional databases.
     */
    ADMIN
    {
        @Override
        public String password(BoxConfiguration box)
        {
            return box.getAdminPassword();
        }

        @Override
        public String user(BoxConfiguration box)
        {
            return box.getAdminUser();
        }
    },

    /**
     * Target the user database, configured by {@link BoxConfiguration#getDatabaseName()}.
     */
    USER
    {
        @Override
        public String password(BoxConfiguration box)
        {
            return box.getDatabasePassword();
        }

        @Override
        public String user(BoxConfiguration box)
        {
            return box.getDatabaseUser();
        }
    };

    public abstract String user(BoxConfiguration box);
    public abstract String password(BoxConfiguration box);
}
