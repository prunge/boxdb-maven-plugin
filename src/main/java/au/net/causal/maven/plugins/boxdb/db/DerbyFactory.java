package au.net.causal.maven.plugins.boxdb.db;

import au.net.causal.maven.plugins.boxdb.DependencyUtils;
import org.eclipse.aether.util.version.GenericVersionScheme;
import org.eclipse.aether.version.InvalidVersionSpecificationException;
import org.eclipse.aether.version.Version;

import java.util.List;
import java.util.Objects;
import java.util.stream.Collectors;

@BoxDbBundled
public class DerbyFactory extends FileBasedDatabaseFactory
{
    public DerbyFactory()
    {
        super("derby");
    }

    /**
     * Need Derby shared JAR for versions >= 10.15 and other changes.
     */
    private boolean isModernDerbyVersion(BoxConfiguration boxConfiguration, BoxContext context)
    {
        try
        {
            GenericVersionScheme versionScheme = new GenericVersionScheme();
            Version lowestModernVersion = versionScheme.parseVersion("10.15");
            Version version = new GenericVersionScheme().parseVersion(boxConfiguration.getDatabaseVersion());
            return version.compareTo(lowestModernVersion) >= 0;
        }
        catch (InvalidVersionSpecificationException e)
        {
            //Failed to parse version so assume not recent
            context.getLog().debug("Failed to parse DB version: " + boxConfiguration.getDatabaseVersion() + " - " + e.getMessage(), e);
            return false;
        }
    }

    @Override
    public DerbyDatabase create(BoxConfiguration boxConfiguration, ProjectConfiguration projectConfiguration, BoxContext context)
    throws BoxDatabaseException
    {
        Objects.requireNonNull(boxConfiguration, "boxConfiguration == null");
        Objects.requireNonNull(context, "context == null");

        initializeDefaults(boxConfiguration, projectConfiguration, context);

        if (isModernDerbyVersion(boxConfiguration, context))
            return new ModernDerbyDatabase(boxConfiguration, projectConfiguration, context);
        else
            return new DerbyDatabase(boxConfiguration, projectConfiguration, context);
    }

    protected void initializeDefaults(BoxConfiguration boxConfiguration, ProjectConfiguration projectConfiguration, BoxContext context)
    throws BoxDatabaseException
    {
        super.initializeDefaults(boxConfiguration, projectConfiguration, context);

        //Intentionally not picking 10.15 since that requires Java 9 but we want our defaults to be compatible with Java 8
        if (boxConfiguration.getDatabaseVersion() == null)
            boxConfiguration.setDatabaseVersion("10.14.2.0");
        if (boxConfiguration.getDatabasePort() <= 0)
            boxConfiguration.setDatabasePort(1527);

        CollationTranslator collationTranslator = new CollationTranslator(null, this::mapCommonCollation);
        collationTranslator.processCollation(boxConfiguration);
    }
    
    private String mapCommonCollation(CommonCollation collation)
    {
        switch (collation)
        {
            case BINARY:
                return null;
            case CASE_INSENSITIVE:
                return "en_US;PRIMARY";
            default:
                throw new Error("Unknown common collation: " + collation);
        }
    }

    @Override
    public List<String> availableVersions(ProjectConfiguration projectConfiguration, BoxContext context)
    throws BoxDatabaseException
    {
        List<? extends Version> versions = DependencyUtils.findAvailableVersions(DerbyDatabase.DERBY_DATABASE_GROUP_ID,
                                                                                 DerbyDatabase.DERBY_DATABASE_ARTIFACT_ID,
                                                                                 "jar", context.getRepositorySystem(),
                                                                                 context.getRepositorySystemSession(),
                                                                                 context.getRemoteRepositories());
        return versions.stream().map(Version::toString).collect(Collectors.toList());
    }
}
