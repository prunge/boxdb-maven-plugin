package au.net.causal.maven.plugins.boxdb.db;

import au.net.causal.maven.plugins.boxdb.ScriptReaderRunner;
import org.apache.maven.plugin.MojoExecutionException;

import java.io.IOException;
import java.net.URL;
import java.sql.Connection;
import java.sql.SQLException;
import java.util.Arrays;
import java.util.regex.Pattern;

public class MySqlDatabase extends BaseMySqlDatabase
{
    public MySqlDatabase(BoxConfiguration boxConfiguration, ProjectConfiguration projectConfiguration,
                         BoxContext context, DockerRegistry dockerRegistry, String dockerRepositoryName)
    {
        super(boxConfiguration, projectConfiguration, context, dockerRegistry, dockerRepositoryName);
    }

    @Override
    public JdbcConnectionInfo jdbcConnectionInfo(DatabaseTarget target)
    throws BoxDatabaseException
    {
        String databaseName;
        if (target == DatabaseTarget.ADMIN)
            databaseName = "mysql";
        else
            databaseName = getBoxConfiguration().getDatabaseName();

        //Client encoding of UTF-8 is OK even if database encoding is only ASCII -
        //attempts to put out-of-range characters into the database results in SQL errors
        String uri =  "jdbc:mysql://" +
                        getContext().getDockerHostAddress() +
                        ":" + getBoxConfiguration().getDatabasePort() +
                        "/" + databaseName +
                        "?characterEncoding=UTF-8";

        return new JdbcConnectionInfo(uri,
                        target.user(getBoxConfiguration()),
                        target.password(getBoxConfiguration()),
                        getContext().getDockerHostAddress(),
                        getBoxConfiguration().getDatabasePort());
    }

    @Override
    public JdbcDriverInfo jdbcDriverInfo()
    throws BoxDatabaseException
    {
        //Drivers are backward/forward compatible so just use latest
        return new JdbcDriverInfo(new RunnerDependency("mysql", "mysql-connector-java", "8.0.33"), "com.mysql.cj.jdbc.Driver");
    }
    
    protected DataSourceBuilder dataSourceBuilder(DatabaseTarget target)
    throws BoxDatabaseException
    {
        JdbcConnectionInfo jdbcInfo = jdbcConnectionInfo(target);
        return new DataSourceBuilder(getContext())
                    .dataSourceClassName("com.mysql.cj.jdbc.MysqlDataSource")
                    .dependencies(jdbcDriverInfo().getDependencies())
                    .configureDataSource("setUrl", String.class, jdbcInfo.getUri())
                    .configureDataSource("setUser", String.class, jdbcInfo.getUser())
                    //Client encoding of UTF-8 is OK even if database encoding is only ASCII -
                    //attempts to put out-of-range characters into the database results in SQL errors
                    .configureDataSource("setCharacterEncoding", String.class, "UTF-8")
                    .configureDataSource("setClobCharacterEncoding", String.class, "UTF-8")
                    //Avoid spam from driver logging (especially during JDBC waiting) - anything important we get
                    //through SQLExceptions that we log ourselves anyway
                    .configureDataSource("setLogger", String.class, "com.mysql.cj.log.NullLogger")
                    .configureDataSource("setPassword", String.class, jdbcInfo.getPassword());
    }

    @Override
    public Connection createJdbcConnection(DatabaseTarget targetDatabase)
    throws SQLException, BoxDatabaseException, IOException
    {
        return dataSourceBuilder(targetDatabase).create().getConnection();
    }

    @Override
    public void configureNewDatabase()
    throws IOException, SQLException, BoxDatabaseException
    {
        URL initScript;
        String[] versionTokens = getBoxConfiguration().getDatabaseVersion().split(Pattern.quote("."));
        String firstVersionToken = versionTokens.length > 0 ? versionTokens[0] : ""; //Should always have one token but be safe anyway
        try
        {
            int firstVersionNumber = Integer.parseInt(firstVersionToken);
            if (firstVersionNumber >= 8)
                initScript = MySqlDatabase.class.getResource("mysql8-create-database.sql");
            else
                initScript = MySqlDatabase.class.getResource("mysql-create-database.sql");
        }
        catch (NumberFormatException e)
        {
            getContext().getLog().warn("Failed to parse MySQL version " + getBoxConfiguration().getDatabaseVersion() + ": " + e, e);
            initScript = MySqlDatabase.class.getResource("mysql-create-database.sql");
        }

        getContext().getLog().debug("Using MySQL init script: " + initScript.toExternalForm());

        ScriptReaderRunner scriptRunner = getContext().createScriptReaderRunner(this, getBoxConfiguration(), getProjectConfiguration());
        ScriptReaderExecution execution = new ScriptReaderExecution();
        execution.setFiltering(true);
        execution.setScripts(Arrays.asList(initScript));

        try
        {
            scriptRunner.execute(execution, DatabaseTarget.ADMIN, getProjectConfiguration().getScriptTimeout());
        }
        catch (MojoExecutionException e)
        {
            throw new BoxDatabaseException(e);
        }
    }
}
