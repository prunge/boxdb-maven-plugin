package au.net.causal.maven.plugins.boxdb.db;

import au.net.causal.maven.plugins.boxdb.DependencyUtils;
import org.eclipse.aether.version.Version;

import java.util.List;
import java.util.Objects;
import java.util.stream.Collectors;

@BoxDbBundled
public class SqliteFactory extends FileBasedDatabaseFactory
{
    public SqliteFactory()
    {
        super("sqlite");
    }

    @Override
    public SqliteDatabase create(BoxConfiguration boxConfiguration, ProjectConfiguration projectConfiguration, BoxContext context)
    throws BoxDatabaseException
    {
        Objects.requireNonNull(boxConfiguration, "boxConfiguration == null");
        Objects.requireNonNull(context, "context == null");

        initializeDefaults(boxConfiguration, projectConfiguration, context);

        return new SqliteDatabase(boxConfiguration, projectConfiguration, context);
    }

    protected void initializeDefaults(BoxConfiguration boxConfiguration, ProjectConfiguration projectConfiguration, BoxContext context)
    throws BoxDatabaseException
    {
        super.initializeDefaults(boxConfiguration, projectConfiguration, context);

        if (boxConfiguration.getDatabaseVersion() == null)
            boxConfiguration.setDatabaseVersion("3.46.0.0");
    }

    @Override
    public List<String> availableVersions(ProjectConfiguration projectConfiguration, BoxContext context)
    throws BoxDatabaseException
    {
        List<? extends Version> versions = DependencyUtils.findAvailableVersions(SqliteDatabase.SQLITE_DATABASE_GROUP_ID,
                                                                                 SqliteDatabase.SQLITE_DATABASE_ARTIFACT_ID,
                                                                                 "jar", context.getRepositorySystem(),
                                                                                 context.getRepositorySystemSession(),
                                                                                 context.getRemoteRepositories());
        return versions.stream().map(Version::toString).collect(Collectors.toList());
    }
}
