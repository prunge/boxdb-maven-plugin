package au.net.causal.maven.plugins.boxdb.db;

import java.util.Objects;
import java.util.function.Function;

/**
 * Handles translation of collation names, including mapping common collation names.
 * 
 * @since 2.0
 */
public class CollationTranslator 
{
    private final String defaultCollation;
    private final Function<CommonCollation, String> commonCollationMapper;

    /**
     * Creates the translator.
     * 
     * @param defaultCollation default collation to configure if none was specified.  May be null.
     * @param commonCollationMapper function to map all common collations to database-specific collation names.
     */
    public CollationTranslator(String defaultCollation, Function<CommonCollation, String> commonCollationMapper)
    {
        Objects.requireNonNull(commonCollationMapper, "commonCollationMapper == null");
        this.defaultCollation = defaultCollation;
        this.commonCollationMapper = commonCollationMapper;
    }

    /**
     * Translates collation name.
     * 
     * @param collationName the name to translate.
     *                      
     * @return translated name.
     */
    public String translate(String collationName)
    {
        if (collationName == null)
            return defaultCollation;
        
        CommonCollation commonCollation = CommonCollation.matching(collationName);
        if (commonCollation != null)
            return commonCollationMapper.apply(commonCollation);
        
        return collationName;
    }

    /**
     * Processes the configured collation on a database configuration, performing translation where necessary.
     * 
     * @param boxDatabase configuration to process.
     */
    public void processCollation(BoxConfiguration boxDatabase)
    {
        String collationName = boxDatabase.getDatabaseCollation();
        if (collationName == null)
            collationName = boxDatabase.getConfiguration().get("collation");
        collationName = translate(collationName);
        boxDatabase.setDatabaseCollation(collationName);
    }
}
