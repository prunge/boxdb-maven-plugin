package au.net.causal.maven.plugins.boxdb.db;

public enum ScriptSelection
{
    /**
     * Runs the first existing script in the script list.
     */
    FIRST,

    /**
     * Runs every script in the script list.
     */
    ALL
}