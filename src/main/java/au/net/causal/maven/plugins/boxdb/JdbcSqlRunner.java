package au.net.causal.maven.plugins.boxdb;

import au.net.causal.maven.plugins.boxdb.db.RunnerDependency;
import org.apache.commons.lang.StringEscapeUtils;
import org.apache.maven.plugin.logging.Log;
import org.eclipse.aether.RepositorySystem;
import org.eclipse.aether.RepositorySystemSession;
import org.eclipse.aether.repository.RemoteRepository;
import org.eclipse.aether.resolution.DependencyResolutionException;

import javax.sql.DataSource;
import java.io.BufferedReader;
import java.io.IOException;
import java.lang.reflect.InvocationTargetException;
import java.sql.Connection;
import java.sql.ResultSet;
import java.sql.ResultSetMetaData;
import java.sql.SQLException;
import java.sql.Statement;
import java.util.List;

public class JdbcSqlRunner implements AutoCloseable
{
    private final Connection connection;
    private final SqlDivider sqlDivider = new SqlDivider();
    private final Log sqlEchoLog;

    public JdbcSqlRunner(Connection connection, Log sqlEchoLog)
    {
        this.connection = connection;
        this.sqlEchoLog = sqlEchoLog;
    }

    @Override
    public void close()
    throws SQLException
    {
        connection.close();
    }

    public static JdbcSqlRunner create(String dataSourceClassName,
                                       List<RunnerDependency> dependencies,
                                       RepositorySystem repositorySystem,
                                       RepositorySystemSession repositorySystemSession,
                                       List<RemoteRepository> remoteRepositories,
                                       Log sqlEchoLog, ClassLoaderCache classLoaderCache)
    throws DependencyResolutionException, IOException, ClassNotFoundException, NoSuchMethodException, 
            IllegalAccessException, InstantiationException, InvocationTargetException, SQLException
    {
        JavaRunner javaRunner = JavaRunner.createFromDependencies(dataSourceClassName, dependencies, repositorySystem, 
                                    repositorySystemSession, remoteRepositories, classLoaderCache);
        Class<?> dataSourceClass = javaRunner.makeClass();
        DataSource dataSource = (DataSource)dataSourceClass.getConstructor().newInstance();
        Connection con = dataSource.getConnection();
        return new JdbcSqlRunner(con, sqlEchoLog);
    }

    public void executeSql(BufferedReader reader)
    throws IOException, SQLException
    {
        try (Statement statement = connection.createStatement())
        {
            sqlDivider.splitAndProcessSql(reader, sql -> runSqlWithConnection(sql, statement));
        }
    }

    private void runSqlWithConnection(String sql, Statement statement)
    throws SQLException
    {
        if (sqlEchoLog != null)
            sqlEchoLog.info("SQL: " + sql);

        boolean hasResultSet = statement.execute(sql);

        //Don't echo anything if there's no log
        if (sqlEchoLog == null)
            return;

        do
        {
            if (hasResultSet)
            {
                try (ResultSet rs = statement.getResultSet())
                {
                    printResultSet(rs);
                }
                hasResultSet = statement.getMoreResults();
            }
            else
            {
                int updateCount = statement.getUpdateCount();
                if (updateCount >= 0)
                    sqlEchoLog.info("SQL Result: " + updateCount + " rows affected");
            }
        }
        while (hasResultSet);
    }

    private void printResultSet(ResultSet rs)
    throws SQLException
    {
        String outputDelimiter = ",";

        ResultSetMetaData md = rs.getMetaData();
        int columnCount = md.getColumnCount();
        StringBuilder line = new StringBuilder();

        boolean first = true;
        for (int col = 1; col <= columnCount; col++)
        {
            String columnValue = md.getColumnName(col);
            if (columnValue != null)
            {
                columnValue = columnValue.trim();

                if (",".equals(outputDelimiter))
                    columnValue = StringEscapeUtils.escapeCsv(columnValue);
            }

            if ( first )
                first = false;
            else
                line.append(outputDelimiter);

            line.append(columnValue);
        }
        sqlEchoLog.info(line);
        line = new StringBuilder();

        while ( rs.next() )
        {
            first = true;
            for ( int col = 1; col <= columnCount; col++ )
            {
                String columnValue = rs.getString( col );
                if ( columnValue != null )
                {
                    columnValue = columnValue.trim();

                    if ( ",".equals( outputDelimiter ) )
                    {
                        columnValue = StringEscapeUtils.escapeCsv( columnValue );
                    }
                }

                if ( first )
                {
                    first = false;
                }
                else
                {
                    line.append( outputDelimiter );
                }
                line.append( columnValue );
            }
            sqlEchoLog.info(line);
            line = new StringBuilder();
        }
    }
}
