package au.net.causal.maven.plugins.boxdb.db;

import com.google.common.annotations.VisibleForTesting;
import com.google.common.collect.ImmutableList;
import org.eclipse.aether.util.version.GenericVersionScheme;
import org.eclipse.aether.version.InvalidVersionSpecificationException;
import org.eclipse.aether.version.Version;
import org.eclipse.aether.version.VersionRange;
import org.eclipse.aether.version.VersionScheme;

import java.util.ArrayList;
import java.util.Arrays;
import java.util.Collections;
import java.util.HashSet;
import java.util.LinkedHashSet;
import java.util.List;
import java.util.Objects;
import java.util.Set;
import java.util.function.Supplier;
import java.util.stream.Collectors;

/**
 * Database factory that can be subclassed to allow multiple different delegate factories to be used depending on the version number, matched
 * by a Maven version range.  Useful when different factories service different version ranges of a database (e.g. 2.x from A, 3.x from B).
 *
 * @since 3.3
 */
public abstract class VersionRangeSwitchingDatabaseFactory implements BoxDatabaseFactory
{
    private final String name;
    private final List<Specifier> specifiers;
    private final Supplier<? extends BoxDatabaseFactory> defaultFactory;
    private final Supplier<? extends BoxDatabaseFactory> fallbackFactory;

    private static final VersionScheme versionScheme = new GenericVersionScheme();

    /**
     * Creates a VersionRangeSwitchingDatabaseFactory.
     *
     * @param name name of the factory.
     * @param defaultFactory used when no version number is supplied.
     * @param fallbackFactory used when a version number does not match one of the specifiers or a version number cannot be parsed.
     * @param specifiers a list of version specifiers that map a version range to a particular factory for that version range.
     */
    protected VersionRangeSwitchingDatabaseFactory(String name,
                                                   Supplier<? extends BoxDatabaseFactory> defaultFactory,
                                                   Supplier<? extends BoxDatabaseFactory> fallbackFactory,
                                                   List<Specifier> specifiers)
    {
        Objects.requireNonNull(name, "name == null");
        Objects.requireNonNull(defaultFactory, "defaultFactory == null");
        Objects.requireNonNull(specifiers, "specifiers == null");
        this.name = name;
        this.defaultFactory = defaultFactory;
        this.fallbackFactory = fallbackFactory;
        this.specifiers = ImmutableList.copyOf(specifiers);
    }

    /**
     * Creates a VersionRangeSwitchingDatabaseFactory with no fallback.
     *
     * @param name name of the factory.
     * @param defaultVersion use this particular version when no version is specified.  Must match a specifier.
     * @param specifiers a list of version specifiers that map a version range to a particular factory for that version range.
     */
    protected VersionRangeSwitchingDatabaseFactory(String name, String defaultVersion, List<Specifier> specifiers)
    {
        Objects.requireNonNull(name, "name == null");
        Objects.requireNonNull(specifiers, "specifiers == null");
        this.name = name;
        this.specifiers = ImmutableList.copyOf(specifiers);
        defaultFactory = factoryForVersion(defaultVersion);
        if (defaultFactory == null)
            throw new IllegalArgumentException("Invalid default version '" + defaultVersion + "' - does not have corresponding factory");

        fallbackFactory = null;
    }

    /**
     * Finds a supplier for a factory for the given version by matching version ranges.  Uses the default if version is null, and uses the
     * fallback if the version has no match or cannot be parsed.
     *
     * @param version the version number.
     *
     * @return the supplier for a matched factory, or null if nothing was found and there was no fallback.
     */
    @VisibleForTesting
    Supplier<? extends BoxDatabaseFactory> factoryForVersion(String version)
    {
        if (version == null)
            return defaultFactory;

        try
        {
            Version parsedVersion = versionScheme.parseVersion(version);

            for (Specifier specifier : specifiers)
            {
                if (specifier.containsVersion(parsedVersion))
                    return specifier.getDatabaseFactory();
            }
        }
        catch (InvalidVersionSpecificationException e)
        {
            //Fallthrough to fallback factory if version cannot be parsed
        }

        return fallbackFactory;
    }

    @Override
    public BoxDatabase create(BoxConfiguration boxConfiguration, ProjectConfiguration projectConfiguration, BoxContext context)
    throws BoxDatabaseException
    {
        String version = boxConfiguration.getDatabaseVersion();
        Supplier<? extends BoxDatabaseFactory> factorySupplier = factoryForVersion(version);
        if (factorySupplier == null)
        {
            String availableVersionRanges = specifiers.stream().map(s -> s.getVersionRange().toString()).collect(Collectors.joining(", "));
            throw new BoxDatabaseException("Invalid version '" + version + "' for database type '" + name() + "'.  Available version ranges: " + availableVersionRanges);
        }

        return factorySupplier.get().create(boxConfiguration, projectConfiguration, context);
    }

    @Override
    public String name()
    {
        return name;
    }

    @Override
    public List<String> availableVersions(ProjectConfiguration projectConfiguration, BoxContext context)
    throws BoxDatabaseException
    {
        Set<String> mappedVersions = new HashSet<>();
        for (Specifier specifier : specifiers)
        {
            if (specifier.getExplicitVersions().isEmpty())
            {
                List<String> versions = specifier.getDatabaseFactory().get().availableVersions(projectConfiguration, context);
                for (String version : versions)
                {
                    try
                    {
                        Version parsedVersion = versionScheme.parseVersion(version);
                        if (specifier.containsVersion(parsedVersion))
                            mappedVersions.add(version);
                    }
                    catch (InvalidVersionSpecificationException e)
                    {
                        //Can't check if we can't parse version
                    }
                }
            }
            else
            {
                mappedVersions.addAll(specifier.getExplicitVersions());
            }
        }

        if (fallbackFactory != null)
            mappedVersions.addAll(fallbackFactory.get().availableVersions(projectConfiguration, context));

        List<String> mappedVersionList = new ArrayList<>(mappedVersions);
        Collections.sort(mappedVersionList);

        return mappedVersionList;
    }

    /**
     * Specifies a version range and a database factory.
     */
    public static class Specifier
    {
        private final VersionRange versionRange;
        private final Supplier<? extends BoxDatabaseFactory> databaseFactory;
        private final Set<String> explicitVersions;

        /**
         * Creates a specifier with a version range and a factory that should be used for it.
         *
         * @param versionRange the version range.
         * @param databaseFactory supplier for a database factory.
         * @param explicitVersions if one or more specified, only these versions will be selectable from this specifier.
         */
        public Specifier(VersionRange versionRange, Supplier<? extends BoxDatabaseFactory> databaseFactory, String... explicitVersions)
        {
            this.versionRange = Objects.requireNonNull(versionRange, "versionRange == null");
            this.databaseFactory = Objects.requireNonNull(databaseFactory, "databaseFactory == null");
            this.explicitVersions = new LinkedHashSet<>(Arrays.asList(explicitVersions));
        }

        /**
         * Creates a specifier with a version range and a factory that should be used for it, parsing the version range.
         *
         * @param versionRange the version range string in the form of a Maven version range.
         * @param databaseFactory supplier for a database factory.
         * @param explicitVersions if one or more specified, only these versions will be selectable from this specifier.
         */
        public Specifier(String versionRange, Supplier<? extends BoxDatabaseFactory> databaseFactory, String... explicitVersions)
        {
            try
            {
                this.versionRange = versionScheme.parseVersionRange(Objects.requireNonNull(versionRange, "versionRange == null"));
            }
            catch (InvalidVersionSpecificationException e)
            {
                throw new RuntimeException(e);
            }
            this.databaseFactory = Objects.requireNonNull(databaseFactory, "databaseFactory == null");
            this.explicitVersions = new LinkedHashSet<>(Arrays.asList(explicitVersions));
        }

        /**
         * @return the version range.
         */
        public VersionRange getVersionRange()
        {
            return versionRange;
        }

        /**
         * @return supplier for a database factory to use for this version range.
         */
        public Supplier<? extends BoxDatabaseFactory> getDatabaseFactory()
        {
            return databaseFactory;
        }

        /**
         * @return a set of version numbers that this specifier explicitly defines.  If empty, the whole range will be supported.
         */
        public Set<String> getExplicitVersions()
        {
            return explicitVersions;
        }

        /**
         * @param version check if this specifier supports this version.
         *
         * @return true if this specifier supports this version, false if not.
         */
        public boolean containsVersion(Version version)
        {
            if (getExplicitVersions().isEmpty())
                return getVersionRange().containsVersion(version);
            else
                return getExplicitVersions().contains(version.toString());
        }

        @Override
        public String toString()
        {
            return "Factory for: " + getVersionRange().toString();
        }
    }
}
