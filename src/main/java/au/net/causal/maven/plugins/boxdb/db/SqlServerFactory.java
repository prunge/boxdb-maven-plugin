package au.net.causal.maven.plugins.boxdb.db;

import au.net.causal.maven.plugins.boxdb.vagrant.Vagrant;

import java.io.IOException;
import java.net.URL;
import java.nio.file.Path;
import java.util.Collections;
import java.util.List;
import java.util.Locale;

@BoxDbBundled
public class SqlServerFactory extends VagrantDatabaseFactory
{
    private static final String VERSION_2014 = "2014";

    public SqlServerFactory()
    {
        super("sqlserver");
    }

    @Override
    protected void initializeDefaults(BoxConfiguration boxConfiguration)
    {
        boxConfiguration.setDatabaseVersion(VERSION_2014);

        super.initializeDefaults(boxConfiguration);

        //Port is hardcoded
        boxConfiguration.setDatabasePort(1433);

        //DB admin credentials are hardcoded
        boxConfiguration.setAdminUser("vagrant");
        boxConfiguration.setAdminPassword("vagrant");

        //If database user/password are not configured then make sensible defaults
        if (boxConfiguration.getDatabaseUser() == null)
            boxConfiguration.setDatabaseUser(boxConfiguration.getDatabaseName());
        if (boxConfiguration.getDatabasePassword() == null)
            boxConfiguration.setDatabasePassword(boxConfiguration.getDatabaseUser());

        if (!boxConfiguration.getConfiguration().containsKey(SqlServerJdbcDriverType.CONFIGURATION_PROPERTY))
            boxConfiguration.getConfiguration().put(SqlServerJdbcDriverType.CONFIGURATION_PROPERTY, SqlServerJdbcDriverType.MICROSOFT.name().toLowerCase(Locale.ENGLISH));

        CollationTranslator collationTranslator = new CollationTranslator("Latin1_General_BIN", this::mapCommonCollation);
        collationTranslator.processCollation(boxConfiguration);
    }

    private String mapCommonCollation(CommonCollation collation)
    {
        switch (collation)
        {
            case BINARY:
                return "Latin1_General_BIN";
            case CASE_INSENSITIVE:
                return "Latin1_General_CI_AI";
            default:
                throw new Error("Unknown common collation: " + collation);
        }
    }

    @Override
    protected VagrantDatabase createVagrantDatabase(Vagrant vagrant,
                                                    Path baseDirectory, Path vagrantFile,
                                                    BoxConfiguration boxConfiguration,
                                                    ProjectConfiguration projectConfiguration,
                                                    BoxContext context)
    throws BoxDatabaseException, IOException
    {
        verifyAndInstallVagrantPluginIfNeeded("vagrant-vbguest", vagrant, context);

        //Extract additional resources
        Path scriptsVagrantfile = baseDirectory.resolve("Vagrantfile-scripts");
        Path sqlVagrantfile = baseDirectory.resolve("Vagrantfile-sql");
        Path logsVagrantfile = baseDirectory.resolve("Vagrantfile-logs");
        copyResource(resource("initdb.sql"), baseDirectory.resolve("initdb.sql"));
        copyResource(resource("Vagrantfile-scripts"), scriptsVagrantfile);
        copyResource(resource("Vagrantfile-sql"), sqlVagrantfile);
        copyResource(resource("Vagrantfile-logs"), logsVagrantfile);

        return new SqlServerDatabase(boxConfiguration, projectConfiguration, context, vagrant, baseDirectory,
                    vagrantFile, scriptsVagrantfile, sqlVagrantfile, logsVagrantfile);
    }

    @Override
    protected URL vagrantFileResource() throws IOException
    {
        return resource("Vagrantfile");
    }

    private URL resource(String name)
    {
        return SqlServerFactory.class.getResource("sqlserver/" + name);
    }

    @Override
    public List<String> availableVersions(ProjectConfiguration projectConfiguration, BoxContext context)
    throws BoxDatabaseException
    {
        return Collections.singletonList(VERSION_2014);
    }
}
