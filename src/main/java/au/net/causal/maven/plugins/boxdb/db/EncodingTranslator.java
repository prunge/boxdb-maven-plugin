package au.net.causal.maven.plugins.boxdb.db;

import java.util.Objects;
import java.util.function.Function;

/**
 * Handles translation of encoding names, including mapping common encoding names.
 * 
 * @since 2.0
 */
public class EncodingTranslator 
{
    private final String defaultEncoding;
    private final Function<CommonEncoding, String> commonEncodingMapper;

    /**
     * Creates the translator.
     * 
     * @param defaultEncoding default encoding to configure if none was specified.  May be null.
     * @param commonEncodingMapper function to map all common encodings to database-specific encoding names.
     */
    public EncodingTranslator(String defaultEncoding, Function<CommonEncoding, String> commonEncodingMapper)
    {
        Objects.requireNonNull(commonEncodingMapper, "commonEncodingMapper == null");
        this.defaultEncoding = defaultEncoding;
        this.commonEncodingMapper = commonEncodingMapper;
    }

    /**
     * Translates encoding name.
     * 
     * @param encodingName the name to translate.
     *                      
     * @return translated name.
     */
    public String translate(String encodingName)
    {
        if (encodingName == null)
            return defaultEncoding;
        
        CommonEncoding commonEncoding = CommonEncoding.matching(encodingName);
        if (commonEncoding != null)
            return commonEncodingMapper.apply(commonEncoding);
        
        return encodingName;
    }

    /**
     * Processes the configured encoding on a database configuration, performing translation where necessary.
     * 
     * @param boxDatabase configuration to process.
     */
    public void processEncoding(BoxConfiguration boxDatabase)
    {
        String encodingName = boxDatabase.getDatabaseEncoding();
        encodingName = translate(encodingName);
        boxDatabase.setDatabaseEncoding(encodingName);
    }
}
