package au.net.causal.maven.plugins.boxdb.db;

import java.io.File;
import java.util.ArrayList;
import java.util.List;

/**
 * Defines the execution of a list of SQL scripts.
 */
public class ScriptExecution extends ScriptExecutionBase
{
    public static final String DEFAULT_BASE_DIRECTORY = "src/test/boxdb/sql";

    private ScriptSelection selection = ScriptSelection.ALL;
    private boolean ignoreMissing;
    private File directory;
    private DatabaseStage stage;
    private List<String> scripts = new ArrayList<>();

    /**
     * The base directory for database scripts.  Useful if the {@linkplain #getScripts() scripts} are relative
     * file names.  Defaults to {@value DEFAULT_BASE_DIRECTORY}.
     */
    public File getDirectory()
    {
        return directory;
    }

    public void setDirectory(File directory)
    {
        this.directory = directory;
    }

    /**
     * Controls when the scripts are run - before or after the standard database creation logic.
     */
    public DatabaseStage getStage()
    {
        return stage;
    }

    public void setStage(DatabaseStage stage)
    {
        this.stage = stage;
    }

    /**
     * If true, script files that don't exist will be ignored.  If false, script files that don't exist
     * will result in a build error.
     */
    public boolean isIgnoreMissing()
    {
        return ignoreMissing;
    }

    public void setIgnoreMissing(boolean ignoreMissing)
    {
        this.ignoreMissing = ignoreMissing;
    }

    /**
     * Controls whether one or all scripts are executed.
     */
    public ScriptSelection getSelection()
    {
        return selection;
    }

    public void setSelection(ScriptSelection selection)
    {
        this.selection = selection;
    }

    /**
     * A list of file names relative to the {@linkplain #getDirectory() base directory}.  May also
     * be absolute file names.
     * <p>
     *
     * As well as standard Maven variables such as ${basedir}, these additional variables can be used
     * which are filled in from the box configuration:
     *
     * <ul>
     *     <li>${box.databaseType}</li>
     *     <li>${box.databaseVersion}</li>
     *     <li>${box.databaseVersion.major}</li>
     * </ul>
     */
    public List<String> getScripts()
    {
        return scripts;
    }

    public void setScripts(List<String> scripts)
    {
        this.scripts = scripts;
    }
}
