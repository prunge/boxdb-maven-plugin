package au.net.causal.maven.plugins.boxdb.db;

public class BoxDatabaseException extends Exception
{
    public BoxDatabaseException()
    {
    }

    public BoxDatabaseException(Throwable cause)
    {
        super(cause);
    }

    public BoxDatabaseException(String message)
    {
        super(message);
    }

    public BoxDatabaseException(String message, Throwable cause)
    {
        super(message, cause);
    }
}
