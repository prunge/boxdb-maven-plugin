package au.net.causal.maven.plugins.boxdb.db;

public abstract class ScriptExecutionBase
{
    private boolean filtering;
    private boolean silentExecution;
    private ExecutionMode mode = ExecutionMode.NATIVE;

    /**
     * @return whether resource filtering is applied to SQL script files.  Defaults to false.
     */
    public boolean isFiltering()
    {
        return filtering;
    }

    public void setFiltering(boolean filtering)
    {
        this.filtering = filtering;
    }

    /**
     * @return whether the script will be executed with JDBC or with the database's native
     * script tool.
     */
    public ExecutionMode getMode()
    {
        return mode;
    }

    public void setMode(ExecutionMode mode)
    {
        this.mode = mode;
    }

    /**
     * @return if true, output to the console/logs is suppressed for this script.
     *
     * @since 3.2
     */
    public boolean isSilentExecution()
    {
        return silentExecution;
    }

    public void setSilentExecution(boolean silentExecution)
    {
        this.silentExecution = silentExecution;
    }
}
