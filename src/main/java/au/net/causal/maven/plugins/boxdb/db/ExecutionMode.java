package au.net.causal.maven.plugins.boxdb.db;

/**
 * How a SQL script is executed.
 */
public enum ExecutionMode
{
    /**
     * Execute scripts using JDBC.
     */
    JDBC,

    /**
     * Execute scripts using native database tool if available.  This mode may be slower but
     * also may allow additional functionality.
     */
    NATIVE
}
