package au.net.causal.maven.plugins.boxdb.db;

import java.io.IOException;
import java.net.URI;
import java.util.ArrayList;
import java.util.List;
import java.util.Locale;
import java.util.stream.Collectors;

@BoxDbBundled
public class SqlServerLinuxFactory extends DockerDatabaseFactory
{
    static final String SQLSERVER_DOCKER_REPOSITORY = "mssql/server";

    public SqlServerLinuxFactory()
    {
        super("sqlserver-linux");
    }

    @Override
    protected void initializeDefaults(BoxConfiguration boxConfiguration)
    {
        if (boxConfiguration.getAdminUser() == null)
            boxConfiguration.setAdminUser("sa");
        if (boxConfiguration.getAdminPassword() == null)
            boxConfiguration.setAdminPassword("SqlServerBox1");
        if (boxConfiguration.getDatabaseName() == null)
            boxConfiguration.setDatabaseName("app");
        if (boxConfiguration.getDatabaseVersion() == null)
            boxConfiguration.setDatabaseVersion("2022-CU13-ubuntu-22.04");
        if (boxConfiguration.getDatabasePort() <= 0)
            boxConfiguration.setDatabasePort(1433);
        if (boxConfiguration.getDatabaseUser() == null)
            boxConfiguration.setDatabaseUser(boxConfiguration.getDatabaseName());
        if (boxConfiguration.getDatabasePassword() == null)
            boxConfiguration.setDatabasePassword(boxConfiguration.getDatabaseUser());

        if (!boxConfiguration.getConfiguration().containsKey(SqlServerJdbcDriverType.CONFIGURATION_PROPERTY))
            boxConfiguration.getConfiguration().put(SqlServerJdbcDriverType.CONFIGURATION_PROPERTY, SqlServerJdbcDriverType.MICROSOFT.name().toLowerCase(Locale.ENGLISH));

        CollationTranslator collationTranslator = new CollationTranslator("Latin1_General_BIN", this::mapCommonCollation);
        collationTranslator.processCollation(boxConfiguration);

        super.initializeDefaults(boxConfiguration);
    }

    private String mapCommonCollation(CommonCollation collation)
    {
        switch (collation)
        {
            case BINARY:
                return "Latin1_General_BIN";
            case CASE_INSENSITIVE:
                return "Latin1_General_CI_AI";
            default:
                throw new Error("Unknown common collation: " + collation);
        }
    }

    @Override
    protected SqlServerLinuxDatabase createDockerDatabase(BoxConfiguration boxConfiguration, ProjectConfiguration projectConfiguration, BoxContext context)
    throws BoxDatabaseException
    {
        try
        {
            return new SqlServerLinuxDatabase(boxConfiguration, projectConfiguration, context, dockerRegistry());
        }
        catch (IOException e)
        {
            throw new BoxDatabaseException("I/O error creating database: " + e, e);
        }
    }

    @Override
    protected DockerRegistry dockerRegistry()
    {
        return new DockerRegistry(URI.create("https://mcr.microsoft.com/v2/"));
    }

    @Override
    public List<String> availableVersions(ProjectConfiguration projectConfiguration, BoxContext context)
    throws BoxDatabaseException
    {
        try
        {
            List<String> tags = dockerRegistry().readTags(SQLSERVER_DOCKER_REPOSITORY);

            List<String> versions = new ArrayList<>();

            //Originally versions without -ubuntu suffix just worked so add those

            //The '-ubuntu' suffix is worked around in SqlServerLinuxDatabase#dockerImageName()
            List<String> strippedTags =  tags.stream()
                                             .filter(tag -> tag.endsWith("-ubuntu"))
                                             .map(tag -> tag.substring(0, tag.length() - "-ubuntu".length()))
                                             .collect(Collectors.toList());
            versions.addAll(strippedTags);

            //And add all the tags unmodified as well, but only ubuntu ones
            List<String> ubuntuTags = tags.stream()
                                          .filter(tag -> tag.contains("-ubuntu"))
                                          .collect(Collectors.toList());
            versions.addAll(ubuntuTags);

            return versions;
        }
        catch (IOException e)
        {
            throw new BoxDatabaseException("Error reading tags: " + e.getMessage(), e);
        }
    }
}
