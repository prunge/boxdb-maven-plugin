package au.net.causal.maven.plugins.boxdb.db;

import au.net.causal.maven.plugins.boxdb.DependencyUtils;
import au.net.causal.maven.plugins.boxdb.ImageCheckerUtils;
import au.net.causal.maven.plugins.boxdb.JavaRunner;
import com.google.common.collect.ImmutableList;
import org.codehaus.plexus.util.FileUtils;
import org.codehaus.plexus.util.StringUtils;
import org.eclipse.aether.resolution.DependencyResolutionException;

import javax.sql.DataSource;
import java.io.IOException;
import java.io.InputStreamReader;
import java.io.Reader;
import java.io.StringReader;
import java.lang.reflect.InvocationTargetException;
import java.lang.reflect.Method;
import java.net.URL;
import java.nio.charset.Charset;
import java.nio.charset.StandardCharsets;
import java.nio.file.Files;
import java.nio.file.Path;
import java.sql.Connection;
import java.sql.SQLException;
import java.sql.Statement;
import java.time.Duration;
import java.util.Collection;
import java.util.Collections;
import java.util.List;
import java.util.Locale;
import java.util.concurrent.TimeoutException;

public class DerbyDatabase extends FileBasedDatabase
{
    static final String DERBY_DATABASE_GROUP_ID = "org.apache.derby";
    static final String DERBY_DATABASE_ARTIFACT_ID = "derbynet";

    private static volatile JavaRunner asyncRunner;

    public DerbyDatabase(BoxConfiguration boxConfiguration, ProjectConfiguration projectConfiguration, BoxContext context)
    {
        super(boxConfiguration, projectConfiguration, context);
    }

    @Override
    public boolean exists()
    throws BoxDatabaseException
    {
        if (!Files.exists(getBoxConfiguration().getDatabaseFile()))
            return false;

        //Also check databaseName exists underneath
        Path databaseDir = getBoxConfiguration().getDatabaseFile().resolve(getBoxConfiguration().getDatabaseName());
        return Files.exists(databaseDir);
    }

    @Override
    public boolean isRunning()
    throws BoxDatabaseException
    {
        return asyncRunner != null;
    }

    @Override
    public void start()
    throws BoxDatabaseException
    {
        //Set up some Derby properties - since it's the same JVM we have to set our own system properties
        Path dbDir = getBoxConfiguration().getDatabaseFile().toAbsolutePath();
        getContext().getLog().info("Derby home: " + dbDir);
        System.setProperty("derby.system.home", dbDir.toString());

        try
        {
            //JavaRunner runner = context.createJavaRunner("org.apache.derby.drda.NetworkServerControl", getDerbyDatabaseDependencies());
            //runner.executeAsync("start", "-p", String.valueOf(boxConfiguration.getDatabasePort()), "-noSecurityManager");

            JavaRunner runner = getContext().createJavaRunner("org.apache.derby.impl.drda.NetworkServerControlImpl", getDerbyDatabaseDependencies());
            Class<?> networkServerControlClass = runner.makeClass();
            Object networkServerControl = networkServerControlClass.getConstructor().newInstance();

            String[] args = new String[] {"start", "-p", String.valueOf(getBoxConfiguration().getDatabasePort()), "-noSecurityManager"};

            Method parseArgs = networkServerControlClass.getMethod("parseArgs", String[].class);
            int result = (Integer)parseArgs.invoke(networkServerControl, new Object[] {args});

            Method executeWork = networkServerControlClass.getMethod("executeWork", int.class);
            runner.runMethodAsync(executeWork, networkServerControl, result);

            //NetworkServerControlImpl networkServerControl = new NetworkServerControlImpl();
            //int result = networkServerControl.parseArgs(args);
            //networkServerControl.executeWork(result);

            asyncRunner = runner;
        }
        catch (DependencyResolutionException e)
        {
            throw new BoxDatabaseException("Error resolving dependencies for Derby: " + e, e);
        }
        catch (ClassNotFoundException | NoSuchMethodException | IOException | InstantiationException | IllegalAccessException e)
        {
            throw new BoxDatabaseException("Error executing Derby: " + e, e);
        }
        catch (InvocationTargetException e)
        {
            throw new BoxDatabaseException("Exception occured executing Derby: " + e.getTargetException(), e);
        }
    }

    @Override
    public void stop()
    throws BoxDatabaseException
    {
        try
        {
            //JavaRunner runner = context.createJavaRunner("org.apache.derby.drda.NetworkServerControl", getDerbyDatabaseDependencies());
            //runner.execute("shutdown", "-p", String.valueOf(boxConfiguration.getDatabasePort()));

            JavaRunner runner = getContext().createJavaRunner("org.apache.derby.impl.drda.NetworkServerControlImpl", getDerbyDatabaseDependencies());
            Class<?> networkServerControlClass = runner.makeClass();
            Object networkServerControl = networkServerControlClass.getConstructor().newInstance();

            String[] args = new String[] {"shutdown", "-p", String.valueOf(getBoxConfiguration().getDatabasePort())};

            Method parseArgs = networkServerControlClass.getMethod("parseArgs", String[].class);
            int result = (Integer)parseArgs.invoke(networkServerControl, new Object[] {args});

            Method executeWork = networkServerControlClass.getMethod("executeWork", int.class);
            runner.runMethod(executeWork, networkServerControl, result);

            try
            {
                asyncRunner.waitForAsyncCompletion();
                asyncRunner = null;
            }
            catch (InterruptedException e)
            {
                throw new BoxDatabaseException("Interrupted waiting for shutdown", e);
            }
        }
        catch (DependencyResolutionException e)
        {
            throw new BoxDatabaseException("Error resolving dependencies for Derby: " + e, e);
        }
        catch (ClassNotFoundException | NoSuchMethodException | IOException | InstantiationException | IllegalAccessException e)
        {
            throw new BoxDatabaseException("Error executing Derby: " + e, e);
        }
        catch (InvocationTargetException e)
        {
            throw new BoxDatabaseException("Exception occured executing Derby: " + e.getTargetException(), e);
        }
    }

    @Override
    public void createAndStart()
    throws BoxDatabaseException
    {
        start();
    }

    @Override
    public void delete()
    throws BoxDatabaseException
    {
        getContext().getLog().info("Deleting Derby DB " + getBoxConfiguration().getDatabaseFile());

        try
        {
            FileUtils.deleteDirectory(getBoxConfiguration().getDatabaseFile().toFile());
        }
        catch (IOException e)
        {
            throw new BoxDatabaseException("Failed to delete derby database in " + getBoxConfiguration().getDatabaseFile() + ": " + e, e);
        }
    }

    @Override
    public void deleteImage()
    throws BoxDatabaseException
    {
        //Not going to wipe derby from local repo, so do nothing
    }

    @Override
    public JdbcConnectionInfo jdbcConnectionInfo(DatabaseTarget target)
    throws BoxDatabaseException
    {
        String uri = "jdbc:derby://localhost:" + getBoxConfiguration().getDatabasePort() + "/" + getBoxConfiguration().getDatabaseName();
        return new JdbcConnectionInfo(uri,
                        target.user(getBoxConfiguration()), target.password(getBoxConfiguration()),
                        "localhost", getBoxConfiguration().getDatabasePort());
    }

    @Override
    public JdbcDriverInfo jdbcDriverInfo()
    throws BoxDatabaseException
    {
        return new JdbcDriverInfo(new RunnerDependency(DERBY_DATABASE_GROUP_ID, "derbyclient", getBoxConfiguration().getDatabaseVersion()), "org.apache.derby.jdbc.ClientDriver");
    }

    @Override
    public void waitUntilStarted(Duration maxTimeToWait)
    throws TimeoutException, BoxDatabaseException
    {
        DatabaseUtils.waitUntilTcpPortResponding(maxTimeToWait, this, getContext());
    }

    @Override
    public void executeScript(URL script, DatabaseTarget targetDatabase, Duration timeout)
    throws IOException, SQLException, BoxDatabaseException
    {
        try (Reader reader = new InputStreamReader(script.openStream(), StandardCharsets.UTF_8))
        {
            executeScript(reader, targetDatabase, timeout);
        }
    }

    protected DataSourceBuilder dataSourceBuilder(DatabaseTarget target, boolean create)
    throws BoxDatabaseException
    {
        DataSourceBuilder builder = new DataSourceBuilder(getContext())
                                            .dataSourceClassName("org.apache.derby.jdbc.ClientDataSource")
                                            .dependencies(jdbcDriverInfo().getDependencies())
                                            .configureDataSource("setDatabaseName", String.class, getBoxConfiguration().getDatabaseName())
                                            .configureDataSource("setPortNumber", int.class, getBoxConfiguration().getDatabasePort())
                                            .configureDataSource("setUser", String.class, target.user(getBoxConfiguration()))
                                            .configureDataSource("setPassword", String.class, target.password(getBoxConfiguration()));
        
        if (create) 
        {
            if (!StringUtils.isEmpty(getBoxConfiguration().getDatabaseCollation()))
            {
                String[] splitCollation = getBoxConfiguration().getDatabaseCollation().split(";", 2);
                String locale = splitCollation[0];
                String collationString = "TERRITORY_BASED";
                if (splitCollation.length > 1)
                    collationString = collationString + ":" + splitCollation[1];

                String connectionAttributes = "collation=" + collationString + ";" + "territory=" + locale;
                getContext().getLog().debug("Derby connection attributes: " + connectionAttributes);
                builder.configureDataSource("setConnectionAttributes", String.class, connectionAttributes);
            }
            
            if (!StringUtils.isEmpty(getBoxConfiguration().getDatabaseEncoding()) && 
                !getBoxConfiguration().getDatabaseEncoding().equalsIgnoreCase(CommonEncoding.UNICODE.name()))
            {
                throw new BoxDatabaseException("Unsupported database encoding '" + getBoxConfiguration().getDatabaseEncoding() + 
                            "', only '" + CommonEncoding.UNICODE.name().toLowerCase(Locale.ENGLISH) + "' is supported.");
            }
            
            builder.configureDataSource("setCreateDatabase", String.class, "create");
        }
        
        return builder;
    }
    
    @Override
    public Connection createJdbcConnection(DatabaseTarget targetDatabase)
    throws SQLException, BoxDatabaseException, IOException
    {
        return dataSourceBuilder(targetDatabase, false).create().getConnection();
    }

    @Override
    public void executeScript(Reader scriptReader, DatabaseTarget targetDatabase, Duration timeout)
    throws IOException, SQLException, BoxDatabaseException
    {
        executeJdbcScript(scriptReader, targetDatabase);
    }

    @Override
    public void executeSql(String sql, DatabaseTarget targetDatabase, Duration timeout)
    throws IOException, SQLException, BoxDatabaseException
    {
        try (Reader reader = new StringReader(sql))
        {
            executeScript(reader, targetDatabase, timeout);
        }
    }

    @Override
    public String getName()
    {
        return getBoxConfiguration().getDatabaseName();
    }

    @Override
    public void configureNewDatabase()
    throws IOException, SQLException, BoxDatabaseException
    {
        DataSource dataSource = dataSourceBuilder(DatabaseTarget.ADMIN, true).create();
        try (Connection con = dataSource.getConnection(); Statement s = con.createStatement())
        {
            getContext().getLog().info("Created Derby database " + getName());

            String adminUser = getBoxConfiguration().getAdminUser();
            String adminPassword = getBoxConfiguration().getAdminPassword();
            
            /* This will work for older versions
            s.executeUpdate("CALL SYSCS_UTIL.SYSCS_SET_DATABASE_PROPERTY('derby.connection.requireAuthentication', 'true')");
            s.executeUpdate("CALL SYSCS_UTIL.SYSCS_SET_DATABASE_PROPERTY('derby.authentication.provider', 'BUILTIN')");
            s.executeUpdate("CALL SYSCS_UTIL.SYSCS_SET_DATABASE_PROPERTY('derby.user." + user + "', '" + password + "')");
            s.executeUpdate("CALL SYSCS_UTIL.SYSCS_SET_DATABASE_PROPERTY('derby.database.propertiesOnly', 'true')");
            */
            
            s.executeUpdate("CALL SYSCS_UTIL.SYSCS_CREATE_USER('" + adminUser + "', '" + adminPassword + "')");
            
            String dbUser = getBoxConfiguration().getDatabaseUser();
            String dbPassword = getBoxConfiguration().getDatabasePassword();
            if (!adminUser.equals(dbUser))
                s.executeUpdate("CALL SYSCS_UTIL.SYSCS_CREATE_USER('" + dbUser + "', '" + dbPassword + "')");
            
            //Need to restart derby for these settings to take effect
            stop();
            start();
            try
            {
                waitUntilStarted(getProjectConfiguration().getScriptTimeout());
            }
            catch (TimeoutException e)
            {
                throw new BoxDatabaseException("Timeout waiting for database to restart.", e);
            }
        }
    }

    @Override
    public List<? extends DatabaseLog> logFiles() throws BoxDatabaseException, IOException 
    {
        Path derbyLog = getBoxConfiguration().getDatabaseFile().resolve("derby.log");
        
        //Derby uses the platform's encoding for its log (I assume)
        return Collections.singletonList(new FileDatabaseLog("derby.log", derbyLog, Charset.defaultCharset()));
    }

    @Override
    public boolean databaseIsReady(SQLException ex)
    {
        //See: https://db.apache.org/derby/docs/10.14/ref/rrefexcept71493.html
        //40000 - disconnect error
        //08004 - connection refused

        //Disconnect error implies that a connection was managed first (so it's not an I/O or TCP connectivity error)
        //Testing indicates that socket errors give 08001 errors instead
        //The typical error we get for fresh databases will be:
        // "The connection was refused because the database <databaseName> was not found."
        //which is expected since no database has been created yet when starting up a fresh database
        if ("08004".equals(ex.getSQLState()) && ex.getErrorCode() == 40000)
            return true;

        //All other cases, assume it's not up yet
        return false;
    }

    /**
     * @return Derby server dependencies.
     */
    protected List<? extends RunnerDependency> getDerbyDatabaseDependencies()
    {
        return Collections.singletonList(new RunnerDependency(DERBY_DATABASE_GROUP_ID, DERBY_DATABASE_ARTIFACT_ID,
                                                              getBoxConfiguration().getDatabaseVersion()));
    }

    /**
     * @return all database and JDBC driver dependencies.
     */
    protected List<? extends RunnerDependency> getAllDerbyDependencies()
    throws BoxDatabaseException
    {
        return ImmutableList.<RunnerDependency>builder()
                //The database itself
                .addAll(getDerbyDatabaseDependencies())
                //JDBC driver / client
                .addAll(jdbcDriverInfo().getDependencies())
                .build();
    }

    @Override
    public void prepareImage()
    throws BoxDatabaseException
    {
        try
        {
            DependencyUtils.resolveDependencies(getAllDerbyDependencies(),
                                                getContext().getRepositorySystem(),
                                                getContext().getRepositorySystemSession(),
                                                getContext().getRemoteRepositories());
        }
        catch (DependencyResolutionException e)
        {
            throw new BoxDatabaseException("Failed to download dependencies for database: " + e.getMessage(), e);
        }
    }

    @Override
    public Collection<? extends ImageComponent> checkImage()
    throws BoxDatabaseException
    {
        return Collections.singleton(ImageCheckerUtils.checkImageUsingMavenDependencies("Derby database", getContext(), getAllDerbyDependencies()));
    }
}
