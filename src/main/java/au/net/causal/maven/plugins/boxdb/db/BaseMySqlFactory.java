package au.net.causal.maven.plugins.boxdb.db;

import java.io.IOException;
import java.util.List;

/**
 * Base factory for MySQL and MariaDB databases, since they have many things in common.
 */
public abstract class BaseMySqlFactory extends DockerDatabaseFactory
{
    protected BaseMySqlFactory(String name)
    {
        super(name);
    }

    @Override
    protected void initializeDefaults(BoxConfiguration boxConfiguration)
    {
        if (boxConfiguration.getDatabaseUser() == null)
            boxConfiguration.setDatabaseUser("app");
        if (boxConfiguration.getDatabasePassword() == null)
            boxConfiguration.setDatabasePassword(boxConfiguration.getDatabaseUser());
        if (boxConfiguration.getAdminUser() == null)
            boxConfiguration.setAdminUser("root");
        if (boxConfiguration.getAdminPassword() == null)
            boxConfiguration.setAdminPassword("mysqlbox");
        if (boxConfiguration.getDatabaseName() == null)
            boxConfiguration.setDatabaseName("app");
        if (boxConfiguration.getDatabasePort() <= 0)
            boxConfiguration.setDatabasePort(3307);

        EncodingTranslator encodingTranslator = new EncodingTranslator("utf8", this::mapCommonEncoding);
        encodingTranslator.processEncoding(boxConfiguration);
        CollationTranslator collationTranslator = new CollationTranslator(boxConfiguration.getDatabaseEncoding() + "_bin", col -> mapCommonCollation(col, boxConfiguration.getDatabaseEncoding()));
        collationTranslator.processCollation(boxConfiguration);
        
        super.initializeDefaults(boxConfiguration);
    }

    private String mapCommonEncoding(CommonEncoding encoding)
    {
        switch (encoding)
        {
            case UNICODE:
                return "utf8";
            case ASCII:
                return "ascii";
            default:
                throw new Error("Unknown common encoding: " + encoding);
        }
    }

    private String mapCommonCollation(CommonCollation collation, String normalizedEncoding)
    {
        switch (collation)
        {
            case BINARY:
                return normalizedEncoding + "_bin";
            case CASE_INSENSITIVE:
                return normalizedEncoding + "_general_ci";
            default:
                throw new Error("Unknown common collation: " + collation);
        }
    }

    /**
     * @return the name of the Docker repository used when accessing the remote registry.
     */
    protected String dockerRepositoryName()
    {
        return "library/" + name();
    }

    @Override
    public List<String> availableVersions(ProjectConfiguration projectConfiguration, BoxContext context)
    throws BoxDatabaseException
    {
        try
        {
            return dockerRegistry().readTags(dockerRepositoryName());
        }
        catch (IOException e)
        {
            throw new BoxDatabaseException("Error reading tags: " + e.getMessage(), e);
        }
    }
}
