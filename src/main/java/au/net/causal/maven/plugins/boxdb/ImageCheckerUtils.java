package au.net.causal.maven.plugins.boxdb;

import au.net.causal.maven.plugins.boxdb.db.BoxContext;
import au.net.causal.maven.plugins.boxdb.db.BoxDatabaseException;
import au.net.causal.maven.plugins.boxdb.db.ImageComponent;
import au.net.causal.maven.plugins.boxdb.db.ImageComponent.ImageStatus;
import au.net.causal.maven.plugins.boxdb.db.RunnerDependency;
import org.eclipse.aether.DefaultRepositoryCache;
import org.eclipse.aether.DefaultRepositorySystemSession;
import org.eclipse.aether.DefaultSessionData;
import org.eclipse.aether.resolution.DependencyResolutionException;

import java.util.Collection;
import java.util.Collections;
import java.util.List;
import java.util.stream.Collectors;

/**
 * Tools for checking images for databases.
 */
public final class ImageCheckerUtils
{
    private static final String MAVEN_DEPENDENCY_TYPE = "Maven dependency";

    /**
     * Private constructor to prevent instantiation.
     */
    private ImageCheckerUtils()
    {
    }

    /**
     * Checks whether all the specified dependencies are in the local Maven repository, or if not, whether they
     * exist locally.  The result of of this method is the status of the 'worst-off' dependency, for example, if all
     * but one dependency exists locally, but one exists remotely, the returned status is
     * {@link ImageStatus#NOT_DOWNLOADED}.
     *
     * @param resourceName name of the resource, used for labelling purposes.
     * @param context box context.
     * @param databaseDependencies the dependencies to check.
     *
     * @return the status for all the dependencies.
     *
     * @throws BoxDatabaseException if an error occurs accessing the remote repository, such as connectivity issues.
     */
    public static ImageComponent checkImageUsingMavenDependencies(String resourceName,
                                                                  BoxContext context,
                                                                  List<? extends RunnerDependency> databaseDependencies)
    throws BoxDatabaseException
    {
        String fullResourceName = resourceName + " " +
                                    databaseDependencies.stream()
                                                        .map(dep -> dep.getGroupId() + ":" + dep.getArtifactId() + ":" + dep.getVersion())
                                                        .collect(Collectors.joining(", ", "[", "]"));

        boolean existsLocally;
        try
        {
            //Resolve locally only first

            //Because we're hacking remote repositories by supplying an empty list, this screws up repository system
            //session caching in Maven (potentially caching POMs without dependency info if they don't exist in local repo)
            //so don't use the cache for this one
            DefaultRepositorySystemSession uncachedSession = new DefaultRepositorySystemSession(context.getRepositorySystemSession());
            uncachedSession.setCache(new DefaultRepositoryCache());
            uncachedSession.setData(new DefaultSessionData());

            DependencyUtils.resolveDependencies(databaseDependencies,
                                                context.getRepositorySystem(),
                                                uncachedSession,
                                                Collections.emptyList() /* no remote repositories */);
            existsLocally = true;
        }
        catch (DependencyResolutionException e)
        {
            //This is OK, it means we don't have them locally
            //Don't have to tell the difference between not found 404 and connectivity errors because it's local!
            existsLocally = false;
        }

        context.getLog().debug("Derby dependencies exist locally: " + existsLocally);

        //While it's technically possible to check if the remote exists despite having local, it's hard subverting
        //the local repo using the standard Maven API.  At the moment, if we have a version in the local repo we
        //will just assume the remote exists as well and be done with it
        if (existsLocally)
            return new ImageComponent(MAVEN_DEPENDENCY_TYPE, fullResourceName, ImageStatus.DOWNLOADED);

        //If we get here, we don't have local copy so check if it exists remotely
        Collection<? extends RunnerDependency> missing = DependencyUtils.findMissingRemoteDependencies(databaseDependencies,
                                                                                                       context.getRepositorySystem(),
                                                                                                       context.getRepositorySystemSession(),
                                                                                                       context.getRemoteRepositories());

        //If there are no missing dependencies, they all exist remotely
        if (missing.isEmpty())
            return new ImageComponent(MAVEN_DEPENDENCY_TYPE, fullResourceName, ImageStatus.NOT_DOWNLOADED);
        else
        {
            String detail = missing.stream()
                                   .map(d -> d.getGroupId() + ":" + d.getArtifactId() + ":" + d.getVersion())
                                   .collect(Collectors.joining(", "));
            return new ImageComponent(MAVEN_DEPENDENCY_TYPE, fullResourceName, ImageStatus.NOT_FOUND, detail);
        }
    }
}
