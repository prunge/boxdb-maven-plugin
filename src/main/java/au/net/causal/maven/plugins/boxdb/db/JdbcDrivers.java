package au.net.causal.maven.plugins.boxdb.db;

/**
 * Shared JDBC driver dependencies.
 *
 * In a separate class so that they can be reused across different box database implementations.
 */
final class JdbcDrivers
{
    /**
     * Private constructor to prevent instantiation.
     */
    private JdbcDrivers()
    {
    }

    /**
     * @return Postgres JDBC driver.
     */
    public static JdbcDriverInfo postgresDriver()
    {
        //Drivers are backward/forward compatible so just use latest independent of version of actual database being used
        return new JdbcDriverInfo(new RunnerDependency("org.postgresql", "postgresql", "42.7.3"), "org.postgresql.Driver");
    }

    /**
     * @return a Postgres data source builder.
     */
    public static DataSourceBuilder postgresDataSourceBuilder(JdbcConnectionInfo jdbcConnectionInfo,
                                                              JdbcDriverInfo jdbcDriverInfo,
                                                              BoxContext context)
    {
        return new DataSourceBuilder(context)
                .dataSourceClassName("org.postgresql.ds.PGSimpleDataSource")
                .dependencies(jdbcDriverInfo.getDependencies())
                .configureDataSource("setUrl", String.class, jdbcConnectionInfo.getUri())
                .configureDataSource("setUser", String.class, jdbcConnectionInfo.getUser())
                .configureDataSource("setPassword", String.class, jdbcConnectionInfo.getPassword());
    }
}
