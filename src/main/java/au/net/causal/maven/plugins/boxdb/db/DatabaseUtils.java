package au.net.causal.maven.plugins.boxdb.db;

import com.google.common.collect.ImmutableList;
import io.fabric8.maven.docker.wait.PreconditionFailedException;
import io.fabric8.maven.docker.wait.TcpPortChecker;
import io.fabric8.maven.docker.wait.WaitChecker;
import io.fabric8.maven.docker.wait.WaitUtil;
import io.fabric8.maven.docker.wait.WaitUtil.Precondition;

import java.time.Duration;
import java.util.concurrent.TimeoutException;

public final class DatabaseUtils
{
    private DatabaseUtils()
    {
    }

    public static void waitUntilTcpPortResponding(Duration maxTimeToWait, BoxDatabase box, BoxContext context)
    throws TimeoutException, BoxDatabaseException
    {
        int waitMillis;
        if (maxTimeToWait == null)
            waitMillis = Integer.MAX_VALUE;
        else
            waitMillis = Math.toIntExact(Math.min(maxTimeToWait.toMillis(), Integer.MAX_VALUE));

        JdbcConnectionInfo jdbcInfo = box.jdbcConnectionInfo(DatabaseTarget.ADMIN);
        WaitChecker checker = new TcpPortChecker(jdbcInfo.getHost(), ImmutableList.of(jdbcInfo.getPort()));
        context.getLog().info("Waiting for database to start up...");
        try
        {
            WaitUtil.wait(new DatabaseRunningPrecondition(box, context), waitMillis, checker);
        }
        catch (PreconditionFailedException e)
        {
            throw new BoxDatabaseException("Error occurred while waiting for database to start up: " + e, e);
        }
    }

    public static class DatabaseRunningPrecondition implements Precondition
    {
        private final BoxDatabase database;
        private final BoxContext context;

        public DatabaseRunningPrecondition(BoxDatabase database, BoxContext context)
        {
            this.database = database;
            this.context = context;
        }

        @Override
        public boolean isOk()
        {
            try
            {
                return database.isRunning();
            }
            catch (BoxDatabaseException e)
            {
                context.getLog().error("Error retrieving status while waiting: " + e, e);
                return false;
            }
        }

        @Override
        public void cleanup()
        {
            //Nothing to clean up
        }
    }
}
