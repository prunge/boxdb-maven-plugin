package au.net.causal.maven.plugins.boxdb;

import au.net.causal.maven.plugins.boxdb.db.BoxDatabase;
import au.net.causal.maven.plugins.boxdb.db.BoxDatabaseException;
import au.net.causal.maven.plugins.boxdb.db.DatabaseLog;
import au.net.causal.maven.plugins.boxdb.db.DockerService;
import io.fabric8.maven.docker.access.DockerAccessException;
import org.apache.maven.plugin.MojoExecutionException;
import org.apache.maven.plugin.MojoFailureException;
import org.apache.maven.plugins.annotations.Mojo;
import org.apache.maven.plugins.annotations.Parameter;

import java.io.File;
import java.io.IOException;
import java.io.Writer;
import java.nio.charset.StandardCharsets;
import java.nio.file.Files;
import java.nio.file.Path;
import java.nio.file.StandardOpenOption;

/**
 * Save logs of the database to a directory on the local filesystem.
 */
@Mojo(name="savelogs", requiresProject = false)
public class SaveDatabaseLogsMojo extends AbstractDatabaseMojo 
{
    /**
     * The directory to save database logs to.
     */
    @Parameter(property = "boxdb.log.directory", defaultValue = "${project.build.directory}/dblogs", required = true)
    private File logDirectory;
    
    @Override
    protected void executeInternal(ExceptionalSupplier<DockerService, BoxDatabaseException> service) 
    throws MojoExecutionException, MojoFailureException, DockerAccessException 
    {
        try 
        {
            Path logPath = logDirectory.toPath();
            Files.createDirectories(logPath);
            
            BoxDatabase database = database(service);
            for (DatabaseLog dbLog : database.logFiles())
            {
                Path localLogFile = logPath.resolve(dbLog.getName());
                getLog().info("Saving log " + dbLog.getName() + " to " + localLogFile.toAbsolutePath().toString());
                
                try (Writer w = Files.newBufferedWriter(localLogFile, StandardCharsets.UTF_8, StandardOpenOption.CREATE)) 
                {
                    dbLog.save(w);
                }
            }
        }
        catch (BoxDatabaseException | IOException e)
        {
            throw new MojoExecutionException("Failed to save database logs: " + e, e);
        }
    }
}
