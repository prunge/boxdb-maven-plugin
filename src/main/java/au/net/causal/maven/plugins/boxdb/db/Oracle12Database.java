package au.net.causal.maven.plugins.boxdb.db;

import au.net.causal.maven.plugins.boxdb.ImageCheckerUtils;
import au.net.causal.maven.plugins.boxdb.db.DatabaseUtils.DatabaseRunningPrecondition;
import com.google.common.collect.ImmutableList;
import io.fabric8.maven.docker.config.Arguments;
import io.fabric8.maven.docker.config.RunImageConfiguration;
import io.fabric8.maven.docker.wait.HttpPingChecker;
import io.fabric8.maven.docker.wait.PreconditionFailedException;
import io.fabric8.maven.docker.wait.WaitChecker;
import io.fabric8.maven.docker.wait.WaitUtil;
import org.codehaus.plexus.util.IOUtil;

import java.io.IOException;
import java.io.InputStream;
import java.io.OutputStream;
import java.net.URL;
import java.nio.charset.StandardCharsets;
import java.nio.file.Files;
import java.nio.file.Path;
import java.nio.file.StandardCopyOption;
import java.sql.SQLException;
import java.time.Duration;
import java.util.Collection;
import java.util.List;
import java.util.concurrent.TimeoutException;

public class Oracle12Database extends OracleDatabase
{
    public Oracle12Database(BoxConfiguration boxConfiguration, ProjectConfiguration projectConfiguration,
                            BoxContext context, DockerRegistry dockerRegistry, String dockerImageName)
    throws IOException
    {
        super(boxConfiguration, projectConfiguration, context, dockerRegistry, dockerImageName);

        Path startupScriptFile = getHostScriptDirectory().resolve("oracle12-boxdb-startup.sh");
        URL startupScriptResource = Oracle12Database.class.getResource("oracle12-boxdb-startup.sh");
        try (OutputStream os = Files.newOutputStream(startupScriptFile);
             InputStream is = startupScriptResource.openStream())
        {
            IOUtil.copy(is, os);
        }
    }

    @Override
    protected String oracleHomePath()
    {
        return "/u01/app/oracle-product/12.1.0/xe/bin/";
    }

    @Override
    protected void configureRunImage(RunImageConfiguration.Builder builder)
    {
        super.configureRunImage(builder);
        builder.ports(ImmutableList.of(
                getBoxConfiguration().getDatabasePort() + ":" + containerDatabasePort(),
                getWebConsolePort() + ":8080"));
        builder.cmd(new Arguments(ImmutableList.of("/bin/sh", "-c", "/bin/bash /data/scripts/oracle12-boxdb-startup.sh")));
    }

    protected int getWebConsolePort()
    {
        return getBoxConfiguration().getDatabasePort() + 1;
    }

    @Override
    public void waitUntilStarted(Duration maxTimeToWait) throws TimeoutException, BoxDatabaseException
    {
        long startTime = System.currentTimeMillis();
        long maxEndTime = startTime + maxTimeToWait.toMillis();

        getContext().getLog().info("Note: Oracle 12 can take over five minutes to start up when first run, please be patient...");
        super.waitUntilStarted(maxTimeToWait);

        int waitMillis = Math.toIntExact(maxEndTime - System.currentTimeMillis());

        //Check HTTP endpoint instead of just TCP because TCP sockets are bugged with some dockers
        WaitChecker checker = new HttpPingChecker("http://" + getContext().getDockerHostAddress() + ":" + getWebConsolePort(), "GET", "401", getContext().getDockerLogger());
        getContext().getLog().debug("Waiting for Web console to start up...");
        try
        {
            WaitUtil.wait(new DatabaseRunningPrecondition(this, getContext()), waitMillis, checker);
        }
        catch (PreconditionFailedException e)
        {
            throw new BoxDatabaseException("Error occurred while waiting for database to start up: " + e, e);
        }

        //Database still might not be fully started up, so further wait until a backup can be made
        waitUntilABackupWorks(maxEndTime);

        long time = System.currentTimeMillis() - startTime;
        getContext().getLog().info("Oracle 12 startup completed in " + time / 1000.0 + " seconds.");
    }

    /**
     * Some systems give some weird errors like 'a job is not attached to this session via the specified handle'
     * because the database still hasn't fully finished setting itself up.  So repeatedly try taking a backup
     * accepting failure until one works.
     *
     * @param maxEndTime the maximum clock time in milliseconds before timing out.
     *
     * @throws BoxDatabaseException if a non-backup-failure error occurs.
     * @throws TimeoutException if maxEndTime is reached and backups are still failing.
     */
    private void waitUntilABackupWorks(long maxEndTime)
    throws BoxDatabaseException, TimeoutException
    {
        //Still might not be fully started up - backups can fail with sporadic errors before full initialization
        //So try to do dummy backups of a known small schema until there are no more failures
        boolean backupSuccessful = false;
        while (!backupSuccessful)
        {
            try
            {
                Path dummyBackupFile = getContext().getTempDirectory().resolve("startup-backup.dmp");
                //The 'SCOTT' schema is one that exists in the demo database in the docker image
                //Need to pick a schema that is not empty that already exits before the user's one or else
                //the backup fails
                backup(dummyBackupFile, DatabaseTarget.ADMIN, "SCOTT", true);
                backupSuccessful = true;
            }
            catch (SQLException e)
            {
                //Can happen if database not fully started up yet
                getContext().getLog().debug("Backup failed because database is not yet ready: " + e, e);
                if (System.currentTimeMillis() > maxEndTime)
                    throw new TimeoutException("Database not fully started up in time.");

                try
                {
                    Thread.sleep(1000L);
                }
                catch (InterruptedException ex)
                {
                    throw new BoxDatabaseException(ex);
                }
            }
            catch (IOException e)
            {
                throw new BoxDatabaseException(e);
            }
        }
    }

    @Override
    public void restore(Path backupFile)
    throws BoxDatabaseException, IOException, SQLException
    {
        //Copy backup file to host script directory so Oracle can access it
        Path mountedBackupFile = Files.createTempFile(getHostScriptDirectory(), "backup", ".dmp");
        Files.copy(backupFile, mountedBackupFile, StandardCopyOption.REPLACE_EXISTING);

        //Shell script to create dump directory if needed and set it up
        runShellScript(Oracle12Database.class.getResource("oracle12-pre-backup.sh"));

        //and copy the backup dmp file to the dump directory
        runShellScript(Oracle12Database.class.getResource("oracle12-pre-restore.sh"));

        //Set up directory and give database user permission to use it
        runFilteredScript("oracle12-prepare-backup.sql");

        //Run tool
        String args = "schemas=" + getBoxConfiguration().getDatabaseUser() +
                " directory=backup_dir dumpfile=" + mountedBackupFile.getFileName().toString() +
                " logfile=" + mountedBackupFile.getFileName().toString() + ".log";
        executeImpDp(args, DatabaseTarget.USER, getProjectConfiguration().getBackupTimeout());

        //Clean up dmp file
        runShellScript(Oracle12Database.class.getResource("oracle12-post-restore.sh"));
    }

    @Override
    public void backup(Path backupFile, BackupFileTypeHint backupFileTypeHint)
            throws BoxDatabaseException, IOException, SQLException
    {
        backup(backupFile, DatabaseTarget.USER, getBoxConfiguration().getDatabaseUser(), false);
    }

    private void backup(Path backupFile, DatabaseTarget target, String schema, boolean silent)
    throws BoxDatabaseException, IOException, SQLException
    {
        //This is the backup file created by Oracle, move it at the end
        //If the file name specified has no extension then it gets ".dmp" by Oracle tool
        //Thanks Oracle
        String oracleFileName = backupFile.getFileName().toString();
        if (!oracleFileName.contains("."))
            oracleFileName = oracleFileName + ".dmp";

        Path targetFile = getHostScriptDirectory().resolve(oracleFileName);

        //Run shell script to initialize /data/dump directory
        //We can't use /data/scripts because permissions might be wrong on some docker instances
        //and oracle user doesn't have write access to it
        runShellScript(Oracle12Database.class.getResource("oracle12-pre-backup.sh"));

        //Set up directory and give database user permission to use it
        if (target == DatabaseTarget.ADMIN)
            runFilteredScript("oracle12-prepare-backup-admin.sql", silent);
        else
            runFilteredScript("oracle12-prepare-backup.sql", silent);

        //Run the dump tool
        String args = "schemas=" + schema +
                " directory=backup_dir dumpfile=" + backupFile.getFileName().toString() +
                " logfile=" + backupFile.getFileName().toString() + ".log";
        executeExpDp(args, target, getProjectConfiguration().getBackupTimeout(), silent);

        //Now run shell script to move file to proper location
        runShellScript(Oracle12Database.class.getResource("oracle12-post-backup.sh"));

        //Move backup file to target
        Files.move(targetFile, backupFile, StandardCopyOption.REPLACE_EXISTING);
    }

    @Override
    public List<? extends DatabaseLog> logFiles() throws BoxDatabaseException, IOException 
    {
        return logFilesInContainer(true, StandardCharsets.UTF_8, "/u01/app/oracle/cfgtoollogs/dbca/xe/", 
                    "xe.log", "postDBCreation.log", "trace.log");
    }

    @Override
    public Collection<? extends ImageComponent> checkImage()
    throws BoxDatabaseException
    {
        ImageComponent jdbcDriverComponent = ImageCheckerUtils.checkImageUsingMavenDependencies("JDBC driver",
                                                                                                getContext(),
                                                                                                jdbcDriverInfo().getDependencies());
        ImageComponent dockerDatabaseComponent = checkDockerDatabaseImage(Oracle12Factory.ORACLE_12_DOCKER_REPOSITORY,
                                                                          Oracle12Factory.ORACLE_12_DOCKER_TAG);

        return ImmutableList.of(jdbcDriverComponent, dockerDatabaseComponent);
    }
}
