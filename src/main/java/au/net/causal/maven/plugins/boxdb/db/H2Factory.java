package au.net.causal.maven.plugins.boxdb.db;

import au.net.causal.maven.plugins.boxdb.DependencyUtils;
import org.eclipse.aether.version.Version;

import java.util.List;
import java.util.Objects;
import java.util.stream.Collectors;

@BoxDbBundled
public class H2Factory extends FileBasedDatabaseFactory
{
    public H2Factory()
    {
        super("h2");
    }

    @Override
    public H2Database create(BoxConfiguration boxConfiguration, ProjectConfiguration projectConfiguration, BoxContext context)
    throws BoxDatabaseException
    {
        Objects.requireNonNull(boxConfiguration, "boxConfiguration == null");
        Objects.requireNonNull(context, "context == null");

        initializeDefaults(boxConfiguration, projectConfiguration, context);

        return new H2Database(boxConfiguration, projectConfiguration, context);
    }

    protected void initializeDefaults(BoxConfiguration boxConfiguration, ProjectConfiguration projectConfiguration, BoxContext context)
    throws BoxDatabaseException
    {
        super.initializeDefaults(boxConfiguration, projectConfiguration, context);

        //2.3 and later requires Java 11, so default to most recent H2 version that still supports Java 8
        if (boxConfiguration.getDatabaseVersion() == null)
            boxConfiguration.setDatabaseVersion("2.2.224");
        if (boxConfiguration.getDatabasePort() <= 0)
            boxConfiguration.setDatabasePort(9092);

        CollationTranslator collationTranslator = new CollationTranslator(null, this::mapCommonCollation);
        collationTranslator.processCollation(boxConfiguration);
    }

    private String mapCommonCollation(CommonCollation collation)
    {
        switch (collation)
        {
            case BINARY:
                return null;
            case CASE_INSENSITIVE:
                return "en_US;PRIMARY";
            default:
                throw new Error("Unknown common collation: " + collation);
        }
    }

    @Override
    public List<String> availableVersions(ProjectConfiguration projectConfiguration, BoxContext context)
    throws BoxDatabaseException
    {
        List<? extends Version> versions = DependencyUtils.findAvailableVersions(H2Database.H2_DATABASE_GROUP_ID,
                                                                                 H2Database.H2_DATABASE_ARTIFACT_ID,
                                                                                 "jar", context.getRepositorySystem(),
                                                                                 context.getRepositorySystemSession(),
                                                                                 context.getRemoteRepositories());
        return versions.stream().map(Version::toString).collect(Collectors.toList());
    }
}
