package au.net.causal.maven.plugins.boxdb;

import au.net.causal.maven.plugins.boxdb.db.BoxDatabase;
import au.net.causal.maven.plugins.boxdb.db.BoxDatabaseException;
import au.net.causal.maven.plugins.boxdb.db.DockerService;
import io.fabric8.maven.docker.access.DockerAccessException;
import org.apache.maven.plugin.MojoExecutionException;
import org.apache.maven.plugins.annotations.LifecyclePhase;
import org.apache.maven.plugins.annotations.Mojo;

/**
 * Stops a previously started database.
 */
@Mojo(name="stop", defaultPhase = LifecyclePhase.POST_INTEGRATION_TEST, requiresProject = false)
public class StopMojo extends AbstractDatabaseMojo
{
    @Override
    protected void executeInternal(ExceptionalSupplier<DockerService, BoxDatabaseException> dockerService)
    throws DockerAccessException, MojoExecutionException
    {
        try
        {
            BoxDatabase boxDatabase = database(dockerService);
            stopDatabaseContainer(boxDatabase);
        }
        catch (BoxDatabaseException e)
        {
            throw new MojoExecutionException(e.getMessage(), e);
        }
    }
}
