package au.net.causal.maven.plugins.boxdb.db;

import au.net.causal.maven.plugins.boxdb.ScriptReaderRunner;
import org.apache.maven.plugin.MojoExecutionException;
import org.eclipse.aether.util.version.GenericVersionScheme;
import org.eclipse.aether.version.InvalidVersionSpecificationException;
import org.eclipse.aether.version.Version;

import java.io.IOException;
import java.net.URL;
import java.sql.Connection;
import java.sql.SQLException;
import java.util.Arrays;

public class MariaDbDatabase extends BaseMySqlDatabase
{
    private final boolean version11OrGreater;

    public MariaDbDatabase(BoxConfiguration boxConfiguration, ProjectConfiguration projectConfiguration,
                           BoxContext context, DockerRegistry dockerRegistry,
                           String dockerRepositoryName)
    {
        super(boxConfiguration, projectConfiguration, context, dockerRegistry, dockerRepositoryName);

        this.version11OrGreater = isMariaDbVersion11OrGreater();
    }

    private boolean isMariaDbVersion11OrGreater()
    {
        if (getBoxConfiguration().getDatabaseVersion().equalsIgnoreCase("latest"))
            return true;

        GenericVersionScheme versionScheme = new GenericVersionScheme();
        try
        {
            Version parsedVersion = versionScheme.parseVersion(getBoxConfiguration().getDatabaseVersion());
            Version version11 = versionScheme.parseVersion("11");
            if (parsedVersion.compareTo(version11) < 0) //If before version 11
                return false;
        }
        catch (InvalidVersionSpecificationException e)
        {
            //Not parsable, fallthrough
        }

        //Assume version 11 or greater if version is unparseable
        return true;
    }

    @Override
    protected String mySqlExecutableName()
    {
        //Newer versions of MariaDB docker image don't have 'mysql' binary
        //See https://mariadb.com/kb/en/mariadb-11-0-1-release-notes/#docker-official-images
        if (version11OrGreater)
            return "mariadb";

        return super.mySqlExecutableName();
    }

    @Override
    protected String mySqlDumpExecutableName()
    {
        //Newer versions of MariaDB docker image don't have 'mysqldump' binary
        //See https://mariadb.com/kb/en/mariadb-11-0-1-release-notes/#docker-official-images
        if (version11OrGreater)
            return "mariadb-dump";

        return super.mySqlDumpExecutableName();
    }

    @Override
    public JdbcConnectionInfo jdbcConnectionInfo(DatabaseTarget target) throws BoxDatabaseException
    {
        String databaseName;
        if (target == DatabaseTarget.ADMIN)
            databaseName = "mysql";
        else
            databaseName = getBoxConfiguration().getDatabaseName();

        String uri =  "jdbc:mariadb://" +
                        getContext().getDockerHostAddress() +
                        ":" + getBoxConfiguration().getDatabasePort() +
                        "/" + databaseName;

        return new JdbcConnectionInfo(uri,
                        target.user(getBoxConfiguration()),
                        target.password(getBoxConfiguration()),
                        getContext().getDockerHostAddress(),
                        getBoxConfiguration().getDatabasePort());
    }

    @Override
    public JdbcDriverInfo jdbcDriverInfo()
    throws BoxDatabaseException
    {
        //Drivers are backward/forward compatible so just use latest
        return new JdbcDriverInfo(new RunnerDependency("org.mariadb.jdbc", "mariadb-java-client", "3.4.0"), "org.mariadb.jdbc.Driver");
    }

    @Override
    public void configureNewDatabase()
    throws IOException, SQLException, BoxDatabaseException
    {
        URL initScript = MariaDbDatabase.class.getResource("mariadb-create-database.sql");

        ScriptReaderRunner scriptRunner = getContext().createScriptReaderRunner(this, getBoxConfiguration(), getProjectConfiguration());
        ScriptReaderExecution execution = new ScriptReaderExecution();
        execution.setFiltering(true);
        execution.setScripts(Arrays.asList(initScript));

        try
        {
            scriptRunner.execute(execution, DatabaseTarget.ADMIN, getProjectConfiguration().getScriptTimeout());
        }
        catch (MojoExecutionException e)
        {
            throw new BoxDatabaseException(e);
        }
    }

    protected DataSourceBuilder dataSourceBuilder(DatabaseTarget target)
    throws BoxDatabaseException
    {
        JdbcConnectionInfo jdbcInfo = jdbcConnectionInfo(target);
        return new DataSourceBuilder(getContext())
                    .dataSourceClassName("org.mariadb.jdbc.MariaDbDataSource")
                    .dependencies(jdbcDriverInfo().getDependencies())
                    .configureDataSource("setUrl", String.class, jdbcInfo.getUri())
                    .configureDataSource("setUser", String.class, jdbcInfo.getUser())
                    .configureDataSource("setPassword", String.class, jdbcInfo.getPassword());
    }

    @Override
    public Connection createJdbcConnection(DatabaseTarget targetDatabase) 
    throws SQLException, BoxDatabaseException, IOException
    {
        return dataSourceBuilder(targetDatabase).create().getConnection();
    }
}
