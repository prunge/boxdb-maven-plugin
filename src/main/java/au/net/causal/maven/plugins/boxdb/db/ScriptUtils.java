package au.net.causal.maven.plugins.boxdb.db;

/**
 * Tools for creating command lines for unix shell scripts.
 */
public final class ScriptUtils
{
    /**
     * Private constructor to prevent instantiation.
     */
    private ScriptUtils()
    {
    }

    /**
     * Escapes a string suitable for use with a unix shell.  Whole string is put in quotes, and existing quotes
     * are escaped.
     *
     * @param s the string to escape.
     *
     * @return an escaped string.
     */
    public static String shellEscape(String s)
    {
        StringBuilder buf = new StringBuilder();
        buf.append('\'');
        for (char c : s.toCharArray())
        {
            if (c == '\'')
                buf.append("'\\''");
            else
                buf.append(c);
        }
        buf.append('\'');

        return buf.toString();
    }
}
