package au.net.causal.maven.plugins.boxdb.db;

import au.net.causal.maven.plugins.boxdb.ImageCheckerUtils;
import com.google.common.collect.ImmutableList;
import com.google.common.collect.ImmutableMap;
import io.fabric8.maven.docker.access.DockerAccessException;
import io.fabric8.maven.docker.config.RunImageConfiguration;

import java.io.IOException;
import java.nio.file.Files;
import java.nio.file.Path;
import java.nio.file.StandardCopyOption;
import java.sql.SQLException;
import java.util.Collection;

public class OracleModernDatabase extends OracleDatabase
{
    public OracleModernDatabase(BoxConfiguration boxConfiguration, ProjectConfiguration projectConfiguration,
                                BoxContext context, DockerRegistry dockerRegistry, String dockerImageName)
    throws IOException
    {
        super(boxConfiguration, projectConfiguration, context, dockerRegistry, dockerImageName);
    }

    @Override
    protected String oracleHomePath()
    {
        return "$ORACLE_HOME/bin/";
    }

    @Override
    protected void configureRunImage(RunImageConfiguration.Builder builder)
    {
        super.configureRunImage(builder);
        builder.ports(ImmutableList.of(
                getBoxConfiguration().getDatabasePort() + ":" + containerDatabasePort()));
        appendEnvToRunImage(builder, ImmutableMap.of(
                "ORACLE_PASSWORD", "oracle"
        ));
    }

    @Override
    protected String createExecContainer(RunImageConfiguration sqlPlusRunConfig)
    throws BoxDatabaseException, DockerAccessException
    {
        //Force to run these as root user
        sqlPlusRunConfig = new RunImageConfiguration.Builder(sqlPlusRunConfig)
                                .user("0")
                                .build();

        return super.createExecContainer(sqlPlusRunConfig);
    }

    @Override
    public Collection<? extends ImageComponent> checkImage()
    throws BoxDatabaseException
    {
        ImageComponent jdbcDriverComponent = ImageCheckerUtils.checkImageUsingMavenDependencies("JDBC driver",
                                                                                                getContext(),
                                                                                                jdbcDriverInfo().getDependencies());
        ImageComponent dockerDatabaseComponent = checkDockerDatabaseImage(OracleModernFactory.ORACLE_MODERN_DOCKER_REPOSITORY,
                                                                          getBoxConfiguration().getDatabaseVersion());

        return ImmutableList.of(jdbcDriverComponent, dockerDatabaseComponent);
    }

    @Override
    public void backup(Path backupFile, BackupFileTypeHint backupFileTypeHint)
    throws BoxDatabaseException, IOException, SQLException
    {
        //This is the backup file created by Oracle, move it at the end
        //If the file name specified has no extension then it gets ".dmp" by Oracle tool
        //Thanks Oracle
        String oracleFileName = backupFile.getFileName().toString();
        if (!oracleFileName.contains("."))
            oracleFileName = oracleFileName + ".dmp";

        Path targetFile = getHostScriptDirectory().resolve(oracleFileName);

        //Run shell script to initialize /data/dump directory
        //We can't use /data/scripts because permissions might be wrong on some docker instances
        //and oracle user doesn't have write access to it
        runShellScript(OracleModernDatabase.class.getResource("oraclem-pre-backup.sh"));

        //Set up directory and give database user permission to use it
        runFilteredScript("oraclem-prepare-backup.sql");

        //Run the dump tool
        String schema = getBoxConfiguration().getDatabaseUser();
        String args = "schemas=" + schema +
                " directory=backup_dir dumpfile=" + backupFile.getFileName().toString() +
                " logfile=" + backupFile.getFileName().toString() + ".log";
        executeExpDp(args, DatabaseTarget.USER, getProjectConfiguration().getBackupTimeout(), false);

        //Now run shell script to move file to proper location
        runShellScript(OracleModernDatabase.class.getResource("oraclem-post-backup.sh"));

        //Move backup file to target
        Files.move(targetFile, backupFile, StandardCopyOption.REPLACE_EXISTING);
    }

    @Override
    public void restore(Path backupFile)
    throws BoxDatabaseException, IOException, SQLException
    {
        //Copy backup file to host script directory so Oracle can access it
        Path mountedBackupFile = Files.createTempFile(getHostScriptDirectory(), "backup", ".dmp");
        Files.copy(backupFile, mountedBackupFile, StandardCopyOption.REPLACE_EXISTING);

        //Shell script to create dump directory if needed and set it up
        runShellScript(OracleModernDatabase.class.getResource("oraclem-pre-backup.sh"));

        //and copy the backup dmp file to the dump directory
        runShellScript(OracleModernDatabase.class.getResource("oraclem-pre-restore.sh"));

        //Set up directory and give database user permission to use it
        runFilteredScript("oraclem-prepare-backup.sql");

        //Run tool
        String schema = getBoxConfiguration().getDatabaseUser();
        String args = "schemas=" + schema +
                " directory=backup_dir dumpfile=" + mountedBackupFile.getFileName().toString() +
                " logfile=" + mountedBackupFile.getFileName().toString() + ".log";
        executeImpDp(args, DatabaseTarget.USER, getProjectConfiguration().getBackupTimeout());

        //Clean up dmp file
        runShellScript(OracleModernDatabase.class.getResource("oraclem-post-restore.sh"));
    }
}
