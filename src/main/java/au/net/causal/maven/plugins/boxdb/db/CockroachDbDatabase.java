package au.net.causal.maven.plugins.boxdb.db;

import au.net.causal.maven.plugins.boxdb.ImageCheckerUtils;
import au.net.causal.maven.plugins.boxdb.JdbcSqlRunner;
import au.net.causal.maven.plugins.boxdb.ScriptReaderRunner;
import com.google.common.collect.ImmutableList;
import io.fabric8.maven.docker.access.DockerAccess;
import io.fabric8.maven.docker.access.DockerAccessException;
import io.fabric8.maven.docker.access.PortMapping;
import io.fabric8.maven.docker.config.Arguments;
import io.fabric8.maven.docker.config.ImageConfiguration;
import io.fabric8.maven.docker.config.LogConfiguration;
import io.fabric8.maven.docker.config.RunImageConfiguration;
import io.fabric8.maven.docker.config.RunVolumeConfiguration;
import io.fabric8.maven.docker.log.LogDispatcher;
import io.fabric8.maven.docker.log.LogOutputSpec;
import io.fabric8.maven.docker.model.Container;
import io.fabric8.maven.docker.service.RunService;
import io.fabric8.maven.docker.util.GavLabel;
import org.apache.maven.artifact.versioning.ArtifactVersion;
import org.apache.maven.artifact.versioning.DefaultArtifactVersion;
import org.apache.maven.plugin.MojoExecutionException;

import java.io.BufferedReader;
import java.io.IOException;
import java.io.Reader;
import java.net.URL;
import java.nio.file.Files;
import java.nio.file.NoSuchFileException;
import java.nio.file.Path;
import java.nio.file.StandardCopyOption;
import java.sql.Connection;
import java.sql.SQLException;
import java.time.Duration;
import java.util.Arrays;
import java.util.Collection;
import java.util.Collections;
import java.util.Date;
import java.util.List;
import java.util.Properties;
import java.util.regex.Pattern;

public class CockroachDbDatabase extends DockerDatabase
{
    private final Path hostBackupDirectory;

    public CockroachDbDatabase(BoxConfiguration boxConfiguration,
                               ProjectConfiguration projectConfiguration,
                               BoxContext context, DockerRegistry dockerRegistry)
    throws IOException
    {
        super(boxConfiguration, projectConfiguration, context, dockerRegistry);

        hostBackupDirectory = context.getTempDirectory().resolve(containerName() + "-backups");
        if (Files.notExists(hostBackupDirectory))
            Files.createDirectories(hostBackupDirectory);
    }

    @Override
    protected String dockerImageName()
    {
        return CockroachDbFactory.COCKROACH_DB_DOCKER_REPOSITORY + ":" + "v" + getBoxConfiguration().getDatabaseVersion();
    }

    @Override
    protected int containerDatabasePort()
    {
        return 26257;
    }

    private Path getBackupDirectory()
    {
        return hostBackupDirectory;
    }

    @Override
    public void configureNewDatabase()
    throws IOException, SQLException, BoxDatabaseException
    {
        URL initScript = CockroachDbDatabase.class.getResource("cockroachdb-create-database.sql");

        ScriptReaderRunner scriptRunner = getContext().createScriptReaderRunner(this, getBoxConfiguration(), getProjectConfiguration());
        ScriptReaderExecution execution = new ScriptReaderExecution();
        execution.setFiltering(true);
        execution.setScripts(Arrays.asList(initScript));

        try
        {
            scriptRunner.execute(execution, DatabaseTarget.ADMIN, getProjectConfiguration().getScriptTimeout());
        }
        catch (MojoExecutionException e)
        {
            throw new BoxDatabaseException(e);
        }
    }

    @Override
    protected void configureRunImage(RunImageConfiguration.Builder builder)
    {
        super.configureRunImage(builder);

        //Need different arguments depending on the version
        //>= 19.2 needs start-single-node instead of start
        if (databaseVersionIsHigherOrEqualTo("19.2"))
            builder.cmd(new Arguments(ImmutableList.of("start-single-node", "--insecure")));
        else
            builder.cmd(new Arguments(ImmutableList.of("start", "--insecure")));

        RunVolumeConfiguration backupVolume = new RunVolumeConfiguration.Builder()
            .bind(Arrays.asList(
                    getProjectConfiguration().getDockerBindPathTranslationMode().translateBind(getBackupDirectory(), "/data/backup")))
            .build();

        builder.volumes(backupVolume);
    }

    private ArtifactVersion databaseVersion()
    {
        return new DefaultArtifactVersion(getBoxConfiguration().getDatabaseVersion());
    }

    private boolean databaseVersionIsHigherOrEqualTo(String requiredVersion)
    {
        ArtifactVersion required = new DefaultArtifactVersion(requiredVersion);
        return databaseVersion().compareTo(required) >= 0;
    }

    @Override
    public Collection<? extends ImageComponent> checkImage()
    throws BoxDatabaseException
    {
        ImageComponent jdbcDriverComponent = ImageCheckerUtils.checkImageUsingMavenDependencies("JDBC driver",
                                                                                                getContext(),
                                                                                                jdbcDriverInfo().getDependencies());
        ImageComponent dockerDatabaseComponent = checkDockerDatabaseImage(CockroachDbFactory.COCKROACH_DB_DOCKER_REPOSITORY);

        return ImmutableList.of(jdbcDriverComponent, dockerDatabaseComponent);
    }

    @Override
    protected ImageComponent checkDockerDatabaseImage(String remoteDockerRepositoryName)
    throws BoxDatabaseException
    {
        return checkDockerDatabaseImage(remoteDockerRepositoryName, "v" + getBoxConfiguration().getDatabaseVersion());
    }

    /**
     * Returns the URL that can be used as a parameter for CockroachDB's console tools to connect to the database.
     *
     * @param target whether to connect as the admin user or the database user.
     *
     * @return a string URL suitable for use with the {@code -url} command line option for CockroachDB command line tools.
     *
     * @throws BoxDatabaseException if an error occurs.
     */
    protected String cockroachDbUrl(DatabaseTarget target)
    throws BoxDatabaseException
    {
        String databaseName;
        if (target == DatabaseTarget.ADMIN)
            databaseName = "system";
        else
            databaseName = getBoxConfiguration().getDatabaseName();

        //This is the host for a console command run with a container link with this hostname
        String host = "cockroachdb";

        //sslmode=disable is required for versions < 2.0
        // //for >= 2.0 it's not required but still works if it is included
        return "postgresql://" + target.user(getBoxConfiguration()) + "@" +
                host + ":" + getBoxConfiguration().getDatabasePort() +
                "/" + databaseName + "?sslmode=disable";
    }

    @Override
    public JdbcConnectionInfo jdbcConnectionInfo(DatabaseTarget target)
    throws BoxDatabaseException
    {
        String databaseName;
        if (target == DatabaseTarget.ADMIN)
            databaseName = "system";
        else
            databaseName = getBoxConfiguration().getDatabaseName();

        String uri =  "jdbc:postgresql://" +
                getContext().getDockerHostAddress() +
                ":" + getBoxConfiguration().getDatabasePort() +
                "/" + databaseName;

        return new JdbcConnectionInfo(uri,
                                      target.user(getBoxConfiguration()),
                                      target.password(getBoxConfiguration()),
                                      getContext().getDockerHostAddress(),
                                      getBoxConfiguration().getDatabasePort());
    }

    @Override
    public JdbcDriverInfo jdbcDriverInfo()
    throws BoxDatabaseException
    {
        //CockroachDB uses Postgres drivers
        return JdbcDrivers.postgresDriver();
    }

    protected DataSourceBuilder dataSourceBuilder(DatabaseTarget target)
    throws BoxDatabaseException
    {
        //CockroachDB uses Postgres drivers
        return JdbcDrivers.postgresDataSourceBuilder(jdbcConnectionInfo(target), jdbcDriverInfo(), getContext());
    }

    @Override
    public Connection createJdbcConnection(DatabaseTarget targetDatabase)
    throws SQLException, BoxDatabaseException, IOException
    {
        return dataSourceBuilder(targetDatabase).create().getConnection();
    }

    @Override
    public void executeJdbcScript(Reader scriptReader, DatabaseTarget targetDatabase)
    throws IOException, SQLException, BoxDatabaseException
    {
        try (Connection con = createJdbcConnection(targetDatabase);
             JdbcSqlRunner sqlRunner = new JdbcSqlRunner(con, getContext().getLog()))
        {
            sqlRunner.executeSql(new BufferedReader(scriptReader));
        }
    }

    @Override
    protected void executeScriptFile(Path scriptFile, DatabaseTarget targetDatabase, Duration timeout)
    throws IOException, SQLException, BoxDatabaseException
    {
        if (!Files.exists(scriptFile))
            throw new NoSuchFileException(scriptFile.toString());

        RunVolumeConfiguration volumes = new RunVolumeConfiguration.Builder()
                .bind(Arrays.asList(getProjectConfiguration().getDockerBindPathTranslationMode().translateBind(scriptFile.getParent(), "/data/scripts", DockerBindPathTranslationMode.BindOption.RO)))
                .build();

        executeCockroachSql(volumes, "< /data/scripts/" + scriptFile.getFileName().toString(), targetDatabase, timeout);
    }

    @Override
    public void executeSql(String sql, DatabaseTarget targetDatabase, Duration timeout)
    throws IOException, SQLException, BoxDatabaseException
    {
        executeCockroachSql(new RunVolumeConfiguration.Builder().build(), "--execute " + ScriptUtils.shellEscape(sql), targetDatabase, timeout);
    }

    @Override
    public void backup(Path backupFile, BackupFileTypeHint backupFileTypeHint)
    throws BoxDatabaseException, IOException, SQLException
    {
        if (isBackupSqlSupportedForDatabaseVersion())
            backupUsingBackupSql(backupFile);
        else
            backupUsingDumpCommand(backupFile);
    }

    /**
     * The dump command is only valid for older versions of CockroachDB, and it was deprecated and then removed in
     * newer versions.  For newer versions, the backup command can be used from SQL scripts which is not supported
     * in older versions.
     */
    private boolean isBackupSqlSupportedForDatabaseVersion()
    {
        return databaseVersionIsHigherOrEqualTo("21.0");
    }

    /**
     * Creates a backup using the CockroachDB dump command.
     * <p>
     *
     * The dump command is only valid for older versions of CockroachDB, and it was deprecated and then removed in
     * newer versions.
     *
     * @param backupFile the backup file to create.
     *
     * @throws BoxDatabaseException if an error occurs running the dump command.
     * @throws IOException if an I/O error occurs processing the backup file.
     * @throws SQLException if a database error occurs.
     *
     * @see <a href="https://www.cockroachlabs.com/docs/v21.1/cockroach-dump.html">Cockroach dump command</a>
     */
    private void backupUsingDumpCommand(Path backupFile)
    throws BoxDatabaseException, IOException, SQLException
    {
        Path backupDirectory = backupFile.getParent();

        if (!Files.exists(backupDirectory))
            Files.createDirectories(backupDirectory);

        //Mount backup file as volume to /data/backup
        RunVolumeConfiguration volumes = new RunVolumeConfiguration.Builder()
                .bind(Arrays.asList(getProjectConfiguration().getDockerBindPathTranslationMode().translateBind(backupDirectory, "/data/backup")))
                .build();

        //Run the dump
        String preArgs = getBoxConfiguration().getDatabaseName();
        String postArgs = " > /data/backup/" + backupFile.getFileName().toString();
        executeCockroachCommand(volumes, DatabaseTarget.USER, "dump", preArgs, postArgs, getProjectConfiguration().getBackupTimeout());
    }

    @Override
    public void restore(Path backupFile)
    throws BoxDatabaseException, IOException, SQLException
    {
        if (isBackupSqlSupportedForDatabaseVersion())
            restoreFromBackupSql(backupFile);
        else
            restoreFromDumpCommand(backupFile);
    }

    /**
     * Restore a backup previously created using the {@linkplain #backupUsingDumpCommand(Path) dump command}.
     *
     * @param backupFile the backup file to restore.
     *
     * @throws BoxDatabaseException if an error occurs running the restore.
     * @throws IOException if an I/O error occurs reading the dump file.
     * @throws SQLException if a database error occurs.
     */
    private void restoreFromDumpCommand(Path backupFile)
    throws BoxDatabaseException, IOException, SQLException
    {
        executeScriptFile(backupFile, DatabaseTarget.USER, getProjectConfiguration().getBackupTimeout());
    }

    private void backupUsingBackupSql(Path backupFile)
    throws IOException, SQLException, BoxDatabaseException
    {
        //Backup to /cockroach/cockroach-data/extern/[backupfilename] in container
        String backupName = backupFile.getFileName().toString();
        Path targetFile = getBackupDirectory().resolve(backupName);
        executeSql("backup database " + getBoxConfiguration().getDatabaseName() + " into 'nodelocal://self/" + backupName + "'",
                DatabaseTarget.ADMIN, getProjectConfiguration().getBackupTimeout());

        //Prepare host directory
        Path backupDirectory = backupFile.getParent();
        if (!Files.exists(backupDirectory))
            Files.createDirectories(backupDirectory);

        //Create TAR file from the backup so we have a single backup file on the host
        doFileTar("/cockroach/cockroach-data/extern/" + backupName, "/data/backup/" + backupName);

        //Move file to destination from the container mapped directory
        Files.move(targetFile, backupFile, StandardCopyOption.REPLACE_EXISTING);
    }

    private void restoreFromBackupSql(Path backupFile)
    throws IOException, SQLException, BoxDatabaseException
    {
        //Copy restore file to target in the backup directory which is mapped into the container
        String backupName = backupFile.getFileName().toString();
        Path targetFile = getBackupDirectory().resolve(backupName);
        Files.copy(backupFile, targetFile, StandardCopyOption.REPLACE_EXISTING);

        //In the container, untar to Cockroach's extern directory
        doFileUntar("/data/backup/" + backupName, "/cockroach/cockroach-data/extern/" + backupName);

        executeSql("drop database if exists " + getBoxConfiguration().getDatabaseName(),
                   DatabaseTarget.ADMIN, getProjectConfiguration().getBackupTimeout());
        executeSql("restore database " + getBoxConfiguration().getDatabaseName() + " from latest in 'nodelocal://self/" + backupName + "'",
                   DatabaseTarget.ADMIN, getProjectConfiguration().getBackupTimeout());
    }

    private static String parentDirectoryOfPath(String path)
    {
        List<String> segments = Arrays.asList(path.split(Pattern.quote("/")));
        return String.join("/", segments.subList(0, segments.size() - 1));
    }

    private void doFileTar(String fromContainerDirectoryPath, String toContainerTarFilePath)
    throws IOException, BoxDatabaseException
    {
        String toContainerDirectoryPath = parentDirectoryOfPath(toContainerTarFilePath);

        DockerAccess docker = getContext().getDockerServiceHub().getDockerAccess();

        LogConfiguration logConfig = new LogConfiguration.Builder()
                .enabled(true)
                .prefix("tar")
                .build();
        RunImageConfiguration tarRunConfig = new RunImageConfiguration.Builder()
                .links(Collections.singletonList(containerName() + ":cockroachdb"))
                .containerNamePattern("%a")
                .cmd("unused") //not used but must be non-null - is replaced later
                .log(logConfig)
                .build();

        //e.g. tar cf /home/backupname.tar -C /cockroach/cockroach-data/extern/backupname .
        tarRunConfig.getCmd().setExec(Arrays.asList("bash", "-i", "-c", "mkdir -p " + toContainerDirectoryPath +
                " ; tar cf " + toContainerTarFilePath + " -C " + fromContainerDirectoryPath + " ."));
        getContext().getLog().debug("Executing command: " + tarRunConfig.getCmd().getExec());
        tarRunConfig.getCmd().setShell(null);
        ImageConfiguration cpConfig = new ImageConfiguration.Builder()
                .runConfig(tarRunConfig)
                .name(dockerImageName())
                .alias(containerName() + "-tar")
                .build();

        String containerId = findDockerContainer().getId();
        String execContainerId = docker.createExecContainer(containerId, tarRunConfig.getCmd());

        LogOutputSpec execLog = getContext().getLogSpecFactory().createSpec(execContainerId, cpConfig);

        docker.startExecContainer(execContainerId, execLog);
        getContext().getLog().debug("Exec container done " + execContainerId);
    }

    private void doFileUntar(String fromContainerTarFilePath, String toContainerDirectoryPath)
    throws IOException, BoxDatabaseException
    {
        DockerAccess docker = getContext().getDockerServiceHub().getDockerAccess();

        LogConfiguration logConfig = new LogConfiguration.Builder()
                .enabled(true)
                .prefix("untar")
                .build();
        RunImageConfiguration tarRunConfig = new RunImageConfiguration.Builder()
                .links(Collections.singletonList(containerName() + ":cockroachdb"))
                .containerNamePattern("%a")
                .cmd("unused") //not used but must be non-null - is replaced later
                .log(logConfig)
                .build();

        //e.g. tar cf /home/backupname.tar -C /cockroach/cockroach-data/extern/backupname .
        tarRunConfig.getCmd().setExec(Arrays.asList("bash", "-i", "-c", "mkdir -p " + toContainerDirectoryPath +
                " ; tar xf " + fromContainerTarFilePath + " -C " + toContainerDirectoryPath + " ."));
        getContext().getLog().debug("Executing command: " + tarRunConfig.getCmd().getExec());
        tarRunConfig.getCmd().setShell(null);
        ImageConfiguration cpConfig = new ImageConfiguration.Builder()
                .runConfig(tarRunConfig)
                .name(dockerImageName())
                .alias(containerName() + "-tar")
                .build();

        String containerId = findDockerContainer().getId();
        String execContainerId = docker.createExecContainer(containerId, tarRunConfig.getCmd());

        LogOutputSpec execLog = getContext().getLogSpecFactory().createSpec(execContainerId, cpConfig);

        docker.startExecContainer(execContainerId, execLog);
        getContext().getLog().debug("Exec container done " + execContainerId);
    }

    private void executeCockroachSql(RunVolumeConfiguration volumes, String cockroachSqlArgs, DatabaseTarget targetDatabase,
                                     Duration timeout)
    throws IOException, SQLException, BoxDatabaseException
    {
        executeCockroachCommand(volumes, targetDatabase, "sql", "", cockroachSqlArgs, timeout);
    }

    private void executeCockroachCommand(RunVolumeConfiguration volumes,
                                         DatabaseTarget target,
                                         String command,
                                         String preArgs, String postArgs,
                                         Duration timeout)
    throws IOException, SQLException, BoxDatabaseException
    {
        RunService runService = getContext().getDockerServiceHub().getRunService();
        DockerAccess docker = getContext().getDockerServiceHub().getDockerAccess();

        Properties projectProperties = getProjectConfiguration().getProjectProperties();
        GavLabel pomLabel = getProjectConfiguration().getPomLabel();
        PortMapping mappedPorts = new PortMapping(Collections.emptyList(), projectProperties);

        int cockroachSqlMaxExecutionTimeSeconds = Math.toIntExact(timeout.getSeconds());
        LogConfiguration logConfig = new LogConfiguration.Builder()
                .enabled(true)
                .prefix("cockroach")
                .build();
        RunImageConfiguration.Builder cockroachSqlRunConfigBuilder = new RunImageConfiguration.Builder()
                .links(Collections.singletonList(containerName() + ":cockroachdb"))
                .containerNamePattern("%a")
                .cmd("unused") //not used but must be non-null - is replaced later
                //.env(Collections.singletonMap("PGPASSWORD", targetDatabase.password(getBoxConfiguration())))
                .volumes(volumes)
                .entrypoint(new Arguments(ImmutableList.of("")))
                .log(logConfig);
        RunImageConfiguration cockroachSqlRunConfig = cockroachSqlRunConfigBuilder.build();
        cockroachSqlRunConfig.getCmd().setExec(
                Arrays.asList("sh", "-c", "/cockroach/cockroach.sh " + command + " " + preArgs + " --insecure --url " + cockroachDbUrl(target) + " " + postArgs));
        cockroachSqlRunConfig.getCmd().setShell(null);
        ImageConfiguration psqlConfig = new ImageConfiguration.Builder()
                .runConfig(cockroachSqlRunConfig)
                .name(dockerImageName())
                .alias(containerName() + "-cockroach")
                .build();

        LogDispatcher dispatcher = new LogDispatcher(getContext().getDockerServiceHub().getDockerAccess());

        Date buildTimestamp = new Date();
        String containerId = runService.createAndStartContainer(psqlConfig, mappedPorts, pomLabel, projectProperties, getProjectConfiguration().getBaseDirectory(), null, buildTimestamp);
        Container container = null;
        try
        {
            getContext().getLog().debug("Cockroach-sql running in container " + containerId);

            dispatcher.trackContainerLog(containerId, new LogOutputSpec.Builder()
                    .logStdout(true)
                    .prefix("cockroach-sql> ")
                    .build());
        }
        finally
        {
            try
            {
                container = waitForContainerToFinish(containerId, cockroachSqlMaxExecutionTimeSeconds);
            }
            catch (DockerAccessException e)
            {
                getContext().getLog().warn("Error shutting down Cockroach-SQL container", e);
            }

            try
            {
                boolean removeVolumes = true;
                docker.removeContainer(containerId, removeVolumes);
            }
            catch (DockerAccessException e)
            {
                getContext().getLog().warn("Error removing Cockroach-SQL container", e);
            }
        }

        int exitCode = readExitCodeFromContainer(container);
        if (exitCode != 0)
            throw new SQLException("Cockroach-SQL exit code: " + exitCode);

        getContext().getLog().debug("All done");
    }
}
