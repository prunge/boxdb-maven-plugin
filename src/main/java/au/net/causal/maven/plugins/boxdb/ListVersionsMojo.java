package au.net.causal.maven.plugins.boxdb;

import au.net.causal.maven.plugins.boxdb.db.BoxDatabaseException;
import au.net.causal.maven.plugins.boxdb.db.BoxDatabaseFactory;
import au.net.causal.maven.plugins.boxdb.db.DockerService;
import io.fabric8.maven.docker.access.DockerAccessException;
import org.apache.maven.plugin.MojoExecutionException;
import org.apache.maven.plugins.annotations.Mojo;

import java.util.List;

/**
 * Lists available versions of a database type.
 */
@Mojo(name="versions", requiresProject = false)
public class ListVersionsMojo extends StartAndWaitMojo
{
    @Override
    protected void executeInternal(ExceptionalSupplier<DockerService, BoxDatabaseException> dockerService)
    throws DockerAccessException, MojoExecutionException
    {
        try
        {
            BoxDatabaseFactory dbFactory = databaseFactory(dockerService);
            List<String> versions = dbFactory.availableVersions(projectConfiguration(), boxContext(dockerService));
            getLog().info(dbFactory.name() + " versions: " + versions);
        }
        catch (BoxDatabaseException e)
        {
            throw new MojoExecutionException("Error setting up database: " + e, e);
        }
    }
}
