package au.net.causal.maven.plugins.boxdb.db;

import java.net.URL;
import java.util.ArrayList;
import java.util.List;

/**
 * Defines executions of script resources.
 */
public class ScriptReaderExecution extends ScriptExecutionBase
{
    private List<URL> scripts = new ArrayList<>();

    /**
     * @return list of URL resources that will be executed.
     */
    public List<URL> getScripts()
    {
        return scripts;
    }

    public void setScripts(List<URL> scripts)
    {
        this.scripts = scripts;
    }
}
