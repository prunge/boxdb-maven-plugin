package au.net.causal.maven.plugins.boxdb.db;

import java.util.ArrayList;
import java.util.Collections;
import java.util.List;

public class JdbcDriverInfo
{
    private final List<? extends RunnerDependency> dependencies;
    private final String driverClassName;
    private final String downloadUrl;

    public JdbcDriverInfo(RunnerDependency dependency, String driverClassName)
    {
        this(Collections.singletonList(dependency), driverClassName);
    }

    public JdbcDriverInfo(RunnerDependency dependency, String driverClassName, String downloadUrl)
    {
        this(Collections.singletonList(dependency), driverClassName, downloadUrl);
    }

    public JdbcDriverInfo(List<? extends RunnerDependency> dependencies, String driverClassName)
    {
        this(dependencies, driverClassName, null);
    }

    public JdbcDriverInfo(List<? extends RunnerDependency> dependencies, String driverClassName, String downloadUrl)
    {
        this.dependencies = Collections.unmodifiableList(new ArrayList<>(dependencies));
        this.driverClassName = driverClassName;
        this.downloadUrl = downloadUrl;
    }

    public List<? extends RunnerDependency> getDependencies()
    {
        return dependencies;
    }

    public String getDriverClassName()
    {
        return driverClassName;
    }

    public String getDownloadUrl()
    {
        return downloadUrl;
    }

    @Override
    public String toString()
    {
        final StringBuilder sb = new StringBuilder("JdbcDriverInfo{");
        sb.append("dependencies=").append(dependencies);
        sb.append(", driverClassName='").append(driverClassName).append('\'');
        sb.append('}');
        return sb.toString();
    }
}
