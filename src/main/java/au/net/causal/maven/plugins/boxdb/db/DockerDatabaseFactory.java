package au.net.causal.maven.plugins.boxdb.db;

import java.net.URI;
import java.util.List;
import java.util.Objects;

public abstract class DockerDatabaseFactory implements BoxDatabaseFactory
{
    private final String name;

    protected DockerDatabaseFactory(String name)
    {
        Objects.requireNonNull(name, "name == null");
        this.name = name;
    }

    @Override
    public String name()
    {
        return name;
    }

    @Override
    public BoxDatabase create(BoxConfiguration boxConfiguration, ProjectConfiguration projectConfiguration, BoxContext context)
    throws BoxDatabaseException
    {
        Objects.requireNonNull(boxConfiguration, "boxConfiguration == null");
        Objects.requireNonNull(context, "context == null");

        initializeDefaults(boxConfiguration);
        return createDockerDatabase(boxConfiguration, projectConfiguration, context);
    }

    protected abstract DockerDatabase createDockerDatabase(BoxConfiguration boxConfiguration,
                                                           ProjectConfiguration projectConfiguration,
                                                           BoxContext context)
    throws BoxDatabaseException;

    protected void initializeDefaults(BoxConfiguration boxConfiguration)
    {
        if (boxConfiguration.getDatabaseUser() == null)
            boxConfiguration.setDatabaseUser(boxConfiguration.getAdminUser());
        if (boxConfiguration.getDatabasePassword() == null)
            boxConfiguration.setDatabasePassword(boxConfiguration.getAdminPassword());
        if (boxConfiguration.getContainerName() == null)
        {
            if (boxConfiguration.getDatabaseVersion() == null)
                boxConfiguration.setContainerName("boxdb-" + boxConfiguration.getDatabaseType());
            else
                boxConfiguration.setContainerName("boxdb-" + boxConfiguration.getDatabaseType() + "-" + boxConfiguration.getDatabaseVersion());
        }
    }

    /**
     * Returns the docker registry for this database factory.  Defaults to <code>index.docker.io</code> unless overridden.
     *
     * @return the docker registry for this database factory.
     */
    protected DockerRegistry dockerRegistry()
    {
        return new DockerRegistry(URI.create("https://index.docker.io/v2/"));
    }
}
