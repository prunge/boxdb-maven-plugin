package au.net.causal.maven.plugins.boxdb.db;

public class PostgisFactory extends PostgresFactory
{
    static final String POSTGIS_DOCKER_REPOSITORY = "kartoza/postgis";

    public PostgisFactory()
    {
        super("postgis");
    }

    @Override
    protected String getPostgresDockerRepository()
    {
        return POSTGIS_DOCKER_REPOSITORY;
    }

    @Override
    protected PostgisDatabase createDockerDatabase(BoxConfiguration boxConfiguration, ProjectConfiguration projectConfiguration, BoxContext context)
    throws BoxDatabaseException
    {
        return new PostgisDatabase(boxConfiguration, projectConfiguration, context, dockerRegistry());
    }

    @Override
    protected void initializeDefaults(BoxConfiguration boxConfiguration)
    {
        if (boxConfiguration.getDatabaseVersion() == null)
            boxConfiguration.setDatabaseVersion("11.5-2.8"); //Latest version that is still compatible with old Docker versions

        super.initializeDefaults(boxConfiguration);
    }
}
