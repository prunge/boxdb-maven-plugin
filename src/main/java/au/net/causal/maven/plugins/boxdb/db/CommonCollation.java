package au.net.causal.maven.plugins.boxdb.db;

import java.util.Locale;

/**
 * Common collation names that BoxDB attempts to support across many databases.
 * 
 * @since 2.0
 */
public enum CommonCollation 
{
    /**
     * Case insensitive collation that compares character values only.
     */
    BINARY,

    /**
     * Case insensitive collation, typically using English locale.
     */
    CASE_INSENSITIVE;

    /**
     * Maps an input collation name to a common collation where possible.  Comparison is case insensitive, and '-' may
     * be used instead of '_', otherwise match is by enum name.
     * 
     * @param name the input collation name.
     *             
     * @return a matching common collation, or <code>null</code> if no match was found for input collation name.
     */
    public static CommonCollation matching(String name)
    {
        if (name == null)
            return null;
        
        String compareName = name.toUpperCase(Locale.ENGLISH).replace('-', '_');
        for (CommonCollation value : values())
        {
            if (value.name().equals(compareName))
                return value;
        }
        
        return null;
    }
}
