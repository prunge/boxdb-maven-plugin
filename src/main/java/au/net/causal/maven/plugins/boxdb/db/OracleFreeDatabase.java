package au.net.causal.maven.plugins.boxdb.db;

import au.net.causal.maven.plugins.boxdb.ImageCheckerUtils;
import com.google.common.collect.ImmutableList;

import java.io.IOException;
import java.util.Collection;

public class OracleFreeDatabase extends OracleModernDatabase
{
    public OracleFreeDatabase(BoxConfiguration boxConfiguration, ProjectConfiguration projectConfiguration,
                              BoxContext context, DockerRegistry dockerRegistry, String dockerImageName)
    throws IOException
    {
        super(boxConfiguration, projectConfiguration, context, dockerRegistry, dockerImageName);
    }

    @Override
    public Collection<? extends ImageComponent> checkImage()
    throws BoxDatabaseException
    {
        ImageComponent jdbcDriverComponent = ImageCheckerUtils.checkImageUsingMavenDependencies("JDBC driver",
                                                                                                getContext(),
                                                                                                jdbcDriverInfo().getDependencies());
        ImageComponent dockerDatabaseComponent = checkDockerDatabaseImage(OracleFreeFactory.ORACLE_FREE_DOCKER_REPOSITORY,
                                                                          getBoxConfiguration().getDatabaseVersion());

        return ImmutableList.of(jdbcDriverComponent, dockerDatabaseComponent);
    }

    @Override
    public JdbcConnectionInfo jdbcConnectionInfo(DatabaseTarget target) throws BoxDatabaseException
    {
        String uri =  "jdbc:oracle:thin:@" +
                getContext().getDockerHostAddress() +
                ":" + getBoxConfiguration().getDatabasePort() +
                ":FREE";

        return new JdbcConnectionInfo(uri,
                target.user(getBoxConfiguration()),
                target.password(getBoxConfiguration()),
                getContext().getDockerHostAddress(),
                getBoxConfiguration().getDatabasePort());
    }
}
