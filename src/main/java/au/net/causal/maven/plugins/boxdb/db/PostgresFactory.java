package au.net.causal.maven.plugins.boxdb.db;

import java.io.IOException;
import java.util.List;

@BoxDbBundled
public class PostgresFactory extends DockerDatabaseFactory
{
    static final String POSTGRES_DOCKER_REPOSITORY = "library/postgres";

    public PostgresFactory()
    {
        this("postgres");
    }

    protected PostgresFactory(String name)
    {
        super(name);
    }

    protected String getPostgresDockerRepository()
    {
        return POSTGRES_DOCKER_REPOSITORY;
    }

    @Override
    protected void initializeDefaults(BoxConfiguration boxConfiguration)
    {
        if (boxConfiguration.getAdminUser() == null)
            boxConfiguration.setAdminUser("postgres");
        if (boxConfiguration.getAdminPassword() == null)
            boxConfiguration.setAdminPassword("postgresbox");
        if (boxConfiguration.getDatabaseName() == null)
            boxConfiguration.setDatabaseName("app");
        if (boxConfiguration.getDatabaseVersion() == null)
            boxConfiguration.setDatabaseVersion("11.16"); //Latest version that still works with older Docker versions - bullseye variants break on old Docker
        if (boxConfiguration.getDatabasePort() <= 0)
            boxConfiguration.setDatabasePort(5433);
        if (boxConfiguration.getDatabaseUser() == null)
            boxConfiguration.setDatabaseUser(boxConfiguration.getDatabaseName());
        if (boxConfiguration.getDatabasePassword() == null)
            boxConfiguration.setDatabasePassword(boxConfiguration.getDatabaseUser());

        EncodingTranslator encodingTranslator = new EncodingTranslator("UTF8", this::mapCommonEncoding);
        encodingTranslator.processEncoding(boxConfiguration);
        CollationTranslator collationTranslator = new CollationTranslator("C", col -> mapCommonCollation(col, boxConfiguration.getDatabaseEncoding()));
        collationTranslator.processCollation(boxConfiguration);

        super.initializeDefaults(boxConfiguration);
    }
    
    private String mapCommonEncoding(CommonEncoding encoding)
    {
        switch (encoding)
        {
            case UNICODE:
                return "UTF8";
            case ASCII:
                return "LATIN1";
            default:
                throw new Error("Unknown common encoding: " + encoding);
        }
    }
    
    private String mapCommonCollation(CommonCollation collation, String normalizedEncoding)
    {
        switch (collation)
        {
            case BINARY:
                return "C";
            case CASE_INSENSITIVE:
                return "en_US." + normalizedEncoding;
            default:
                throw new Error("Unknown common collation: " + collation);
        }
    }

    @Override
    protected PostgresDatabase createDockerDatabase(BoxConfiguration boxConfiguration, ProjectConfiguration projectConfiguration, BoxContext context)
    throws BoxDatabaseException
    {
        return new PostgresDatabase(boxConfiguration, projectConfiguration, context, dockerRegistry());
    }

    @Override
    public List<String> availableVersions(ProjectConfiguration projectConfiguration, BoxContext context)
    throws BoxDatabaseException
    {
        try
        {
            return dockerRegistry().readTags(getPostgresDockerRepository());
        }
        catch (IOException e)
        {
            throw new BoxDatabaseException("Error reading tags: " + e.getMessage(), e);
        }
    }
}
