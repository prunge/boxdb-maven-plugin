package au.net.causal.maven.plugins.boxdb.db;

import io.fabric8.maven.docker.config.RunVolumeConfiguration;

import java.nio.file.Path;
import java.util.List;
import java.util.regex.Matcher;
import java.util.regex.Pattern;

/**
 * Controls how host paths are translated into Docker paths.  Different docker platforms, especially on Windows,
 * may need host mount paths translated differently.
 *
 * @since 3.3
 */
public enum DockerBindPathTranslationMode
{
    /**
     * Standard path translation mode that works with filesystems with forward slash separators and
     * modern Windows docker platforms (Docker desktop, Podman, Rancher desktop).  Has problems with
     * docker machine on Windows.  Leaves Windows host paths in their standard form.
     */
    STANDARD()
    {
        @Override
        public String translateBind(Path hostPath, String dockerPath, BindOption option)
        {
            String bind = hostPath.toAbsolutePath().toString() + ":" + dockerPath;
            if (option != null)
                bind = bind + ":" + option.getValue();

            return bind;
        }
    },

    /**
     * Legacy Windows path translation mode.  Compatible with Docker machine on Windows.
     * Converts Windows paths to forward slash form, e.g.
     * c:\...\ to /c/.../
     */
    LEGACY_WINDOWS()
    {
        @Override
        public String translateBind(Path hostPath, String dockerPath, BindOption option)
        {
            String bind = hostPath.toAbsolutePath().toString() + ":" + dockerPath;
            if (option != null)
                bind = bind + ":" + option.getValue();

            return fixupPath(bind);
        }

        /**
         * Fix path on Windows machines, i.e. convert 'c:\...\' to '/c/..../'.
         * Copied from docker-maven-plugin which was removed as part of
         * https://github.com/fabric8io/docker-maven-plugin/pull/1720
         * which broke compatibility with Docker machine on Windows.
         *
         * @param path path to fix
         * @return the fixed path
         */
        public String fixupPath(String path) {
            // Hack-fix for mounting on Windows where the ${projectDir} variable and other
            // contain backslashes and what not. Related to #188 (docker-maven-plugin)
            Pattern pattern = Pattern.compile("^(?i)([A-Z]):(.*)$");
            Matcher matcher = pattern.matcher(path);
            if (matcher.matches()) {
                String result = "/" + matcher.group(1).toLowerCase() + matcher.group(2);
                return result.replace("\\","/");
            }
            return path;
        }
    };

    /**
     * Translates a host and container path into a bind path string.
     *
     * @param hostPath directory or file on the host to bind.
     * @param dockerPath directory or file in the docker container to bind.
     * @param option 'ro', 'rw' or null.
     *               
     * @return a bind string.
     *
     * @see RunVolumeConfiguration.Builder#bind(List) 
     */
    public abstract String translateBind(Path hostPath, String dockerPath, BindOption option);

    /**
     * Translates a host and container path into a bind path string with no 'ro' or 'rw' option.
     *
     * @param hostPath directory or file on the host to bind.
     * @param dockerPath directory or file in the docker container to bind.
     *
     * @return a bind string.
     *
     * @see RunVolumeConfiguration.Builder#bind(List)
     */
    public String translateBind(Path hostPath, String dockerPath)
    {
        return translateBind(hostPath, dockerPath, null);
    }

    /**
     * Bind option - rw or ro.
     *
     * @since 3.3
     */
    public static enum BindOption
    {
        RW("rw"),
        RO("ro");

        private final String value;

        private BindOption(String value)
        {
            this.value = value;
        }

        public String getValue()
        {
            return value;
        }
    }
}
