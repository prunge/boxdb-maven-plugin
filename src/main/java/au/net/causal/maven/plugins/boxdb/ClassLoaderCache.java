package au.net.causal.maven.plugins.boxdb;

import java.net.URL;
import java.net.URLClassLoader;
import java.util.Arrays;
import java.util.HashMap;
import java.util.HashSet;
import java.util.Map;
import java.util.Set;

/**
 * Cache for URL classloaders to avoid spinning up large numbers of distinct classloaders from the same URLs.
 * This fixes problems to do with static rubbish, such as threads, being spawned large numbers of times from things such as 
 * JDBC drivers.
 */
public class ClassLoaderCache
{
    private final Map<Set<URL>, URLClassLoader> cache = new HashMap<>();

    /**
     * Creates or retrieves an existing loader created from the specified URLs.
     * 
     * @param urls the URLs to use for loading classes.
     *             
     * @return a URL classloader that loads from the specified URLs and no parent loader.
     */
    public synchronized URLClassLoader create(URL... urls)
    {
        Set<URL> urlSet = new HashSet<>(Arrays.asList(urls));
        ClassLoader parent = ClassLoader.getSystemClassLoader();
        return cache.computeIfAbsent(urlSet, k -> URLClassLoader.newInstance(urls, parent));
    }
}
