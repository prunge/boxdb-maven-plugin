package au.net.causal.maven.plugins.boxdb.db;

@BoxDbBundled
public class MariaDbFactory extends BaseMySqlFactory
{
    public MariaDbFactory()
    {
        super("mariadb");
    }

    @Override
    protected void initializeDefaults(BoxConfiguration boxConfiguration)
    {
        if (boxConfiguration.getDatabaseVersion() == null)
            boxConfiguration.setDatabaseVersion("10.8.2"); //The latest version that doesn't have issues with old Docker versions

        super.initializeDefaults(boxConfiguration);
    }

    @Override
    protected MariaDbDatabase createDockerDatabase(BoxConfiguration boxConfiguration, ProjectConfiguration projectConfiguration, BoxContext context)
    throws BoxDatabaseException
    {
        return new MariaDbDatabase(boxConfiguration, projectConfiguration, context, dockerRegistry(), dockerRepositoryName());
    }
}
