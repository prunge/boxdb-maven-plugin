package au.net.causal.maven.plugins.boxdb.db;

import java.util.Collections;
import java.util.List;

/**
 * Factory for boxed databases.  Implementations should have zero-argument constructors and be registered
 * in a <code>/META-INF/services/au.net.causal.maven.plugins.boxdb.db.BoxDatabaseFactory</code> file.
 */
public interface BoxDatabaseFactory
{
    /**
     * Creates a box database.
     *
     * @param boxConfiguration configuration for the database itself.
     * @param projectConfiguration generic project and global configuration.
     * @param context context for interacting with Maven and other parts of the system.
     *
     * @return the created database.
     *
     * @throws BoxDatabaseException if an error occurs.
     */
    public BoxDatabase create(BoxConfiguration boxConfiguration, ProjectConfiguration projectConfiguration, BoxContext context)
    throws BoxDatabaseException;

    /**
     * The database type name.  Users select this factory by specifying this name as database type in the
     * box configuration or through a property on the command line.
     */
    public String name();

    /**
     * Returns a list of database versions available from this provider.  The list returned is in order from oldest
     * version to newest.
     * <p>
     *
     * This might not list <i>all</i> available versions, but only the canonical versions.  For example, the database
     * version <code>5.4.28</code> might be available through an alias of <code>5.4</code> and <code>5</code>, but
     * only <code>5.4.28</code> may be listed.  However it is provider-specific behaviour whether or not these
     * non-canonical versions are omitted.  In the end, this list is meant to be an aid for a user to choose a version,
     * and may not be complete or even populated at all.
     * <p>
     *
     * The default implementation returns an empty list.
     *
     * @param projectConfiguration generic project and global configuration.
     * @param context context for interacting with Maven and other parts of the system.
     *
     * @return a list of database versions.
     *
     * @throws BoxDatabaseException if an error occurs.
     *
     * @since 3.0
     */
    public default List<String> availableVersions(ProjectConfiguration projectConfiguration, BoxContext context)
    throws BoxDatabaseException
    {
        return Collections.emptyList();
    }
}
