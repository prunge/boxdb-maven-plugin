package au.net.causal.maven.plugins.boxdb;

import au.net.causal.maven.plugins.boxdb.db.BoxDatabaseException;
import au.net.causal.maven.plugins.boxdb.db.DockerService;
import io.fabric8.maven.docker.access.DockerAccess;
import io.fabric8.maven.docker.access.DockerAccessException;
import io.fabric8.maven.docker.access.DockerConnectionDetector;
import io.fabric8.maven.docker.access.DockerMachine;
import io.fabric8.maven.docker.access.hc.DockerAccessWithHcClient;
import io.fabric8.maven.docker.config.DockerMachineConfiguration;
import io.fabric8.maven.docker.config.RegistryAuthConfiguration;
import io.fabric8.maven.docker.log.LogOutputSpecFactory;
import io.fabric8.maven.docker.service.ImagePullManager;
import io.fabric8.maven.docker.service.ServiceHub;
import io.fabric8.maven.docker.service.ServiceHubFactory;
import io.fabric8.maven.docker.util.AnsiLogger;
import io.fabric8.maven.docker.util.AuthConfigFactory;
import io.fabric8.maven.docker.util.Logger;
import org.apache.maven.execution.MavenSession;
import org.apache.maven.plugin.AbstractMojo;
import org.apache.maven.plugin.MojoExecutionException;
import org.apache.maven.plugin.MojoFailureException;
import org.apache.maven.plugins.annotations.Component;
import org.apache.maven.plugins.annotations.Parameter;
import org.apache.maven.project.MavenProject;
import org.apache.maven.settings.Settings;
import org.codehaus.plexus.PlexusContainer;
import org.codehaus.plexus.context.Context;
import org.codehaus.plexus.context.ContextException;
import org.codehaus.plexus.personality.plexus.lifecycle.phase.Contextualizable;

import java.io.IOException;
import java.net.URI;
import java.net.URISyntaxException;
import java.util.ArrayList;
import java.util.List;
import java.util.Properties;

//Mostly copy+pate from AbstractDockerMojo but without inheriting all the properties that
//we don't need
public abstract class AbstractDockerDbMojo extends AbstractMojo implements Contextualizable
{
    @Parameter(defaultValue = "${project}", readonly = true)
    protected MavenProject project;

    @Parameter(defaultValue = "${session}", readonly = true)
    protected MavenSession session;

    @Parameter(defaultValue = "${settings}", readonly = true)
    protected Settings settings;

    @Component
    protected ServiceHubFactory serviceHubFactory;

    @Parameter(property = "docker.host")
    protected String dockerHost;

    @Parameter(property = "docker.certPath")
    protected String certPath;

    @Parameter(property = "docker.maxConnections", defaultValue = "100")
    protected int maxConnections;

    /**
     * Customize configuration of docker machine.
     */
    @Parameter
    private DockerMachineConfiguration machine;

    @Parameter(property = "docker.skip.machine", defaultValue = "false")
    private boolean skipMachine;

    // Authentication information
    @Parameter
    protected RegistryAuthConfiguration authConfig;

    /**
     * If enabled, Docker log messages use color/ANSI.  This can interfere with Maven's own color/ANSI messages, so it is disabled by default.
     * BoxDB plugin versions before 3.3 used ANSI logging by default, so set this to true to get previous behaviour.
     *
     * @since 3.3
     */
    @Parameter(property = "boxdb.ansiLogging", defaultValue = "false", required = true)
    private boolean ansiLogging;

    /**
     * Skip extended authentication for Docker.
     */
    @Parameter(property = "docker.skip.extendedAuth", defaultValue = "false")
    protected boolean skipExtendedAuth;


    protected AuthConfigFactory authConfigFactory;

    private String dockerUrl;
    private String dockerHostAddress;

    private DockerService dockerService;
    private Logger dockerLog;
    protected LogOutputSpecFactory logSpecFactory;

    @Override
    public void execute() throws MojoExecutionException, MojoFailureException
    {
        boolean useColor = false;
        String verbose = "false";
        boolean logStdout = false;
        String logDate = null;
        if (ansiLogging)
            dockerLog = new AnsiLogger(this.getLog(), useColor, verbose, false, "DOCKER: ");
        else
            dockerLog = new PlainLogger(this.getLog(), verbose, false, "DOCKER: ");

        authConfigFactory.setLog(dockerLog);
        logSpecFactory = new LogOutputSpecFactory(useColor, logStdout, logDate);

        ExceptionalSupplier<DockerService, BoxDatabaseException> serviceHubSupplier = () ->
        {
            if (dockerService == null)
                dockerService = createDockerService();

            return dockerService;
        };
        try
        {
            executeInternal(serviceHubSupplier);
        }
        catch (DockerAccessException e)
        {
            throw new MojoExecutionException("Error running docker: " + e, e);
        }
        finally
        {
            if (dockerService != null)
                dockerService.getServiceHub().getDockerAccess().shutdown();
        }
    }

    protected abstract void executeInternal(ExceptionalSupplier<DockerService, BoxDatabaseException> service)
    throws MojoExecutionException, MojoFailureException, DockerAccessException;

    private DockerService createDockerService()
    throws BoxDatabaseException
    {
        ServiceHub serviceHub = createServiceHub();
        return new DockerService(serviceHub, dockerHostAddress);
    }

    private ServiceHub createServiceHub()
    throws BoxDatabaseException
    {
        DockerAccess dockerAccess = createDockerAccess(null, dockerLog);
        return serviceHubFactory.createServiceHub(project, session, dockerAccess, dockerLog, logSpecFactory);
    }

    protected List<DockerConnectionDetector.DockerHostProvider> getDockerHostProviders(Logger log)
    {
        DockerMachineConfiguration config = this.machine;
        if(config == null)
        {
            Properties ret = this.project.getProperties();
            if(!this.skipMachine && ret.containsKey(DockerMachineConfiguration.DOCKER_MACHINE_NAME_PROP))
            {
                config = new DockerMachineConfiguration(ret.getProperty(DockerMachineConfiguration.DOCKER_MACHINE_NAME_PROP),
                                                        ret.getProperty(DockerMachineConfiguration.DOCKER_MACHINE_AUTO_CREATE_PROP),
                                                        ret.getProperty(DockerMachineConfiguration.DOCKER_MACHINE_REGENERATE_CERTS_AFTER_START_PROP));
            }
        }

        ArrayList<DockerConnectionDetector.DockerHostProvider> results = new ArrayList<>();
        results.add(new DockerMachine(log, config));
        return results;
    }

    private DockerConnectionDetector createDockerConnectionDetector(Logger log)
    {
        return new DockerConnectionDetector(getDockerHostProviders(log));
    }

    private DockerAccess createDockerAccess(String minimalVersion, Logger log) throws BoxDatabaseException
    {
        DockerAccessWithHcClient access = null;

        try
        {
            DockerConnectionDetector e = createDockerConnectionDetector(log);
            DockerConnectionDetector.ConnectionParameter connectionParam = e.detectConnectionParameter(this.dockerHost, this.certPath);
            access = new DockerAccessWithHcClient(connectionParam.getUrl(), connectionParam.getCertPath(), this.maxConnections, log);
            access.start();
            this.setDockerHostAddressProperty(project.getProperties(), connectionParam.getUrl());
        }
        catch (IOException e)
        {
            throw new BoxDatabaseException("Cannot create docker access object ", e);
        }

        return access;
    }

    private void setDockerHostAddressProperty(Properties projectProperties, String dockerUrl) 
    throws BoxDatabaseException 
    {
        final String host;
        if (projectProperties.getProperty("docker.host.address") == null) 
        {
            try 
            {
                URI uri = new URI(dockerUrl);
                if (uri.getHost() == null && (uri.getScheme().equals("unix") || uri.getScheme().equals("npipe"))) 
                    host = "localhost";
                else 
                    host = uri.getHost();
            } 
            catch (URISyntaxException e) 
            {
                throw new BoxDatabaseException("Cannot parse " + dockerUrl + " as URI: " + e.getMessage(), e);
            }
            projectProperties.setProperty("docker.host.address", host == null ? "" : host);
        }
        else
            host = projectProperties.getProperty("docker.host.address");
        
        dockerHostAddress = host;
    }
    
    @Override
    public void contextualize(Context context) throws ContextException
    {
        this.authConfigFactory = new AuthConfigFactory((PlexusContainer)context.get("plexus"));
    }

    protected RegistryAuthConfiguration getAuthConfig()
    {
        return authConfig;
    }

    protected boolean isSkipExtendedAuth()
    {
        return skipExtendedAuth;
    }

    protected ImagePullManager.CacheStore getSessionCacheStore()
    {
        return new ImagePullManager.CacheStore()
        {
            @Override
            public String get(String key)
            {
                Properties userProperties = session.getUserProperties();
                return userProperties.getProperty(key);
            }

            @Override
            public void put(String key, String value)
            {
                Properties userProperties = session.getUserProperties();
                userProperties.setProperty(key, value);
            }
        };
    }

    public Logger getDockerLog()
    {
        return dockerLog;
    }
}
