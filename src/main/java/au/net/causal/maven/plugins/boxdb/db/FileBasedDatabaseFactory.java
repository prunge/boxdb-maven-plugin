package au.net.causal.maven.plugins.boxdb.db;

import java.io.IOException;
import java.nio.file.Path;
import java.nio.file.Paths;
import java.util.Objects;

public abstract class FileBasedDatabaseFactory implements BoxDatabaseFactory
{
    private final String name;

    protected FileBasedDatabaseFactory(String name)
    {
        Objects.requireNonNull(name, "name == null");
        this.name = name;
    }

    @Override
    public String name()
    {
        return name;
    }

    protected void initializeDefaults(BoxConfiguration boxConfiguration, ProjectConfiguration projectConfiguration, BoxContext context)
    throws BoxDatabaseException
    {
        if (boxConfiguration.getDatabaseName() == null)
            boxConfiguration.setDatabaseName("app");
        if (boxConfiguration.getContainerName() == null)
            boxConfiguration.setContainerName(boxConfiguration.getDatabaseName());
        if (boxConfiguration.getAdminUser() == null)
            boxConfiguration.setAdminUser(boxConfiguration.getDatabaseName());
        if (boxConfiguration.getAdminPassword() == null)
            boxConfiguration.setAdminPassword(boxConfiguration.getAdminUser());
        if (boxConfiguration.getDatabaseUser() == null)
            boxConfiguration.setDatabaseUser(boxConfiguration.getAdminUser());
        if (boxConfiguration.getDatabasePassword() == null)
            boxConfiguration.setDatabasePassword(boxConfiguration.getAdminPassword());
        try
        {
            if (boxConfiguration.getDatabaseFile() == null)
                boxConfiguration.setDatabaseFile(databaseHome(boxConfiguration, projectConfiguration, context));
        }
        catch (IOException e)
        {
            throw new BoxDatabaseException("Error initializing database home directory: " + e, e);
        }
    }

    protected Path databaseHome(BoxConfiguration boxConfiguration, ProjectConfiguration projectConfiguration, BoxContext context)
    throws IOException
    {
        Path home;
        boolean projectExists = (projectConfiguration != null &&
                projectConfiguration.getProject() != null &&
                projectConfiguration.getProject().getFile() != null &&
                projectConfiguration.getProject().getFile().exists() &&
                projectConfiguration.getProject().getBuild() != null &&
                projectConfiguration.getProject().getBuild().getDirectory() != null);

        if (!projectExists)
            home = context.getGlobalConfigDirectory().resolve(name()).resolve(boxConfiguration.getContainerName());
        else
        {
            String buildDir = projectConfiguration.getProject().getBuild().getDirectory();
            home = Paths.get(buildDir).resolve(name()).resolve(boxConfiguration.getContainerName());
        }

        return home;
    }
}
