package au.net.causal.maven.plugins.boxdb.vagrant;

import java.util.Objects;
import java.util.StringJoiner;

public class BoxUpdateStatus
{
    private final String name;
    private final String provider;

    private final String localVersion;
    private final String remoteVersion;

    private final State state;

    private final String detail;

    public BoxUpdateStatus(String name, String provider, State state, String localVersion, String remoteVersion, String detail)
    {
        Objects.requireNonNull(name, "name == null");
        Objects.requireNonNull(provider, "provider == null");
        Objects.requireNonNull(state, "state == null");

        this.name = name;
        this.provider = provider;
        this.localVersion = localVersion;
        this.remoteVersion = remoteVersion;
        this.state = state;
        this.detail = detail;
    }

    public String getName()
    {
        return name;
    }

    public String getProvider()
    {
        return provider;
    }

    public State getState()
    {
        return state;
    }

    public String getLocalVersion()
    {
        return localVersion;
    }

    public String getRemoteVersion()
    {
        return remoteVersion;
    }

    public String getDetail()
    {
        return detail;
    }

    @Override
    public String toString()
    {
        return new StringJoiner(", ", BoxUpdateStatus.class.getSimpleName() + "[", "]")
                .add("name=" + name)
                .add("provider=" + provider)
                .add("localVersion=" + localVersion)
                .add("remoteVersion=" + remoteVersion)
                .add("state=" + state)
                .add("detail=" + detail)
                .toString();
    }

    public static enum State
    {
        UP_TO_DATE,
        OUTDATED,
        NO_VERSION_INFORMATION,
        ERROR;
    }
}
