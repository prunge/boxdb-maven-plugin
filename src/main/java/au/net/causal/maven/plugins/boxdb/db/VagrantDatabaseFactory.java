package au.net.causal.maven.plugins.boxdb.db;

import au.net.causal.maven.plugins.boxdb.vagrant.LocalVagrant;
import au.net.causal.maven.plugins.boxdb.vagrant.Vagrant;
import au.net.causal.maven.plugins.boxdb.vagrant.VagrantException;
import org.codehaus.plexus.util.cli.Commandline;

import java.io.IOException;
import java.io.InputStream;
import java.net.URL;
import java.nio.file.Files;
import java.nio.file.Path;
import java.nio.file.StandardCopyOption;
import java.util.List;
import java.util.Objects;

public abstract class VagrantDatabaseFactory implements BoxDatabaseFactory
{
    private final String name;

    protected VagrantDatabaseFactory(String name)
    {
        Objects.requireNonNull(name, "name == null");
        this.name = name;
    }

    @Override
    public String name()
    {
        return name;
    }

    @Override
    public BoxDatabase create(BoxConfiguration boxConfiguration, ProjectConfiguration projectConfiguration, BoxContext context)
    throws BoxDatabaseException
    {
        Objects.requireNonNull(boxConfiguration, "boxConfiguration == null");
        Objects.requireNonNull(context, "context == null");

        initializeDefaults(boxConfiguration);

        return createVagrantDatabase(boxConfiguration, projectConfiguration, context);
    }

    protected abstract VagrantDatabase createVagrantDatabase(Vagrant vagrant, Path baseDirectory,
                                                             Path vagrantFile,
                                                             BoxConfiguration boxConfiguration,
                                                             ProjectConfiguration projectConfiguration,
                                                             BoxContext context)
    throws BoxDatabaseException, IOException;

    private Vagrant createVagrantExecutor(BoxConfiguration boxConfiguration, ProjectConfiguration projectConfiguration, BoxContext context)
    {
        //TODO toolchain lookup, etc.
        Commandline commandLine = new Commandline("vagrant");

        Vagrant vagrant = new LocalVagrant(commandLine, context.getLog());
        return vagrant;
    }

    private VagrantDatabase createVagrantDatabase(BoxConfiguration boxConfiguration,
                                                     ProjectConfiguration projectConfiguration,
                                                     BoxContext context)
    throws BoxDatabaseException
    {
        Vagrant vagrant = createVagrantExecutor(boxConfiguration, projectConfiguration, context);

        //Make a new directory under project (or temp directory) for this container
        try
        {
            Path containerDirectory = context.getGlobalConfigDirectory().resolve(boxConfiguration.getContainerName());
            Files.createDirectories(containerDirectory);

            Path vagrantFile = containerDirectory.resolve("Vagrantfile");
            copyResource(vagrantFileResource(), vagrantFile);

            return createVagrantDatabase(vagrant, containerDirectory, vagrantFile,
                                        boxConfiguration, projectConfiguration, context);
        }
        catch (IOException e)
        {
            throw new BoxDatabaseException("Failed to create container directory: " + e, e);
        }
    }

    protected void copyResource(URL resource, Path toFile)
    throws IOException
    {
        try (InputStream is = resource.openStream())
        {
            Files.copy(is, toFile, StandardCopyOption.REPLACE_EXISTING);
        }
    }

    protected void initializeDefaults(BoxConfiguration boxConfiguration)
    {
        if (boxConfiguration.getDatabaseUser() == null)
            boxConfiguration.setDatabaseUser(boxConfiguration.getAdminUser());
        if (boxConfiguration.getDatabasePassword() == null)
            boxConfiguration.setDatabasePassword(boxConfiguration.getAdminPassword());
        if (boxConfiguration.getDatabaseName() == null)
            boxConfiguration.setDatabaseName("app");
        if (boxConfiguration.getContainerName() == null)
            boxConfiguration.setContainerName("boxdb-" + boxConfiguration.getDatabaseType() + "-" + boxConfiguration.getDatabaseVersion());
    }

    protected abstract URL vagrantFileResource()
    throws IOException;

    protected void verifyAndInstallVagrantPluginIfNeeded(String pluginName, Vagrant vagrant, BoxContext context)
    throws BoxDatabaseException
    {
        try
        {
            //Check Vagrant plugins
            Vagrant.PluginListOptions pluginListOptions = new Vagrant.PluginListOptions();
            List<? extends Vagrant.Plugin> plugins = vagrant.pluginList(pluginListOptions);
            boolean pluginInstalled = plugins.stream().anyMatch(plugin -> pluginName.equals(plugin.getName()));
            context.getLog().debug("Plugin '" + pluginName + "' installed: " + pluginInstalled);
            if (!pluginInstalled)
            {
                context.getLog().info("Installing required Vagrant plugin " + pluginName);
                Vagrant.PluginInstallOptions installOptions = new Vagrant.PluginInstallOptions(pluginName);
                vagrant.pluginInstall(installOptions);
            }
        }
        catch (VagrantException e)
        {
            throw new BoxDatabaseException("Error validating/installing Vagrant plugins: " + e, e);
        }
    }
}
