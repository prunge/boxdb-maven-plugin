package au.net.causal.maven.plugins.boxdb.db;

import au.net.causal.maven.plugins.boxdb.DependencyUtils;
import org.eclipse.aether.version.Version;

import java.util.List;
import java.util.Objects;
import java.util.stream.Collectors;

@BoxDbBundled
public class HsqlFactory extends FileBasedDatabaseFactory
{
    public HsqlFactory()
    {
        super("hsql");
    }

    @Override
    public HsqlDatabase create(BoxConfiguration boxConfiguration, ProjectConfiguration projectConfiguration, BoxContext context)
    throws BoxDatabaseException
    {
        Objects.requireNonNull(boxConfiguration, "boxConfiguration == null");
        Objects.requireNonNull(context, "context == null");

        initializeDefaults(boxConfiguration, projectConfiguration, context);

        return new HsqlDatabase(boxConfiguration, projectConfiguration, context);
    }

    protected void initializeDefaults(BoxConfiguration boxConfiguration, ProjectConfiguration projectConfiguration, BoxContext context)
    throws BoxDatabaseException
    {
        super.initializeDefaults(boxConfiguration, projectConfiguration, context);
        if (boxConfiguration.getDatabaseVersion() == null)
            boxConfiguration.setDatabaseVersion("2.5.1"); //2.5.x is latest version that supports Java 8
        if (boxConfiguration.getDatabasePort() <= 0)
            boxConfiguration.setDatabasePort(9001);

        //Admin username/password is hardcoded
        boxConfiguration.setAdminUser("SA");
        boxConfiguration.setAdminPassword("");

        CollationTranslator collationTranslator = new CollationTranslator(null, this::mapCommonCollation);
        collationTranslator.processCollation(boxConfiguration);
    }

    private String mapCommonCollation(CommonCollation collation)
    {
        switch (collation)
        {
            case BINARY:
                return null;
            case CASE_INSENSITIVE:
                return "English 1";
            default:
                throw new Error("Unknown common collation: " + collation);
        }
    }

    @Override
    public List<String> availableVersions(ProjectConfiguration projectConfiguration, BoxContext context)
    throws BoxDatabaseException
    {
        List<? extends Version> versions = DependencyUtils.findAvailableVersions(HsqlDatabase.HSQL_DATABASE_GROUP_ID,
                                                                                 HsqlDatabase.HSQL_DATABASE_ARTIFACT_ID,
                                                                                 "jar", context.getRepositorySystem(),
                                                                                 context.getRepositorySystemSession(),
                                                                                 context.getRemoteRepositories());
        return versions.stream().map(Version::toString).collect(Collectors.toList());
    }
}
