package au.net.causal.maven.plugins.boxdb;

/**
 * Supplier of results that can throw a checked exception.
 *
 * @param <T> the type of results supplied by this supplier.
 * @param <E> the type of exception that may be thrown.
 *
 * @see java.util.function.Supplier
 */
public interface ExceptionalSupplier<T, E extends Exception>
{
    /**
     * Gets a result.
     *
     * @return a result.
     *
     * @throws E if an error occurs.
     */
    public T get()
    throws E;
}
