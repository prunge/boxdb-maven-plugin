package au.net.causal.maven.plugins.boxdb.db;

import java.io.IOException;
import java.util.List;
import java.util.stream.Collectors;

@BoxDbBundled
public class CockroachDbFactory extends DockerDatabaseFactory
{
    static final String COCKROACH_DB_DOCKER_REPOSITORY = "cockroachdb/cockroach";

    public CockroachDbFactory()
    {
        super("cockroachdb");
    }

    protected String getCockroachDbDockerRepository()
    {
        return COCKROACH_DB_DOCKER_REPOSITORY;
    }

    @Override
    protected void initializeDefaults(BoxConfiguration boxConfiguration)
    {
        if (boxConfiguration.getAdminUser() == null)
            boxConfiguration.setAdminUser("root");
        if (boxConfiguration.getAdminPassword() == null)
            boxConfiguration.setAdminPassword("");
        if (boxConfiguration.getDatabaseName() == null)
            boxConfiguration.setDatabaseName("app");
        if (boxConfiguration.getDatabaseVersion() == null)
            boxConfiguration.setDatabaseVersion("23.1.23");
        if (boxConfiguration.getDatabasePort() <= 0)
            boxConfiguration.setDatabasePort(26257);
        if (boxConfiguration.getDatabaseUser() == null)
            boxConfiguration.setDatabaseUser(boxConfiguration.getAdminUser());
        if (boxConfiguration.getDatabasePassword() == null)
            boxConfiguration.setDatabasePassword("");

        super.initializeDefaults(boxConfiguration);
    }

    @Override
    protected CockroachDbDatabase createDockerDatabase(BoxConfiguration boxConfiguration, ProjectConfiguration projectConfiguration, BoxContext context)
    throws BoxDatabaseException
    {
        try
        {
            return new CockroachDbDatabase(boxConfiguration, projectConfiguration, context, dockerRegistry());
        }
        catch (IOException e)
        {
            throw new BoxDatabaseException("I/O error creating database: " + e, e);
        }
    }

    @Override
    public List<String> availableVersions(ProjectConfiguration projectConfiguration, BoxContext context)
    throws BoxDatabaseException
    {
        try
        {
            //There's a lot of snapshot/pre-release versions in the list so only include the ones users
            //would typically be interested in - the ones that are prefixed with 'v'
            return dockerRegistry().readTags(getCockroachDbDockerRepository())
                                   .stream()
                                   .filter(version -> version.startsWith("v"))
                                   .map(version -> version.substring(1))
                                   .collect(Collectors.toList());
        }
        catch (IOException e)
        {
            throw new BoxDatabaseException("Error reading tags: " + e.getMessage(), e);
        }
    }
}
