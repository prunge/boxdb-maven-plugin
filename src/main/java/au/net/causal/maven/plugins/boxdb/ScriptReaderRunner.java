package au.net.causal.maven.plugins.boxdb;

import au.net.causal.maven.plugins.boxdb.db.BoxConfiguration;
import au.net.causal.maven.plugins.boxdb.db.BoxContext;
import au.net.causal.maven.plugins.boxdb.db.BoxDatabase;
import au.net.causal.maven.plugins.boxdb.db.BoxDatabaseException;
import au.net.causal.maven.plugins.boxdb.db.DatabaseTarget;
import au.net.causal.maven.plugins.boxdb.db.ProjectConfiguration;
import au.net.causal.maven.plugins.boxdb.db.ScriptReaderExecution;
import org.apache.maven.plugin.MojoExecutionException;
import org.apache.maven.shared.filtering.MavenFilteringException;
import org.apache.maven.shared.filtering.MavenReaderFilter;
import org.apache.maven.shared.filtering.MavenReaderFilterRequest;

import java.io.IOException;
import java.io.InputStreamReader;
import java.io.Reader;
import java.net.URL;
import java.nio.charset.StandardCharsets;
import java.sql.SQLException;
import java.time.Duration;
import java.util.Collections;
import java.util.List;
import java.util.Objects;
import java.util.Properties;

/**
 * Run SQL scripts.
 */
public class ScriptReaderRunner
{
    private final BoxConfiguration boxConfiguration;
    private final BoxDatabase boxDatabase;
    private final ProjectConfiguration projectConfiguration;
    private final BoxContext context;
    private final MavenReaderFilter readerFilter;

    public ScriptReaderRunner(BoxDatabase boxDatabase, BoxConfiguration boxConfiguration,
                              ProjectConfiguration projectConfiguration, BoxContext context,
                              MavenReaderFilter readerFilter)
    {
        this.boxDatabase = boxDatabase;
        this.boxConfiguration = boxConfiguration;
        this.projectConfiguration = projectConfiguration;
        this.context = context;
        this.readerFilter = readerFilter;
    }

    public void execute(ScriptReaderExecution execution, DatabaseTarget targetDatabase, Duration timeout)
    throws IOException, SQLException, BoxDatabaseException, MojoExecutionException
    {
        Objects.requireNonNull(execution, "execution == null");

        List<URL> scripts = execution.getScripts();

        if (execution.isFiltering())
        {
            for (URL scriptFile : scripts)
            {
                if (execution.isSilentExecution())
                    context.getLog().debug("Executing script " + scriptFile.toExternalForm());
                else
                    context.getLog().info("Executing script " + scriptFile.toExternalForm());

                try (Reader reader = new InputStreamReader(scriptFile.openConnection().getInputStream(), StandardCharsets.UTF_8))
                {
                    Properties extraProperties = new Properties();
                    boxConfiguration.toProperties(extraProperties);

                    MavenReaderFilterRequest filterRequest = new MavenReaderFilterRequest(reader, true, projectConfiguration.getProject(),
                                                                Collections.emptyList(), true, context.getSession(),
                                                                extraProperties);
                    try (Reader filteredReader = readerFilter.filter(filterRequest))
                    {
                        switch (execution.getMode())
                        {
                            case JDBC:
                                boxDatabase.executeJdbcScript(filteredReader, targetDatabase, execution.isSilentExecution());
                                break;
                            case NATIVE:
                                boxDatabase.executeScript(filteredReader, targetDatabase, timeout, execution.isSilentExecution());
                                break;
                            default:
                                throw new Error("Unsupported execution mode: " + execution.getMode());
                        }
                    }
                }
                catch (MavenFilteringException e)
                {
                    throw new MojoExecutionException("Error filtering script " + scriptFile.toExternalForm() + ": " + e, e);
                }
            }
        }
        else
        {
            for (URL scriptFile : scripts)
            {
                if (execution.isSilentExecution())
                    context.getLog().debug("Executing script " + scriptFile.toExternalForm());
                else
                    context.getLog().info("Executing script " + scriptFile.toExternalForm());

                boxDatabase.executeScript(scriptFile, targetDatabase, timeout, execution.isSilentExecution());
            }
        }
    }
}
