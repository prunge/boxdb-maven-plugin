package au.net.causal.maven.plugins.boxdb;

import io.fabric8.maven.docker.util.Logger;
import org.apache.maven.plugin.logging.Log;
import org.codehaus.plexus.util.StringUtils;

import java.io.Closeable;
import java.io.File;
import java.io.IOException;
import java.util.ArrayList;
import java.util.Arrays;
import java.util.Collections;
import java.util.HashMap;
import java.util.List;
import java.util.Map;
import java.util.concurrent.atomic.AtomicInteger;
import java.util.function.Consumer;
import java.util.function.Predicate;

/**
 * Copy of {@link io.fabric8.maven.docker.util.AnsiLogger} but without all the ANSI stuff, used because AnsiLogger can break
 * Maven's own ANSI logging/colorization.  This logger just logs formatted, non-ANSI messages.
 *
 * @since 3.3
 */
public class PlainLogger implements Logger, Closeable {

    // prefix used for console output
    public static final String DEFAULT_LOG_PREFIX = "DOCKER> ";
    private static final int NON_ANSI_UPDATE_PERIOD = 80;

    private final Log log;
    private final String prefix;
    private final boolean batchMode;

    private boolean isVerbose = false;
    private List<LogVerboseCategory> verboseModes = null;

    // Map remembering lines
    private ThreadLocal<Map<String, Integer>> imageLines = new ThreadLocal<>();
    private ThreadLocal<AtomicInteger> updateCount = new ThreadLocal<>();

    public PlainLogger(Log log, String verbose) {
        this(log, verbose, false);
    }

    public PlainLogger(Log log, String verbose, boolean batchMode) {
        this(log, verbose, batchMode, DEFAULT_LOG_PREFIX, null);
    }

    public PlainLogger(Log log, String verbose, boolean batchMode, String prefix) {
        this(log, verbose, batchMode, prefix, null);
    }

    public PlainLogger(Log log, String verbose, boolean batchMode, String prefix, File outpufFile) {
        this.log = log;
        this.prefix = prefix;
        this.batchMode = batchMode;
        checkVerboseLoggingEnabled(verbose);
    }

    /** {@inheritDoc} */
    public void debug(String message, Object ... params) {
        if (isDebugEnabled()) {
            logOrPrintToFile(
                    log -> true,
                    log -> log.debug(prefix + format(message, params)),
                    message,
                    params);
        }
    }

    /** {@inheritDoc} */
    public void info(String message, Object ... params) {
        logOrPrintToFile(
                log -> log.isInfoEnabled(),
                log -> log.info(formatLogMessage(message, true, params)),
                message,
                params);
    }

    /** {@inheritDoc} */
    public void verbose(LogVerboseCategory logVerboseCategory, String message, Object ... params) {
        if (isVerbose && verboseModes != null && verboseModes.contains(logVerboseCategory)) {
            logOrPrintToFile(
                    log -> true,
                    log -> log.info(format(message, params)),
                    message,
                    params);
        }
    }

    /** {@inheritDoc} */
    public void warn(String message, Object ... params) {
        logOrPrintToFile(
                log -> log.isWarnEnabled(),
                log -> log.warn(formatLogMessage(message, true, params)),
                message,
                params);
    }

    /** {@inheritDoc} */
    public void error(String message, Object ... params) {
        logOrPrintToFile(
                log -> log.isErrorEnabled(),
                log -> log.error(formatLogMessage(message, true, params)),
                message,
                params);
    }

    @Override
    public String errorMessage(String message) {
        return formatLogMessage(message,false);
    }

    /**
     * Whether debugging is enabled.
     */
    public boolean isDebugEnabled() {
        return log.isDebugEnabled();
    }

    public boolean isVerboseEnabled() {
        return isVerbose;
    }

    /**
     * Start a progress bar
     */
    public void progressStart() {
        // A progress indicator is always written out to standard out if a tty is enabled.
        if (!batchMode && log.isInfoEnabled()) {
            imageLines.remove();
            updateCount.remove();
            imageLines.set(new HashMap<String, Integer>());
            updateCount.set(new AtomicInteger());
        }
    }

    /**
     * Update the progress
     */
    public void progressUpdate(String layerId, String status, String progressMessage) {
        if (!batchMode && log.isInfoEnabled() && StringUtils.isNotEmpty(layerId)) {
            updateNonAnsiProgress(layerId);
            flush();
        }
    }

    private void updateNonAnsiProgress(String imageId) {
        AtomicInteger count = updateCount.get();
        int nr = count.getAndIncrement();
        if (nr % NON_ANSI_UPDATE_PERIOD == 0) {
            print("#");
        }
        if (nr > 0 && nr % (80 * NON_ANSI_UPDATE_PERIOD) == 0) {
            print("\n");
        }
    }

    /**
     * Finis progress meter. Must be always called if {@link #progressStart()} has been used.
     */
    public void progressFinished() {
        if (!batchMode && log.isInfoEnabled()) {
            imageLines.remove();
            println("");
        }
    }

    private void flush() {
        System.out.flush();
    }

    private void println(String txt) {
        System.out.println(txt);
    }

    private void print(String txt) {
        System.out.print(txt);
    }

    private String formatLogMessage(String message, boolean addPrefix, Object ... params) {
        String msgToPrint = addPrefix ? prefix + message : message;
        return format(evaluateEmphasis(msgToPrint), params);
    }

    // Use parameters when given, otherwise we use the string directly
    private String format(String message, Object[] params) {
        if (params.length == 0) {
            return message;
        } else if (params.length == 1 && params[0] instanceof Throwable) {
            // We print only the message here since breaking exception will bubble up
            // anyway
            return message + ": " + params[0].toString();
        } else {
            return String.format(message, params);
        }
    }

    // Emphasize parts encloses in "[[*]]" tags
    private String evaluateEmphasis(String message) {
        // Split but keep the content by splitting on [[ and ]] separately when they
        // are followed or preceded by their counterpart. This lets the split retain
        // the character in the center.
        String[] parts = message.split("(\\[\\[(?=.]])|(?<=\\[\\[.)]])");
        if (parts.length == 1) {
            return message;
        }
        // The split up string is comprised of a leading plain part, followed
        // by groups of colorization that are <SET> color-part <RESET> plain-part.
        // To avoid emitting needless color changes, we skip the set or reset
        // if the subsequent part is empty.
        StringBuilder ret = new StringBuilder(parts[0]);

        for (int i = 1; i < parts.length; i += 4) {
            boolean colorPart = i + 1 < parts.length && parts[i + 1].length() > 0;
            boolean plainPart = i + 3 < parts.length && parts[i + 3].length() > 0;

            if (colorPart) {
                ret.append(parts[i + 1]);
            }
            if (plainPart) {
                ret.append(parts[i + 3]);
            }
        }
        return ret.toString();
    }

    private void checkVerboseLoggingEnabled(String verbose) {
        if (verbose == null || verbose.equalsIgnoreCase("false")) {
            this.isVerbose = false;
            return;
        }
        if (verbose.equalsIgnoreCase("all")) {
            this.isVerbose = true;
            this.verboseModes = Arrays.asList(LogVerboseCategory.values());
            return;
        }
        if (verbose.equals("") || verbose.equalsIgnoreCase("true")) {
            this.isVerbose = true;
            this.verboseModes = Collections.singletonList(LogVerboseCategory.BUILD);
            return;
        }

        this.verboseModes = getVerboseModesFromString(verbose);
        this.isVerbose = true;
    }

    private List<LogVerboseCategory> getVerboseModesFromString(String groups) {
        List<LogVerboseCategory> ret = new ArrayList<>();
        for (String group : groups.split(",")) {
            try {
                ret.add(LogVerboseCategory.valueOf(group.toUpperCase()));
            } catch (Exception exp) {
                log.info("log: Unknown verbosity group " + groups + ". Ignoring...");
            }
        }
        return ret;
    }

    private void logOrPrintToFile(Predicate<Log> logPredicate, Consumer<Log> logConsumer, String message, Object ... params) {
        logConsumer.accept(log);
    }

    @Override
    public void close() throws IOException {
    }
}