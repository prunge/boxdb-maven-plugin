package au.net.causal.maven.plugins.boxdb.db;

import org.codehaus.plexus.util.IOUtil;

import java.io.BufferedReader;
import java.io.IOException;
import java.io.Writer;
import java.nio.charset.Charset;
import java.nio.file.Files;
import java.nio.file.Path;
import java.util.Objects;

public class FileDatabaseLog implements DatabaseLog 
{
    private final String name;
    private final Path file;
    private final Charset charset;

    public FileDatabaseLog(String name, Path file, Charset charset) 
    {
        Objects.requireNonNull(name, "name == null");
        Objects.requireNonNull(file, "file == null");
        Objects.requireNonNull(charset, "charset == null");
        
        this.name = name;
        this.file = file;
        this.charset = charset;
    }

    @Override
    public String getName() 
    throws BoxDatabaseException 
    {
        return name;
    }

    @Override
    public void save(Writer w) 
    throws BoxDatabaseException, IOException 
    {
        if (Files.exists(file))
        {
            try (BufferedReader reader = Files.newBufferedReader(file, charset))
            {
                IOUtil.copy(reader, w);
            }
        }
    }
}
