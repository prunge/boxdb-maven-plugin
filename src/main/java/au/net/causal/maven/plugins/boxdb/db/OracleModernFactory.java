package au.net.causal.maven.plugins.boxdb.db;

import java.io.IOException;
import java.util.List;

@BoxDbBundled
public class OracleModernFactory extends DockerDatabaseFactory
{
    static final String ORACLE_MODERN_DOCKER_REPOSITORY = "gvenzl/oracle-xe";

    public OracleModernFactory()
    {
        super("oracle");
    }

    @Override
    protected void initializeDefaults(BoxConfiguration boxConfiguration)
    {
        if (boxConfiguration.getAdminUser() == null)
            boxConfiguration.setAdminUser("system");
        if (boxConfiguration.getAdminPassword() == null)
            boxConfiguration.setAdminPassword("oracle");
        if (boxConfiguration.getDatabaseName() == null && boxConfiguration.getDatabaseUser() != null)
            boxConfiguration.setDatabaseName(boxConfiguration.getDatabaseUser());
        if (boxConfiguration.getDatabaseName() == null)
            boxConfiguration.setDatabaseName("app");
        if (boxConfiguration.getDatabaseVersion() == null)
            boxConfiguration.setDatabaseVersion("18.4.0-full-faststart");
        if (boxConfiguration.getDatabasePort() <= 0)
            boxConfiguration.setDatabasePort(49161);
        if (boxConfiguration.getDatabaseUser() == null)
            boxConfiguration.setDatabaseUser(boxConfiguration.getDatabaseName());
        if (boxConfiguration.getDatabasePassword() == null)
            boxConfiguration.setDatabasePassword(boxConfiguration.getDatabaseUser());

        super.initializeDefaults(boxConfiguration);
    }

    protected String dockerImageName(BoxConfiguration boxConfiguration)
    throws BoxDatabaseException
    {
        return ORACLE_MODERN_DOCKER_REPOSITORY + ":" + boxConfiguration.getDatabaseVersion();
    }

    @Override
    protected OracleDatabase createDockerDatabase(BoxConfiguration boxConfiguration, ProjectConfiguration projectConfiguration, BoxContext context)
    throws BoxDatabaseException
    {
        try
        {
            return new OracleModernDatabase(boxConfiguration, projectConfiguration, context, dockerRegistry(),
                                            dockerImageName(boxConfiguration));
        }
        catch (IOException e)
        {
            throw new BoxDatabaseException("I/O error creating database: " + e, e);
        }
    }

    @Override
    public List<String> availableVersions(ProjectConfiguration projectConfiguration, BoxContext context)
            throws BoxDatabaseException
    {
        try
        {
            return dockerRegistry().readTags(ORACLE_MODERN_DOCKER_REPOSITORY);
        }
        catch (IOException e)
        {
            throw new BoxDatabaseException("Error reading tags: " + e.getMessage(), e);
        }
    }
}
