package au.net.causal.maven.plugins.boxdb.db;

import java.util.Objects;

public class ImageComponent
{
    private final String type;
    private final String name;
    private final ImageStatus status;
    private final String detail;

    public ImageComponent(String type, String name, ImageStatus status)
    {
        this(type, name, status, null);
    }

    public ImageComponent(String type, String name, ImageStatus status, String detail)
    {
        Objects.requireNonNull(type, "type == null");
        Objects.requireNonNull(name, "name == null");
        Objects.requireNonNull(status, "status == null");

        this.type = type;
        this.name = name;
        this.status = status;
        this.detail = detail;
    }

    public String getType()
    {
        return type;
    }

    public String getName()
    {
        return name;
    }

    public ImageStatus getStatus()
    {
        return status;
    }

    /**
     * @return extra details if they exist.  May return null if there is no extra detail.
     */
    public String getDetail()
    {
        return detail;
    }

    @Override
    public String toString()
    {
        return getName() + " (" + getType() + "): " + getStatus() + (detail != null ? " - " + detail : "");
    }

    /**
     * Indicates the status of a database image, its local-machine presence and whether updates are available for
     * download.
     */
    public enum ImageStatus
    {
        /**
         * Image is downloaded locally, but there is an update available remotely.
         */
        REMOTE_UPDATE_AVAILABLE,

        /**
         * Image does not exist locally and must be downloaded first for the database to be usable.
         */
        NOT_DOWNLOADED,

        /**
         * Image exists locally and is up-to-date.
         */
        DOWNLOADED,

        /**
         * Image is downloaded locally, but does not exist remotely.  This might happen when an image was downloaded
         * previously but then was removed from the remote repository.  Indicates that if the image was removed
         * locally then it could not be re-downloaded or rebuilt from the remote.
         */
        LOCAL_ONLY,

        /**
         * Image is not downloaded locally and does not exist remotely.
         */
        NOT_FOUND;
    }
}
