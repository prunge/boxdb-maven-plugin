package au.net.causal.maven.plugins.boxdb;

/**
 * An operation that takes a single argument and does not return anything, possibly throwing a checked exception.
 *
 * @param <T> the argument type.
 * @param <E> the exception type.
 *
 * @see java.util.function.Consumer
 */
@FunctionalInterface
public interface ExceptionalConsumer<T, E extends Exception>
{
    /**
     * Performs this operation on the given argument.
     *
     * @param t the input argument
     *
     * @throws E if an error occurs.
     */
    void accept(T t)
    throws E;
}
