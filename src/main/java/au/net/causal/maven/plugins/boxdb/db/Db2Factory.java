package au.net.causal.maven.plugins.boxdb.db;

import org.eclipse.aether.util.version.GenericVersionScheme;
import org.eclipse.aether.version.InvalidVersionSpecificationException;
import org.eclipse.aether.version.Version;

import java.io.IOException;
import java.net.URI;
import java.util.ArrayList;
import java.util.Arrays;
import java.util.LinkedHashSet;
import java.util.List;
import java.util.Set;
import java.util.regex.Pattern;

@BoxDbBundled
public class Db2Factory extends DockerDatabaseFactory
{
    public static final String DB2_TERRITORY_PROPERTY = "db2Territory";
    public static final String DB2_COLLATION_PROPERTY = "db2Collation";

    public Db2Factory()
    {
        super("db2");
    }

    @Override
    protected void initializeDefaults(BoxConfiguration boxConfiguration)
    {
        if (boxConfiguration.getAdminUser() == null)
            boxConfiguration.setAdminUser("db2inst1");
        if (boxConfiguration.getAdminPassword() == null)
            boxConfiguration.setAdminPassword("db2box");
        if (boxConfiguration.getDatabaseName() == null)
            boxConfiguration.setDatabaseName("app");
        if (boxConfiguration.getDatabaseVersion() == null)
            boxConfiguration.setDatabaseVersion("11.5.9.0");
        if (boxConfiguration.getDatabasePort() <= 0)
            boxConfiguration.setDatabasePort(50000);
        if (boxConfiguration.getDatabaseUser() == null)
            boxConfiguration.setDatabaseUser(boxConfiguration.getDatabaseName());
        if (boxConfiguration.getDatabasePassword() == null)
            boxConfiguration.setDatabasePassword(boxConfiguration.getDatabaseUser());
        
        EncodingTranslator encodingTranslator = new EncodingTranslator("UTF-8", this::mapCommonEncoding);
        encodingTranslator.processEncoding(boxConfiguration);
        CollationTranslator collationTranslator = new CollationTranslator("US;identity", this::mapCommonCollation);
        collationTranslator.processCollation(boxConfiguration);
        
        //Configure DB2-specific elements from the parsed collation
        configureDb2CollationProperties(boxConfiguration);
        
        super.initializeDefaults(boxConfiguration);
    }
    
    private void configureDb2CollationProperties(BoxConfiguration boxConfiguration)
    {
        String collation = boxConfiguration.getDatabaseCollation();
        if (collation == null)
            return;
        
        String[] colSplit = collation.split(Pattern.quote(";"), 2);
        String territory = colSplit[0];
        String db2Collation = "identity";
        if (colSplit.length > 1)
            db2Collation = colSplit[1];
        
        boxConfiguration.getConfiguration().putIfAbsent(DB2_TERRITORY_PROPERTY, territory);
        boxConfiguration.getConfiguration().putIfAbsent(DB2_COLLATION_PROPERTY, db2Collation);
    }

    private String mapCommonEncoding(CommonEncoding encoding)
    {
        switch (encoding)
        {
            case UNICODE:
                return "UTF-8";
            case ASCII:
                return "ISO8859-1";
            default:
                throw new Error("Unknown common encoding: " + encoding);
        }
    }

    private String mapCommonCollation(CommonCollation collation)
    {
        switch (collation)
        {
            case BINARY:
                return "US;identity";
            case CASE_INSENSITIVE:
                return "US;UCA500R1_LEN_S2";
            default:
                throw new Error("Unknown common collation: " + collation);
        }
    }

    @Override
    protected Db2Database createDockerDatabase(BoxConfiguration boxConfiguration, ProjectConfiguration projectConfiguration, BoxContext context)
    throws BoxDatabaseException
    {
        try 
        {
            DockerRegistry.Repository repository = dockerRepositoryForDatabaseVersion(boxConfiguration.getDatabaseVersion());
            return new Db2Database(boxConfiguration, projectConfiguration, context, repository);
        }
        catch (IOException e)
        {
            throw new BoxDatabaseException("I/O error creating database: " + e, e);
        }
    }

    @Override
    public List<String> availableVersions(ProjectConfiguration projectConfiguration, BoxContext context)
    throws BoxDatabaseException
    {
        //Combine from both docker hub and IBM's registry
        Set<String> allTags = new LinkedHashSet<>();

        //IBM docker registry
        for (DockerRegistry.Repository repository : Arrays.asList(ibmDockerRepository(), dockerHubRepository()))
        {
            try
            {
                allTags.addAll(repository.getRegistry().readTags(repository.getFullRepositoryName()));
            }
            catch (IOException e)
            {
                throw new BoxDatabaseException("Error reading tags: " + e.getMessage(), e);
            }
        }

        return new ArrayList<>(allTags);
    }

    @Override
    protected DockerRegistry dockerRegistry()
    {
        return ibmDockerRepository().getRegistry(); //IBM's registry has newer versions so default to that
    }

    private DockerRegistry.Repository ibmDockerRepository()
    {
        return new DockerRegistry.Repository(new DockerRegistry(URI.create("https://icr.io/v2/")), "db2_community/db2", "icr.io/db2_community/db2");
    }

    private DockerRegistry.Repository dockerHubRepository()
    {
        return new DockerRegistry.Repository(super.dockerRegistry(), "ibmcom/db2", "ibmcom/db2");
    }

    private DockerRegistry.Repository dockerRepositoryForDatabaseVersion(String version)
    {
        GenericVersionScheme versionScheme = new GenericVersionScheme();
        try
        {
            Version parsedVersion = versionScheme.parseVersion(version);
            Version dockerHubNewestVersion = versionScheme.parseVersion("11.5.8.0");
            if (parsedVersion.compareTo(dockerHubNewestVersion) <= 0)
                return dockerHubRepository(); //Docker hub
        }
        catch (InvalidVersionSpecificationException e)
        {
            //Not parsable, fallthrough
        }

        return ibmDockerRepository(); //DB2 registry
    }
}
