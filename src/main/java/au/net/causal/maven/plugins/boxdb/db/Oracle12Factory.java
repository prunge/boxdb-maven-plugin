package au.net.causal.maven.plugins.boxdb.db;

import java.io.IOException;

@BoxDbBundled
public class Oracle12Factory extends DockerDatabaseFactory
{
    static final String ORACLE_12_DOCKER_REPOSITORY = "ryangoh/oracle_12c_r1_xe";
    static final String ORACLE_12_DOCKER_TAG = "latest";
    private static final String ORACLE_12_IMAGE_NAME = ORACLE_12_DOCKER_REPOSITORY + ":" + ORACLE_12_DOCKER_TAG;

    public Oracle12Factory()
    {
        super("oracle");
    }

    @Override
    protected void initializeDefaults(BoxConfiguration boxConfiguration)
    {
        if (boxConfiguration.getAdminUser() == null)
            boxConfiguration.setAdminUser("system");
        if (boxConfiguration.getAdminPassword() == null)
            boxConfiguration.setAdminPassword("oracle");
        if (boxConfiguration.getDatabaseName() == null && boxConfiguration.getDatabaseUser() != null)
            boxConfiguration.setDatabaseName(boxConfiguration.getDatabaseUser());
        if (boxConfiguration.getDatabaseName() == null)
            boxConfiguration.setDatabaseName("app");
        if (boxConfiguration.getDatabaseVersion() == null)
            boxConfiguration.setDatabaseVersion("12c");
        if (boxConfiguration.getDatabasePort() <= 0)
            boxConfiguration.setDatabasePort(49161);
        if (boxConfiguration.getDatabaseUser() == null)
            boxConfiguration.setDatabaseUser(boxConfiguration.getDatabaseName());
        if (boxConfiguration.getDatabasePassword() == null)
            boxConfiguration.setDatabasePassword(boxConfiguration.getDatabaseUser());

        super.initializeDefaults(boxConfiguration);
    }

    protected String dockerImageName(BoxConfiguration boxConfiguration)
    throws BoxDatabaseException
    {
        return ORACLE_12_IMAGE_NAME;
    }

    @Override
    protected OracleDatabase createDockerDatabase(BoxConfiguration boxConfiguration, ProjectConfiguration projectConfiguration, BoxContext context)
    throws BoxDatabaseException
    {
        try
        {
            return new Oracle12Database(boxConfiguration, projectConfiguration, context, dockerRegistry(),
                                        dockerImageName(boxConfiguration));
        }
        catch (IOException e)
        {
            throw new BoxDatabaseException("I/O error creating database: " + e, e);
        }
    }
}
