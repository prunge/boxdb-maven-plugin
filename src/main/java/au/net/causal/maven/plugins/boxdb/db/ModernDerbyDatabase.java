package au.net.causal.maven.plugins.boxdb.db;

import com.google.common.collect.ImmutableList;

import java.util.List;

/**
 * Used for Derby versions &gt;= 10.15.  Modern versions of Derby require additional JARs on the classpath and have
 * different driver class names.
 */
public class ModernDerbyDatabase extends DerbyDatabase
{
    public ModernDerbyDatabase(BoxConfiguration boxConfiguration,
                               ProjectConfiguration projectConfiguration,
                               BoxContext context)
    {
        super(boxConfiguration, projectConfiguration, context);
    }

    @Override
    public JdbcDriverInfo jdbcDriverInfo()
    throws BoxDatabaseException
    {
        JdbcDriverInfo baseInfo = super.jdbcDriverInfo();

        ImmutableList.Builder<RunnerDependency> deps = ImmutableList.builder();
        deps.addAll(baseInfo.getDependencies());

        //Modern Derby needs Derby shared JAR explicitly added since it's not declared as a dependency
        deps.add(new RunnerDependency(DERBY_DATABASE_GROUP_ID, "derbyshared",
                                      getBoxConfiguration().getDatabaseVersion()));

        return new JdbcDriverInfo(deps.build(), "org.apache.derby.client.ClientAutoloadedDriver",
                                  baseInfo.getDownloadUrl());
    }

    @Override
    protected List<? extends RunnerDependency> getDerbyDatabaseDependencies()
    {
        ImmutableList.Builder<RunnerDependency> deps = ImmutableList.builder();

        deps.addAll(super.getDerbyDatabaseDependencies());

        //Modern Derby needs shared and tools JARs added explicitly since they are not declared as dependencies
        //Maybe a bug: https://issues.apache.org/jira/browse/DERBY-7046
        deps.add(new RunnerDependency(DERBY_DATABASE_GROUP_ID, "derbyshared",
                                      getBoxConfiguration().getDatabaseVersion()));
        deps.add(new RunnerDependency(DERBY_DATABASE_GROUP_ID, "derbytools",
                                      getBoxConfiguration().getDatabaseVersion()));

        return deps.build();
    }

    @Override
    protected DataSourceBuilder dataSourceBuilder(DatabaseTarget target, boolean create)
    throws BoxDatabaseException
    {
        //Everything the same except for the data source class name
        return super.dataSourceBuilder(target, create)
                    .dataSourceClassName("org.apache.derby.client.BasicClientDataSource");
    }
}
