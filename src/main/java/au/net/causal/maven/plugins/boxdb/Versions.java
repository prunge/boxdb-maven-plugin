package au.net.causal.maven.plugins.boxdb;

/**
 * Tools for parsing and splitting version numbers.
 */
public final class Versions 
{
    /**
     * Private constructor to prevent instantiation.
     */
    private Versions() 
    {
    }

    /**
     * Determine the major version number given a full version.  For example, for "5.6.7", "5" is returned.  Only
     * digits are considered, so for "12c" the result is "12".
     * 
     * @param fullVersion the full version to parse.
     *                    
     * @return the major version, or <code>null</code> if <code>fullVersion</code> is <code>null</code>.
     */
    public static String majorVersion(String fullVersion) 
    {
        if (fullVersion == null) 
            return null;
        
        String[] numericVersionSegments = fullVersion.split("\\D+");
        if (numericVersionSegments.length < 1) 
            return fullVersion;
        
        return numericVersionSegments[0];
    }
}
