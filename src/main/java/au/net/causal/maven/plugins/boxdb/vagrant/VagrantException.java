package au.net.causal.maven.plugins.boxdb.vagrant;

public class VagrantException extends Exception
{
    public VagrantException()
    {
    }

    public VagrantException(String message)
    {
        super(message);
    }

    public VagrantException(String message, Throwable cause)
    {
        super(message, cause);
    }

    public VagrantException(Throwable cause)
    {
        super(cause);
    }
}
