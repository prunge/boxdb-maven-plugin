package au.net.causal.maven.plugins.boxdb.db;

import com.google.common.collect.ImmutableMap;

import java.util.ArrayList;
import java.util.Collections;
import java.util.HashSet;
import java.util.List;
import java.util.Map;
import java.util.Objects;
import java.util.Set;
import java.util.function.Supplier;

public abstract class VersionSwitchingDatabaseFactory implements BoxDatabaseFactory
{
    private final String name;
    private final Map<String, Supplier<? extends BoxDatabaseFactory>> versionFactoryMap;
    private final Supplier<? extends BoxDatabaseFactory> defaultFactory;
    private final Supplier<? extends BoxDatabaseFactory> fallbackFactory;

    protected VersionSwitchingDatabaseFactory(String name,
                                              Supplier<? extends BoxDatabaseFactory> defaultFactory,
                                              Supplier<? extends BoxDatabaseFactory> fallbackFactory,
                                              Map<String, ? extends Supplier<? extends BoxDatabaseFactory>> versionFactoryMap)
    {
        Objects.requireNonNull(name, "name == null");
        Objects.requireNonNull(defaultFactory, "defaultFactory == null");
        Objects.requireNonNull(versionFactoryMap, "versionFactoryMap == null");
        this.name = name;
        this.defaultFactory = defaultFactory;
        this.fallbackFactory = fallbackFactory;
        this.versionFactoryMap = ImmutableMap.copyOf(versionFactoryMap);
    }

    protected VersionSwitchingDatabaseFactory(String name, String defaultVersion, Map<String, ? extends Supplier<? extends BoxDatabaseFactory>> versionFactoryMap)
    {
        Objects.requireNonNull(name, "name == null");
        Objects.requireNonNull(versionFactoryMap, "versionFactoryMap == null");
        this.name = name;
        this.versionFactoryMap = ImmutableMap.copyOf(versionFactoryMap);
        defaultFactory = versionFactoryMap.get(defaultVersion);
        if (defaultFactory == null)
            throw new IllegalArgumentException("Invalid default version '" + defaultVersion + "' - does not have entry in factory map.");

        fallbackFactory = null;
    }

    @Override
    public BoxDatabase create(BoxConfiguration boxConfiguration, ProjectConfiguration projectConfiguration, BoxContext context)
    throws BoxDatabaseException
    {
        String version = boxConfiguration.getDatabaseVersion();

        if (version == null)
            return defaultFactory.get().create(boxConfiguration, projectConfiguration, context);

        Supplier<? extends BoxDatabaseFactory> factory = versionFactoryMap.get(version);
        if (factory == null)
            factory = fallbackFactory;
        if (factory == null)
            throw new BoxDatabaseException("Invalid version '" + version + "' for database type '" + name() + "'.  Available versions: " + versionFactoryMap.keySet());

        return factory.get().create(boxConfiguration, projectConfiguration, context);
    }

    @Override
    public String name()
    {
        return name;
    }

    @Override
    public List<String> availableVersions(ProjectConfiguration projectConfiguration, BoxContext context)
    throws BoxDatabaseException
    {
        Set<String> mappedVersions = new HashSet<>(versionFactoryMap.keySet());

        if (fallbackFactory != null)
            mappedVersions.addAll(fallbackFactory.get().availableVersions(projectConfiguration, context));

        List<String> mappedVersionList = new ArrayList<>(mappedVersions);
        Collections.sort(mappedVersionList);

        return mappedVersionList;
    }
}
