File outFile = new File(basedir, 'build.log')
assert outFile.exists()
assert outFile.size() > 0

//Check that we have some known versions in the list
assert outFile.getText().contains("h2 versions: ")
assert outFile.getText().contains("2.2.224")
assert outFile.getText().contains("2.1.214")
assert outFile.getText().contains("1.4.197")
assert outFile.getText().contains("1.4.196")
assert outFile.getText().contains("1.4.195")
assert outFile.getText().contains("1.4.194")


//Check latest/oldest versions are in the list
assert outFile.getText().contains(boxdb_supportedDatabaseVersions_h2_latest)
assert outFile.getText().contains(boxdb_supportedDatabaseVersions_h2_oldest)
