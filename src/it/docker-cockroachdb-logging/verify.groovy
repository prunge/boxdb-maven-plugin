['dblogs-default/docker.log', 'dblogs-latest/docker.log', 'dblogs-oldest/docker.log'].each {

    File outFile = new File(basedir, it)
    assert outFile.exists()
    assert outFile.size() > 0

    //Check for known strings in CockroachDB startup log
    assert outFile.getText().contains("CockroachDB node starting at ")
}

return
