package au.net.causal.boxdbit.postgres;

import au.net.causal.boxdbit.base.BaseIntegrationTestCase;

import org.junit.jupiter.api.Test;

import java.sql.SQLException;
import java.util.List;

import static org.assertj.core.api.Assertions.*;

class PostgresIT extends BaseIntegrationTestCase
{
    @Test
    void testBadLoginCredentialsAdmin()
    throws SQLException
    {
        verifyBadLoginCredentialsAdmin(this::checkBadLogin);
    }

    @Test
    void testBadLoginCredentialsUser()
    throws SQLException
    {
        verifyBadLoginCredentialsUser(this::checkBadLogin);
    }

    @Test
    void testLoginAsAdmin()
    throws SQLException
    {
        verifyLoginAsAdmin();
    }

    @Test
    void testNormalData()
    throws SQLException
    {
        List<String> cats = readSelectCustomData();
        assertThat(cats.get(0)).isEqualTo("Costly");
        assertThat(cats.get(1)).isNotEqualTo("Dinah-Kah");
        assertThat(cats.get(2)).isEqualTo("Janvier");
    }

    private void checkBadLogin(SQLException ex)
    {
        //Postgres 8.x has a different state for bad logins
        if ("oldest".equals(dbTestName()))
            assertThat(ex.getSQLState()).isEqualTo("28000");
        else
            assertThat(ex.getSQLState()).isEqualTo("28P01");
    }
}
