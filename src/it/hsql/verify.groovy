['default', 'latest', 'oldest'].each {

    File outFile = new File(basedir, "target/db${it}/app.script")
    assert outFile.exists()
    assert outFile.size() > 0
}

return
