File outFile = new File(basedir, 'build.log')
assert outFile.exists()
assert outFile.size() > 0

//Check a couple of known strings in Oracle startup log
assert outFile.getText().contains("docker.log:")
assert outFile.getText().contains("Starting Oracle Database 11g Express Edition instance.")
