File outFile = new File(basedir, 'build.log')
assert outFile.exists()
assert outFile.size() > 0

//Check that we have some known versions in the list
assert outFile.getText().contains("derby versions: ")
assert outFile.getText().contains("10.12.1.1")
assert outFile.getText().contains("10.13.1.1")
assert outFile.getText().contains("10.14.1.0")
assert outFile.getText().contains("10.14.2.0")
assert outFile.getText().contains("10.15.2.0")
assert outFile.getText().contains("10.16.1.1")
assert outFile.getText().contains("10.17.1.0")

//Check latest/oldest versions are in the list
assert outFile.getText().contains(boxdb_supportedDatabaseVersions_derby_latest)
assert outFile.getText().contains(boxdb_supportedDatabaseVersions_derby_oldest)
