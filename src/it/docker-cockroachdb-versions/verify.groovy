File outFile = new File(basedir, 'build.log')
assert outFile.exists()
assert outFile.size() > 0

//Check that we have some known versions in the list
assert outFile.getText().contains("cockroachdb versions: ")
assert outFile.getText().contains("23.1.23")
assert outFile.getText().contains("22.1.6")
assert outFile.getText().contains("19.1.5")
assert outFile.getText().contains("2.1.9")
assert outFile.getText().contains("1.1.9")

//Check latest/oldest versions are in the list
assert outFile.getText().contains(boxdb_supportedDatabaseVersions_cockroachdb_latest)
assert outFile.getText().contains(boxdb_supportedDatabaseVersions_cockroachdb_oldest)
