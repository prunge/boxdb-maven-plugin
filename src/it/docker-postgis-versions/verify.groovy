File outFile = new File(basedir, 'build.log')
assert outFile.exists()
assert outFile.size() > 0

//Check that we have some known versions in the list
assert outFile.getText().contains("postgis versions: ")
assert outFile.getText().contains("16-3.4")
assert outFile.getText().contains("14-3.3")
assert outFile.getText().contains("11.0-2.5")
assert outFile.getText().contains("10.0-2.4")
assert outFile.getText().contains("9.6-2.4")

//Check latest/oldest versions are in the list
assert outFile.getText().contains(boxdb_supportedDatabaseVersions_postgis_latest)
assert outFile.getText().contains(boxdb_supportedDatabaseVersions_postgis_oldest)
