File outFile = new File(basedir, 'build.log')
assert outFile.exists()
assert outFile.size() > 0

//Check that we have some known versions in the list
assert outFile.getText().contains("mysql versions: ")
assert outFile.getText().contains("9.0.0")
assert outFile.getText().contains("8.4.1")
assert outFile.getText().contains("8.0.30")
assert outFile.getText().contains("8.0.4")
assert outFile.getText().contains("5.7.25")
assert outFile.getText().contains("5.6.43")
assert outFile.getText().contains("5.5.62")

//Check latest/oldest versions are in the list
assert outFile.getText().contains(boxdb_supportedDatabaseVersions_mysql_latest)
assert outFile.getText().contains(boxdb_supportedDatabaseVersions_mysql_oldest)
