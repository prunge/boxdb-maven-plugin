package au.net.causal.boxdbit.mysql;

import au.net.causal.boxdbit.base.BaseIntegrationTestCase;

import org.junit.jupiter.api.Test;

import java.sql.PreparedStatement;
import java.sql.ResultSet;
import java.sql.SQLException;

import static org.assertj.core.api.Assertions.*;

class MySqlIT extends BaseIntegrationTestCase
{
    @Test
    void testBadLoginCredentialsAdmin()
    throws SQLException
    {
        verifyBadLoginCredentialsAdmin(this::checkBadLogin);
    }

    @Test
    void testBadLoginCredentialsUser()
    throws SQLException
    {
        verifyBadLoginCredentialsUser(this::checkBadLogin);
    }

    @Test
    void testLoginAsAdmin()
    throws SQLException
    {
        verifyLoginAsAdmin();
    }

    @Test
    void testNormalData()
    throws SQLException
    {
        verifySelectData();
    }

    @Test
    void testServerTimezone()
    throws SQLException
    {
        setUpConnection();
        try (PreparedStatement stat = connection.prepareStatement("SELECT @@system_time_zone"); ResultSet rs = stat.executeQuery())
        {
            rs.next();
            String systemTimeZone = rs.getString(1);
            assertThat(systemTimeZone).isEqualTo("+07"); //Offset of Antarctica/Davis - see https://en.wikipedia.org/wiki/List_of_tz_database_time_zones
        }
    }

    private void checkBadLogin(SQLException ex)
    {
        assertThat(ex.getSQLState()).isEqualTo("28000");
        assertThat(ex.getErrorCode()).isEqualTo(1045);
    }
}
