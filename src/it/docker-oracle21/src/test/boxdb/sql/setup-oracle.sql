whenever sqlerror exit 1;
create table cat (name varchar2(32) not null primary key, owner varchar2(32) not null);
insert into cat (name, owner) values ('Dinah-Kah', 'John Galah');
insert into cat (name, owner) values ('Janvier', 'Melissa Crane');
exit;
