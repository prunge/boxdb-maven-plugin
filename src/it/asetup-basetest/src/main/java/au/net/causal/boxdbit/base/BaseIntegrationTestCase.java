package au.net.causal.boxdbit.base;

import java.sql.Connection;
import java.sql.DriverManager;
import java.sql.PreparedStatement;
import java.sql.ResultSet;
import java.sql.SQLException;
import java.util.ArrayList;
import java.util.Arrays;
import java.util.List;
import java.util.function.Consumer;

import org.junit.jupiter.api.AfterEach;

import static org.assertj.core.api.Assertions.*;

public abstract class BaseIntegrationTestCase
{
    protected Connection connection;

    protected String dbTestName()
    {
        return System.getProperty("dbtest.name", "");
    }

    protected void setUpConnection()
    throws SQLException
    {
        String uri = System.getProperty("jdbc.url");
        String user = System.getProperty("jdbc.user");
        String password = System.getProperty("jdbc.password");

        System.out.println("JDBC Info: " + uri + " " + user + " " + password);

        connection = DriverManager.getConnection(uri, user, password);
    }

    protected void setUpConnectionAdmin()
    throws SQLException
    {
        String uri = System.getProperty("admin.jdbc.url");
        String user = System.getProperty("admin.jdbc.user");
        String password = System.getProperty("admin.jdbc.password");

        System.out.println("Admin JDBC Info: " + uri + " " + user + " " + password);

        connection = DriverManager.getConnection(uri, user, password);
    }

    @AfterEach
    protected void tearDownDatabaseConnection()
    throws SQLException
    {
        if (connection != null)
            connection.close();
    }

    protected void verifyBadLoginCredentialsUser(Consumer<? super SQLException> checker)
    throws SQLException
    {
        String uri = System.getProperty("jdbc.url");
        String user = System.getProperty("jdbc.user");
        String password = "notthecorrectpassword";
        try
        {
            connection = DriverManager.getConnection(uri, user, password);
            fail("Should not be able to log in with bad credentials");
        }
        catch (SQLException e)
        {
            checker.accept(e);
        }
    }

    protected void verifyBadLoginCredentialsAdmin(Consumer<? super SQLException> checker)
    throws SQLException
    {
        String uri = System.getProperty("admin.jdbc.url");
        String user = System.getProperty("admin.jdbc.user");
        String password = "notthecorrectpassword";
        try
        {
            connection = DriverManager.getConnection(uri, user, password);
            fail("Should not be able to log in with bad credentials");
        }
        catch (SQLException e)
        {
            checker.accept(e);
        }
    }

    protected void verifyLoginAsAdmin()
    throws SQLException
    {
        setUpConnectionAdmin();
    }

    protected void verifySelectData()
    throws SQLException
    {
        verifySelectCustomData("Dinah-Kah", "Janvier");
    }

    protected void verifySelectRestoreData()
    throws SQLException
    {
        verifySelectCustomData("Costly", "Dinah-Kah", "Janvier");
    }

    protected void verifySelectCustomData(String... expectedValues)
    throws SQLException
    {
        setUpConnection();

        List<String> catNames = new ArrayList<>();
        try (PreparedStatement stat = connection.prepareStatement("select name from cat order by name"); ResultSet rs = stat.executeQuery())
        {
            while (rs.next())
            {
                catNames.add(rs.getString(1));
            }
        }

        System.out.println("Cats: " + catNames);

        assertThat(catNames).containsSequence(expectedValues);
    }

    protected List<String> readSelectCustomData()
    throws SQLException
    {
        setUpConnection();

        List<String> catNames = new ArrayList<>();
        try (PreparedStatement stat = connection.prepareStatement("select name from cat order by name"); ResultSet rs = stat.executeQuery())
        {
            while (rs.next())
            {
                catNames.add(rs.getString(1));
            }
        }

        return catNames;
    }

    protected void verifyNamesCanBeInserted(String... namesToAdd)
    throws SQLException
    {
        setUpConnection();
        try (PreparedStatement stat = connection.prepareStatement("insert into bird (name) values (?)"))
        {
            for (String nameToAdd : namesToAdd)
            {
                stat.setString(1, nameToAdd);
                stat.executeUpdate();
            }
        }
    }

    protected void verifyNamesCannotBeInserted(Consumer<? super SQLException> checker, String... namesToAdd)
    throws SQLException
    {
        try
        {
            verifyNamesCanBeInserted(namesToAdd);
            fail("Expected SQL error not thrown when inserting data " + Arrays.toString(namesToAdd));
        }
        catch (SQLException e)
        {
            //Expected
            checker.accept(e);
        }
    }
}
