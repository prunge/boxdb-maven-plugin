['dblogs-default/docker.log', 'dblogs-latest/docker.log'].each {

    File outFile = new File(basedir, it)
    assert outFile.exists()
    assert outFile.size() > 0

    //Check for known strings in MySQL startup log
    assert outFile.getText().contains("[Server] /usr/sbin/mysqld: ready for connections.")
}

['dblogs-oldest/docker.log'].each {

    File outFile = new File(basedir, it)
    assert outFile.exists()
    assert outFile.size() > 0

    //Check for known strings in MySQL startup log
    assert outFile.getText().contains("[Note] mysqld: ready for connections.")
}

return