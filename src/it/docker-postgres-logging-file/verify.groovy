['dblogs-default/docker.log', 'dblogs-latest/docker.log'].each {

    File logFile = new File(basedir, it)
    assert logFile.exists()
    assert logFile.size() > 0

    //Check a couple of known strings in Postgres startup log
    assert logFile.getText().contains("PostgreSQL init process complete; ready for start up.")
}

['dblogs-oldest/docker.log'].each {

    File logFile = new File(basedir, it)
    assert logFile.exists()
    assert logFile.size() > 0

    //Check a couple of known strings in Postgres startup log
    assert logFile.getText().contains("database system is ready to accept connections")
}

return
