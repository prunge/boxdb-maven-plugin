File outFile = new File(basedir, 'build.log')
assert outFile.exists()
assert outFile.size() > 0

//Check that we have some known versions in the list
assert outFile.getText().contains("sqlserver-linux versions: ")
assert outFile.getText().contains("2022-CU13-ubuntu-22.04")
assert outFile.getText().contains("2019-CU17-ubuntu-20.04")
assert outFile.getText().contains("2017-CU13")
assert outFile.getText().contains("2017-CU12")
assert outFile.getText().contains("2017-CU11")
assert outFile.getText().contains("2017-GA")

//Check latest/oldest versions are in the list
assert outFile.getText().contains(boxdb_supportedDatabaseVersions_sqlserver_linux_latest)
assert outFile.getText().contains(boxdb_supportedDatabaseVersions_sqlserver_linux_oldest)
