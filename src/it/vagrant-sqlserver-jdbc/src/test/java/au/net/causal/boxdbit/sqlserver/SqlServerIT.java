package au.net.causal.boxdbit.sqlserver;

import au.net.causal.boxdbit.base.BaseIntegrationTestCase;

import org.junit.jupiter.api.BeforeAll;
import org.junit.jupiter.api.Test;

import java.sql.SQLException;

import static org.assertj.core.api.Assertions.*;

class SqlServerIT extends BaseIntegrationTestCase
{
    @BeforeAll
    static void ensureDriverClassLoaded()
    throws Exception
    {
        //Ensure driver class is loaded and registered
        //Only an issue specificly for Vagrant because the tests don't fork the JVM
        //and there's driver registration from different classloaders in the same JVM
        Class.forName("com.microsoft.sqlserver.jdbc.SQLServerDriver");
    }

    @Test
    void testBadLoginCredentialsAdmin()
    throws SQLException
    {
        verifyBadLoginCredentialsAdmin(this::checkBadLogin);
    }

    @Test
    void testBadLoginCredentialsUser()
    throws SQLException
    {
        verifyBadLoginCredentialsUser(this::checkBadLogin);
    }

    @Test
    void testLoginAsAdmin()
    throws SQLException
    {
        verifyLoginAsAdmin();
    }

    @Test
    void testNormalData()
    throws SQLException
    {
        verifySelectData();
    }

    private void checkBadLogin(SQLException ex)
    {
        assertThat(ex.getSQLState()).isEqualTo("S0001");
        assertThat(ex.getErrorCode()).isEqualTo(18456);
    }
}
