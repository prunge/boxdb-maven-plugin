package au.net.causal.boxdbit.sqlite;

import au.net.causal.boxdbit.base.BaseIntegrationTestCase;

import org.junit.jupiter.api.Test;

import java.sql.SQLException;

import static org.assertj.core.api.Assertions.*;

class SqliteRestoreIT extends BaseIntegrationTestCase
{
    //No real login for SQLite

    @Test
    void testNormalData()
    throws SQLException
    {
        verifySelectCustomData("Costly", "Dinah-Kah", "Janvier", "Twisp");
    }
}
