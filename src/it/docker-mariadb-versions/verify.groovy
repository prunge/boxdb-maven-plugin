File outFile = new File(basedir, 'build.log')
assert outFile.exists()
assert outFile.size() > 0

//Check that we have some known versions in the list
assert outFile.getText().contains("mariadb versions: ")
assert outFile.getText().contains("11.4.2")
assert outFile.getText().contains("10.9.2")
assert outFile.getText().contains("10.4.1")
assert outFile.getText().contains("10.3.12")
assert outFile.getText().contains("10.2.21")
assert outFile.getText().contains("5.5.62")

//Check latest/oldest versions are in the list
assert outFile.getText().contains(boxdb_supportedDatabaseVersions_mariadb_latest)
assert outFile.getText().contains(boxdb_supportedDatabaseVersions_mariadb_oldest)
