['default', 'latest', 'oldest'].each {

    File outFile = new File(basedir, "target/db${it}/app/log/log.ctrl")
    assert outFile.exists()
    assert outFile.size() > 0
}

return
