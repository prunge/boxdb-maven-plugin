File outFile = new File(basedir, 'build.log')
assert outFile.exists()
assert outFile.size() > 0

//Check that we have some known versions in the list
assert outFile.getText().contains("sqlite versions: ")
assert outFile.getText().contains("3.21.0")
assert outFile.getText().contains("3.21.0.1")
assert outFile.getText().contains("3.23.1")
assert outFile.getText().contains("3.25.2")
assert outFile.getText().contains("3.39.2.1")
assert outFile.getText().contains("3.46.0.0")


//Check latest/oldest versions are in the list
assert outFile.getText().contains(boxdb_supportedDatabaseVersions_sqlite_latest)
assert outFile.getText().contains(boxdb_supportedDatabaseVersions_sqlite_oldest)
