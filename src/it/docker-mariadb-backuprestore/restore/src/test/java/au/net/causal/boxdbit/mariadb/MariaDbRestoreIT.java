package au.net.causal.boxdbit.mariadb;

import au.net.causal.boxdbit.base.BaseIntegrationTestCase;

import org.junit.jupiter.api.Test;

import java.sql.SQLException;
import java.sql.SQLInvalidAuthorizationSpecException;

import static org.assertj.core.api.Assertions.*;

class MariaDbRestoreIT extends BaseIntegrationTestCase
{
    @Test
    void testBadLoginCredentialsAdmin()
    throws SQLException
    {
        verifyBadLoginCredentialsAdmin(this::checkBadLogin);
    }

    @Test
    void testBadLoginCredentialsUser()
    throws SQLException
    {
        verifyBadLoginCredentialsUser(this::checkBadLogin);
    }

    @Test
    void testLoginAsAdmin()
    throws SQLException
    {
        verifyLoginAsAdmin();
    }

    @Test
    void testNormalData()
    throws SQLException
    {
        verifySelectRestoreData();
    }

    private void checkBadLogin(SQLException ex)
    {
        assertThat((Exception)ex).isInstanceOf(SQLInvalidAuthorizationSpecException.class);
        assertThat(ex.getSQLState()).isEqualTo("28000");
        assertThat(ex.getErrorCode()).isEqualTo(1045);
    }
}
