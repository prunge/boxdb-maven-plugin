['dblogs-default/docker.log', 'dblogs-latest/docker.log', 'dblogs-oldest/docker.log'].each {

    File outFile = new File(basedir, it)
    assert outFile.exists()
    assert outFile.size() > 0

    //Check for known strings in SQL Server startup log
    assert outFile.getText().contains("SQL Server is now ready for client connections.")
}

return
