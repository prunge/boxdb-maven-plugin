['dblogs-default/app.app.log', 'dblogs-latest/app.app.log', 'dblogs-oldest/app.app.log'].each {

    File outFile = new File(basedir, it)
    assert outFile.exists()
    assert outFile.size() > 0

    //Check for known string in startup log
    assert outFile.getText().contains("NORMAL Database closed")
}

return
