set database event log level 3;
set database event log sql level 3;
create table cat (name varchar(32) not null primary key, owner varchar(32) not null);
insert into cat (name, owner) values ('Dinah-Kah', 'John Galah');
insert into cat (name, owner) values ('Janvier', 'Melissa Crane');
select * from cat;