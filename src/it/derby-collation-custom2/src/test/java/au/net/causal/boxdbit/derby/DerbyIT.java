package au.net.causal.boxdbit.derby;

import au.net.causal.boxdbit.base.BaseIntegrationTestCase;

import org.junit.jupiter.api.Test;

import java.sql.SQLException;
import java.sql.SQLIntegrityConstraintViolationException;
import java.sql.SQLNonTransientConnectionException;

import static org.assertj.core.api.Assertions.*;

class DerbyIT extends BaseIntegrationTestCase
{
    @Test
    void testBadLoginCredentialsAdmin()
    throws SQLException
    {
        verifyBadLoginCredentialsAdmin(this::checkBadLogin);
    }

    @Test
    void testBadLoginCredentialsUser()
    throws SQLException
    {
        verifyBadLoginCredentialsUser(this::checkBadLogin);
    }

    @Test
    void testLoginAsAdmin()
    throws SQLException
    {
        verifyLoginAsAdmin();
    }

    @Test
    void testNormalData()
    throws SQLException
    {
        //Case insensitive comparison
        verifySelectCustomData("Costly", "dinah-kah", "Janvier");
    }

    @Test
    void testCollationStrengthAgainstPrimaryKey()
    throws SQLException
    {
        verifyNamesCannotBeInserted(this::checkKeyViolation, "Annabelle", "Ånnabelle");
    }

    private void checkBadLogin(SQLException ex)
    {
        assertThat((Exception)ex).isInstanceOf(SQLNonTransientConnectionException.class);
        assertThat(ex.getSQLState()).isEqualTo("08004");
    }

    private void checkKeyViolation(SQLException ex)
    {
        assertThat((Exception)ex).isInstanceOf(SQLIntegrityConstraintViolationException.class);
        assertThat(ex.getSQLState()).isEqualTo("23505");
    }
}
