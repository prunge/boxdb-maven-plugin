['default', 'latest', 'oldest'].each {

    File dumpFile = new File(basedir, "backup/target/backup${it}.dmp")
    assert dumpFile.exists()
    assert dumpFile.size() > 0
}

return
