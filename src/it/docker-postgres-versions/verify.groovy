File outFile = new File(basedir, 'build.log')
assert outFile.exists()
assert outFile.size() > 0

//Check that we have some known versions in the list
assert outFile.getText().contains("postgres versions: ")
assert outFile.getText().contains("16.3")
assert outFile.getText().contains("14.5")
assert outFile.getText().contains("11.1")
assert outFile.getText().contains("10.6")
assert outFile.getText().contains("9.6.9")
assert outFile.getText().contains("8.4.22")

//Check latest/oldest versions are in the list
assert outFile.getText().contains(boxdb_supportedDatabaseVersions_postgres_latest)
assert outFile.getText().contains(boxdb_supportedDatabaseVersions_postgres_oldest)
