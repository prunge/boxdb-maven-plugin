File outFile = new File(basedir, 'build.log')
assert outFile.exists()
assert outFile.size() > 0

//Check a couple of known strings in Postgres startup log
assert outFile.getText().contains("docker.log:")
assert outFile.getText().contains("database system is ready to accept connections")
assert outFile.getText().contains("Enabling postgis in the database gis") || outFile.getText().contains("Enabling \"postgis\" in the database gis")
