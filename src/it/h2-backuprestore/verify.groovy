['default', 'latest', 'oldest'].each {

    File binaryDumpFile = new File(basedir, "backup/target/backup${it}-binary.dmp")
    assert binaryDumpFile.exists()
    assert binaryDumpFile.size() > 0
    File textDumpFile = new File(basedir, "backup/target/backup${it}-text.dmp")
    assert textDumpFile.exists()
    assert textDumpFile.size() > 0
}

return
