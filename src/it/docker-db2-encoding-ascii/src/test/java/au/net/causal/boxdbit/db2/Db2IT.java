package au.net.causal.boxdbit.db2;

import au.net.causal.boxdbit.base.BaseIntegrationTestCase;

import org.junit.jupiter.api.Test;

import java.sql.PreparedStatement;
import java.sql.ResultSet;
import java.sql.SQLException;
import java.sql.SQLIntegrityConstraintViolationException;
import java.sql.SQLInvalidAuthorizationSpecException;

import static org.assertj.core.api.Assertions.*;

class Db2IT extends BaseIntegrationTestCase
{
    @Test
    void testBadLoginCredentialsAdmin()
    throws SQLException
    {
        verifyBadLoginCredentialsAdmin(this::checkBadLogin);
    }

    @Test
    void testBadLoginCredentialsUser()
    throws SQLException
    {
        verifyBadLoginCredentialsUser(this::checkBadLogin);
    }

    @Test
    void testLoginAsAdmin()
    throws SQLException
    {
        verifyLoginAsAdmin();
    }

    @Test
    void testNormalData()
    throws SQLException
    {
        //Case insensitive comparison
        verifySelectCustomData("Costly", "Dinah-Kah", "Janvier");
    }

    @Test
    void testInsertUnsupportedCharacters()
    throws SQLException
    {
        setUpConnection();

        try (PreparedStatement stat = connection.prepareStatement("insert into cat (name, owner) values (?, ?)"))
        {
            stat.setString(1, "Dinah-Kah ⌁");
            stat.setString(2, "John Galah ⌁");
            stat.executeUpdate();

            //If symbols cannot be stored properly we will get primary key violation, which we expect for ASCII encoding
            stat.setString(1, "Dinah-Kah ♡");
            stat.setString(2, "John Galah ♡");
            stat.executeUpdate();

            fail("Should have thrown exception because of unsupported encoding in data");
        }
        catch (SQLIntegrityConstraintViolationException ex)
        {
            //Expected
            assertThat(ex.getSQLState()).isEqualTo("23505");
            assertThat(ex.getErrorCode()).isEqualTo(-803);
        }
    }

    private void checkBadLogin(SQLException ex)
    {
        assertThat((Exception)ex).isInstanceOf(SQLInvalidAuthorizationSpecException.class);
        assertThat(ex.getSQLState()).isEqualTo("28000");
        assertThat(ex.getErrorCode()).isEqualTo(-4214);
    }
}
