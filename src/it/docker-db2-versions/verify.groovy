File outFile = new File(basedir, 'build.log')
assert outFile.exists()
assert outFile.size() > 0

//Check that we have some known versions in the list
assert outFile.getText().contains("db2 versions: ")
assert outFile.getText().contains("11.5.0.0a")
assert outFile.getText().contains("11.5.7.0a")
assert outFile.getText().contains("11.5.8.0")
assert outFile.getText().contains("11.5.9.0")

//Check latest/oldest versions are in the list
assert outFile.getText().contains(boxdb_supportedDatabaseVersions_db2_latest)
assert outFile.getText().contains(boxdb_supportedDatabaseVersions_db2_oldest)
