File outFile = new File(basedir, 'build.log')
assert outFile.exists()
assert outFile.size() > 0

//Check a couple of known strings in SQL Server startup log
assert outFile.getText().contains("ERRORLOG:")
assert outFile.getText().contains("SQL Server is now ready for client connections.")
