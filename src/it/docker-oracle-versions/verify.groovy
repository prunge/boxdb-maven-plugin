File outFile = new File(basedir, 'build.log')
assert outFile.exists()
assert outFile.size() > 0

//Check that we have some known versions in the list
assert outFile.getText().contains("oracle versions: ")
assert outFile.getText().contains("11g")
assert outFile.getText().contains("12c")

//And the more modern versions
assert outFile.getText().contains("18.4.0-full-faststart")
assert outFile.getText().contains("21.3.0-full-faststart")
assert outFile.getText().contains("23.4-full-faststart")
