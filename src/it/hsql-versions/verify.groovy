File outFile = new File(basedir, 'build.log')
assert outFile.exists()
assert outFile.size() > 0

//Check that we have some known versions in the list
assert outFile.getText().contains("hsql versions: ")
assert outFile.getText().contains("2.3.5")
assert outFile.getText().contains("2.3.6")
assert outFile.getText().contains("2.4.0")
assert outFile.getText().contains("2.4.1")
assert outFile.getText().contains("2.7.0")
assert outFile.getText().contains("2.7.3")


//Check latest/oldest versions are in the list
assert outFile.getText().contains(boxdb_supportedDatabaseVersions_hsql_latest)
assert outFile.getText().contains(boxdb_supportedDatabaseVersions_hsql_oldest)
