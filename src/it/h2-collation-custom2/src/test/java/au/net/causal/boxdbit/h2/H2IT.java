package au.net.causal.boxdbit.h2;

import au.net.causal.boxdbit.base.BaseIntegrationTestCase;

import org.junit.jupiter.api.Test;

import java.sql.SQLException;
import java.sql.SQLIntegrityConstraintViolationException;

import static org.assertj.core.api.Assertions.*;

class H2IT extends BaseIntegrationTestCase
{
    @Test
    void testBadLoginCredentialsAdmin()
    throws SQLException
    {
        verifyBadLoginCredentialsAdmin(this::checkBadLogin);
    }

    @Test
    void testBadLoginCredentialsUser()
    throws SQLException
    {
        verifyBadLoginCredentialsUser(this::checkBadLogin);
    }

    @Test
    void testLoginAsAdmin()
    throws SQLException
    {
        verifyLoginAsAdmin();
    }

    @Test
    void testNormalData()
    throws SQLException
    {
        //Case insensitive comparison
        verifySelectCustomData("Costly", "dinah-kah", "Janvier");
    }

    @Test
    void testCollationStrengthAgainstPrimaryKey()
    throws SQLException
    {
        verifyNamesCannotBeInserted(this::checkKeyViolation, "Annabelle", "Ånnabelle");
    }

    private void checkBadLogin(SQLException ex)
    {
        assertThat(ex.getSQLState()).isEqualTo("28000");
        assertThat(ex.getErrorCode()).isEqualTo(28000);
    }

    private void checkKeyViolation(SQLException ex)
    {
        assertThat(ex.getSQLState()).isEqualTo("23505");
        assertThat(ex.getErrorCode()).isEqualTo(23505);
    }
}
