package au.net.causal.boxdbit.mysql;

import au.net.causal.boxdbit.base.BaseIntegrationTestCase;

import org.junit.jupiter.api.Test;

import java.sql.PreparedStatement;
import java.sql.SQLException;

import static org.assertj.core.api.Assertions.*;

class MySqlIT extends BaseIntegrationTestCase
{
    @Test
    void testBadLoginCredentialsAdmin()
    throws SQLException
    {
        verifyBadLoginCredentialsAdmin(this::checkBadLogin);
    }

    @Test
    void testBadLoginCredentialsUser()
    throws SQLException
    {
        verifyBadLoginCredentialsUser(this::checkBadLogin);
    }

    @Test
    void testLoginAsAdmin()
    throws SQLException
    {
        verifyLoginAsAdmin();
    }

    @Test
    void testNormalData()
    throws SQLException
    {
        verifySelectCustomData("Costly", "Dinah-Kah", "Janvier");
    }

    @Test
    void testInsertUnsupportedCharacters()
    throws SQLException
    {
        setUpConnection();

        try (PreparedStatement stat = connection.prepareStatement("insert into cat (name, owner) values (?, ?)"))
        {
            stat.setString(1, "Dinah-Kah ⌁");
            stat.setString(2, "John Galah ⌁");
            stat.executeUpdate();
            fail("Should have thrown exception because of unsupported encoding in data");
        }
        catch (SQLException ex)
        {
            //Expected
            assertThat(ex.getSQLState()).isEqualTo("HY000");
            assertThat(ex.getErrorCode()).isEqualTo(1366); //Incorrect string value
        }
    }

    private void checkBadLogin(SQLException ex)
    {
        assertThat(ex.getSQLState()).isEqualTo("28000");
        assertThat(ex.getErrorCode()).isEqualTo(1045);
    }
}
