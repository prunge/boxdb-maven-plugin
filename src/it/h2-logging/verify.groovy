['dblogs-default/app.trace.db', 'dblogs-latest/app.trace.db', 'dblogs-oldest/app.trace.db'].each {

    File outFile = new File(basedir, it)
    assert outFile.exists()
    assert outFile.size() > 0

    //Check for known string in startup log
    assert outFile.getText().contains("exclusive write lock requesting for SYS") || outFile.getText().contains("exclusive requesting for SYS")
}

return
