File outFile = new File(basedir, 'build.log')
assert outFile.exists()
assert outFile.size() > 0

//Check a couple of known strings in Postgres startup log
assert outFile.getText().contains("docker.log:")
assert outFile.getText().contains("PostgreSQL init process complete; ready for start up.")
