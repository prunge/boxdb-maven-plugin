File outFile = new File(basedir, 'build.log')
assert outFile.exists()
assert outFile.size() > 0

//Check a couple of known strings in DB2 startup log
assert outFile.getText().contains("docker.log:")
assert outFile.getText().contains("SQL1063N  DB2START processing was successful.")
