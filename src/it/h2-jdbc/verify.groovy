['default', 'latest', 'oldest'].each {

    File outFile = new File(basedir, "target/db${it}/app.mv.db")
    assert outFile.exists()
    assert outFile.size() > 0
}

return
