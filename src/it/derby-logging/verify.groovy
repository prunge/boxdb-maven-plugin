['dblogs-default/derby.log', 'dblogs-latest/derby.log', 'dblogs-oldest/derby.log'].each {

    File outFile = new File(basedir, it)
    assert outFile.exists()
    assert outFile.size() > 0

    //Check for known strings in Derby startup log
    assert outFile.getText().contains("Booting Derby version The Apache Software Foundation - Apache Derby -")
}

return
