package au.net.causal.boxdbit.postgres;

import au.net.causal.boxdbit.base.BaseIntegrationTestCase;

import org.junit.jupiter.api.Test;

import java.sql.SQLException;

import static org.assertj.core.api.Assertions.*;

class PostgresIT extends BaseIntegrationTestCase
{
    @Test
    void testBadLoginCredentialsAdmin()
    throws SQLException
    {
        verifyBadLoginCredentialsAdmin(this::checkBadLogin);
    }

    @Test
    void testBadLoginCredentialsUser()
    throws SQLException
    {
        verifyBadLoginCredentialsUser(this::checkBadLogin);
    }

    @Test
    void testLoginAsAdmin()
    throws SQLException
    {
        verifyLoginAsAdmin();
    }

    @Test
    void testNormalData()
    throws SQLException
    {
        //Case insensitive comparison
        verifySelectCustomData("Costly", "dinah-kah", "Janvier");
    }
    
    private void checkBadLogin(SQLException ex)
    {
        //Postgres 8.x has a different state for bad logins
        if ("oldest".equals(dbTestName()))
            assertThat(ex.getSQLState()).isEqualTo("28000");
        else
            assertThat(ex.getSQLState()).isEqualTo("28P01");
    }
}
