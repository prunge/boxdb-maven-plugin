package au.net.causal.boxdb.custombox;

import au.net.causal.maven.plugins.boxdb.db.BoxConfiguration;
import au.net.causal.maven.plugins.boxdb.db.BoxContext;
import au.net.causal.maven.plugins.boxdb.db.BoxDatabaseException;
import au.net.causal.maven.plugins.boxdb.db.PostgresFactory;
import au.net.causal.maven.plugins.boxdb.db.ProjectConfiguration;

public class MyCustomFactory extends PostgresFactory
{
    @Override
    protected MyCustomDatabase createDockerDatabase(BoxConfiguration boxConfiguration, ProjectConfiguration projectConfiguration, BoxContext context)
    throws BoxDatabaseException
    {
        return new MyCustomDatabase(boxConfiguration, projectConfiguration, context, dockerRegistry());
    }

    @Override
    protected void initializeDefaults(BoxConfiguration boxConfiguration)
    {
        super.initializeDefaults(boxConfiguration);

        //Override and hardcode the database name - for testing
        boxConfiguration.setDatabaseName("mydatabase");
    }

    @Override
    public String name()
    {
        return "mycustom";
    }
}
