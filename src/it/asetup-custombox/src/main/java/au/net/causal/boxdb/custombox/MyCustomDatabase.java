package au.net.causal.boxdb.custombox;

import au.net.causal.maven.plugins.boxdb.db.BoxConfiguration;
import au.net.causal.maven.plugins.boxdb.db.BoxContext;
import au.net.causal.maven.plugins.boxdb.db.DockerRegistry;
import au.net.causal.maven.plugins.boxdb.db.PostgresDatabase;
import au.net.causal.maven.plugins.boxdb.db.ProjectConfiguration;

public class MyCustomDatabase extends PostgresDatabase
{
    public MyCustomDatabase(BoxConfiguration boxConfiguration, ProjectConfiguration projectConfiguration,
                            BoxContext context, DockerRegistry dockerRegistry)
    {
        super(boxConfiguration, projectConfiguration, context, dockerRegistry);
    }

    @Override
    protected String dockerImageName()
    {
        return "postgres:" + getBoxConfiguration().getDatabaseVersion();
    }
}
