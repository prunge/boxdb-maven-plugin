package au.net.causal.maven.plugins.boxdb.db;

import au.net.causal.maven.plugins.boxdb.db.VersionRangeSwitchingDatabaseFactory.Specifier;
import org.junit.jupiter.api.Test;

import java.util.Arrays;
import java.util.List;

import static org.assertj.core.api.Assertions.*;

class TestVersionRangeSwitchingDatabaseFactory
{
    @Test
    void test()
    {
        FakeFactory one = new FakeFactory("one");
        FakeFactory two = new FakeFactory("two");

        VersionRangeSwitchingDatabaseFactory factory = new VersionRangeSwitchingDatabaseFactory("galah", "1.0", Arrays.asList(
            new Specifier("[1.0, 2.0)", () -> one),
            new Specifier("[2.0,)", () -> two)
        )){};

        assertThat(factory.factoryForVersion("1.0").get()).isEqualTo(one);
        assertThat(factory.factoryForVersion("1.1").get()).isEqualTo(one);
        assertThat(factory.factoryForVersion("1.2-abc").get()).isEqualTo(one);
        assertThat(factory.factoryForVersion("2.0").get()).isEqualTo(two);
        assertThat(factory.factoryForVersion("2.1.0").get()).isEqualTo(two);
        assertThat(factory.factoryForVersion("3.4-abc").get()).isEqualTo(two);

        //Default
        assertThat(factory.factoryForVersion(null).get()).isEqualTo(one);
    }

    @Test
    void testFallback()
    {
        FakeFactory one = new FakeFactory("one");
        FakeFactory two = new FakeFactory("two");
        FakeFactory fallback = new FakeFactory("fallback");

        VersionRangeSwitchingDatabaseFactory factory = new VersionRangeSwitchingDatabaseFactory("galah", () -> one, () -> fallback, Arrays.asList(
                new Specifier("[1.0, 2.0)", () -> one),
                new Specifier("[2.0, 3.0)", () -> two)
        )){};

        assertThat(factory.factoryForVersion("1.0").get()).isEqualTo(one);
        assertThat(factory.factoryForVersion("1.1").get()).isEqualTo(one);
        assertThat(factory.factoryForVersion("1.2-abc").get()).isEqualTo(one);
        assertThat(factory.factoryForVersion("2.0").get()).isEqualTo(two);
        assertThat(factory.factoryForVersion("2.1.0").get()).isEqualTo(two);

        //Default
        assertThat(factory.factoryForVersion(null).get()).isEqualTo(one);

        //Fallback
        assertThat(factory.factoryForVersion("0.0").get()).isEqualTo(fallback);
        assertThat(factory.factoryForVersion("3.0").get()).isEqualTo(fallback);
    }

    @Test
    void testOracleLikeVersionNumbers()
    {
        FakeFactory eleven = new FakeFactory("eleven");
        FakeFactory twelve = new FakeFactory("twelve");
        FakeFactory fallback = new FakeFactory("fallback");

        VersionRangeSwitchingDatabaseFactory factory = new VersionRangeSwitchingDatabaseFactory("galah", () -> fallback, () -> fallback, Arrays.asList(
                new Specifier("[11, 12)", () -> eleven),
                new Specifier("[12, 13)", () -> twelve)
        )){};

        assertThat(factory.factoryForVersion("11").get()).isEqualTo(eleven);
        assertThat(factory.factoryForVersion("11g").get()).isEqualTo(eleven);
        assertThat(factory.factoryForVersion("12").get()).isEqualTo(twelve);
        assertThat(factory.factoryForVersion("12c").get()).isEqualTo(twelve);

        //Default
        assertThat(factory.factoryForVersion(null).get()).isEqualTo(fallback);

        //Fallback
        assertThat(factory.factoryForVersion("0.0").get()).isEqualTo(fallback);
        assertThat(factory.factoryForVersion("24").get()).isEqualTo(fallback);
    }

    private static class FakeFactory implements BoxDatabaseFactory
    {
        private final String name;

        public FakeFactory(String name)
        {
            this.name = name;
        }

        @Override
        public String name()
        {
            return name;
        }

        @Override
        public BoxDatabase create(BoxConfiguration boxConfiguration, ProjectConfiguration projectConfiguration, BoxContext context) throws BoxDatabaseException
        {
            throw new UnsupportedOperationException();
        }

        @Override
        public List<String> availableVersions(ProjectConfiguration projectConfiguration, BoxContext context) throws BoxDatabaseException
        {
            throw new UnsupportedOperationException();
        }

        @Override
        public String toString()
        {
            return FakeFactory.class.getSimpleName() + ":" + name;
        }
    }
}
