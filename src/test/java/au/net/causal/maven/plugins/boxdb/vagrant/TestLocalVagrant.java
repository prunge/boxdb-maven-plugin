package au.net.causal.maven.plugins.boxdb.vagrant;

import au.net.causal.maven.plugins.boxdb.vagrant.BoxUpdateStatus.State;
import org.apache.maven.plugin.logging.SystemStreamLog;
import org.codehaus.plexus.util.cli.Commandline;
import org.junit.jupiter.api.Test;

import java.util.List;

import static org.assertj.core.api.Assertions.*;

class TestLocalVagrant
{
    private final LocalVagrant vagrant = new LocalVagrant(new Commandline("ignore"), new SystemStreamLog());

    @Test
    void testParseBoxListLine()
    throws VagrantException
    {
        BoxDefinition result = vagrant.parseBoxListLine("debian/jessie64                 (virtualbox, 8.3.0)");
        assertThat(result.getName()).isEqualTo("debian/jessie64");
        assertThat(result.getProvider()).isEqualTo("virtualbox");
        assertThat(result.getVersion()).isEqualTo("8.3.0");
    }

    @Test
    void testParseBoxListLine2()
    throws VagrantException
    {
        BoxDefinition result = vagrant.parseBoxListLine("msabramo/mssqlserver2014express (virtualbox, 0.1)");
        assertThat(result.getName()).isEqualTo("msabramo/mssqlserver2014express");
        assertThat(result.getProvider()).isEqualTo("virtualbox");
        assertThat(result.getVersion()).isEqualTo("0.1");
    }

    @Test
    void testParseBoxListOutput()
    throws VagrantException
    {
        String s = "debian/jessie64                 (virtualbox, 8.3.0)\n" +
                   "msabramo/mssqlserver2014express (virtualbox, 0.1)\n";
        List<? extends BoxDefinition> results = vagrant.parseBoxListOutput(s);

        assertThat(results).hasSize(2);
        assertThat(results.get(0).getName()).isEqualTo("debian/jessie64");
        assertThat(results.get(1).getName()).isEqualTo("msabramo/mssqlserver2014express");
        assertThat(results.get(0).getVersion()).isEqualTo("8.3.0");
        assertThat(results.get(1).getVersion()).isEqualTo("0.1");
        assertThat(results.get(0).getProvider()).isEqualTo("virtualbox");
        assertThat(results.get(1).getProvider()).isEqualTo("virtualbox");
    }

    @Test
    void testParseStatusLineNotCreated()
    throws VagrantException
    {
        String s = "box           not created (virtualbox)";
        Vagrant.BoxStatus result = vagrant.parseStatusLine(s);
        assertThat(result).isEqualTo(Vagrant.BoxStatus.NOT_CREATED);
    }

    @Test
    void testParseStatusLineStopped()
    throws VagrantException
    {
        String s = "box           poweroff (virtualbox)";
        Vagrant.BoxStatus result = vagrant.parseStatusLine(s);
        assertThat(result).isEqualTo(Vagrant.BoxStatus.STOPPED);
    }

    @Test
    void testParseStatusLineRunning()
    throws VagrantException
    {
        String s = "box           running (virtualbox)";
        Vagrant.BoxStatus result = vagrant.parseStatusLine(s);
        assertThat(result).isEqualTo(Vagrant.BoxStatus.RUNNING);
    }

    @Test
    void testParseStatusOutput()
    throws VagrantException
    {
        String s = "Current machine states:\n" +
                    "\n" +
                    "boxdb-sqlserver           poweroff (virtualbox)\n" +
                    "\n" +
                    "The VM is powered off. To restart the VM, simply run `vagrant up`";

        Vagrant.BoxStatus result = vagrant.parseStatusOutput(s, "boxdb-sqlserver");
        assertThat(result).isEqualTo(Vagrant.BoxStatus.STOPPED);
    }

    @Test
    void testParsePluginListLine()
    throws VagrantException
    {
        String s = "vagrant-vbguest (0.12.0)";
        Vagrant.Plugin plugin = vagrant.parsePluginListLine(s);

        assertThat(plugin.getName()).isEqualTo("vagrant-vbguest");
        assertThat(plugin.getVersion()).isEqualTo("0.12.0");
    }

    @Test
    void testParsePluginListOutput()
    throws VagrantException
    {
        String s = "vagrant-exec (0.5.2)\n" +
                    "vagrant-share (1.1.5, system)\n" +
                    "vagrant-vbguest (0.12.0)";
        List<? extends Vagrant.Plugin> plugins = vagrant.parsePluginListOutput(s);

        assertThat(plugins).hasSize(3);
        assertThat(plugins.get(0).getName()).isEqualTo("vagrant-exec");
        assertThat(plugins.get(0).getVersion()).isEqualTo("0.5.2");
        assertThat(plugins.get(1).getName()).isEqualTo("vagrant-share");
        assertThat(plugins.get(1).getVersion()).isEqualTo("1.1.5, system");
        assertThat(plugins.get(2).getName()).isEqualTo("vagrant-vbguest");
        assertThat(plugins.get(2).getVersion()).isEqualTo("0.12.0");
    }

    @Test
    void testParseBoxOutdatedLineUpToDate()
    throws VagrantException
    {
        BoxUpdateStatus result = vagrant.parseBoxOutdatedLine("* 'ubuntu/zesty64' for 'virtualbox' (v20171219.0.0) is up to date");
        assertThat(result.getName()).isEqualTo("ubuntu/zesty64");
        assertThat(result.getProvider()).isEqualTo("virtualbox");
        assertThat(result.getLocalVersion()).isEqualTo("v20171219.0.0");
        assertThat(result.getRemoteVersion()).isEqualTo("v20171219.0.0");
        assertThat(result.getState()).isEqualTo(State.UP_TO_DATE);
    }

    @Test
    void testParseBoxOutdatedLineOutdated()
    throws VagrantException
    {
        BoxUpdateStatus result = vagrant.parseBoxOutdatedLine("* 'ubuntu/bionic64' for 'virtualbox' is outdated! Current: 20180809.0.0. Latest: 20190210.0.0");
        assertThat(result.getName()).isEqualTo("ubuntu/bionic64");
        assertThat(result.getProvider()).isEqualTo("virtualbox");
        assertThat(result.getLocalVersion()).isEqualTo("20180809.0.0");
        assertThat(result.getRemoteVersion()).isEqualTo("20190210.0.0");
        assertThat(result.getState()).isEqualTo(State.OUTDATED);
    }

    @Test
    void testParseBoxOutdatedLineNoVersionInfo()
    throws VagrantException
    {
        BoxUpdateStatus result = vagrant.parseBoxOutdatedLine("* 'test/mybox' for 'virtualbox' wasn't added from a catalog, no version information");
        assertThat(result.getName()).isEqualTo("test/mybox");
        assertThat(result.getProvider()).isEqualTo("virtualbox");
        assertThat(result.getLocalVersion()).isNull();
        assertThat(result.getRemoteVersion()).isNull();
        assertThat(result.getState()).isEqualTo(State.NO_VERSION_INFORMATION);
    }

    @Test
    void testParseBoxOutdatedLineError()
    throws VagrantException
    {
        BoxUpdateStatus result = vagrant.parseBoxOutdatedLine("* 'ubuntu/bionic64' for 'virtualbox': Error loading metadata: Could not resolve host: vagrantcloud.com");
        assertThat(result.getName()).isEqualTo("ubuntu/bionic64");
        assertThat(result.getProvider()).isEqualTo("virtualbox");
        assertThat(result.getLocalVersion()).isNull();
        assertThat(result.getRemoteVersion()).isNull();
        assertThat(result.getState()).isEqualTo(State.ERROR);
        assertThat(result.getDetail()).contains("Error loading metadata: Could not resolve host: vagrantcloud.com");
    }

    @Test
    void testParseBoxOutdatedOutput()
    throws VagrantException
    {
        String s = "* 'ubuntu/zesty64' for 'virtualbox' (v20171219.0.0) is up to date\n" +
                "* 'ubuntu/bionic64' for 'virtualbox' is outdated! Current: 20180809.0.0. Latest: 20190210.0.0\n" +
                "* 'ubuntu/artful64' for 'virtualbox' is outdated! Current: 20180126.0.0. Latest: 20180718.0.0\n" +
                "* 'msabramo/mssqlserver2014express' for 'virtualbox' (v0.1) is up to date\n" +
                "* 'test/mybox' for 'virtualbox' wasn't added from a catalog, no version information";
        List<? extends BoxUpdateStatus> updates = vagrant.parseBoxOutdatedOutput(s);

        assertThat(updates).hasSize(5);

        assertThat(updates.get(0).getName()).isEqualTo("ubuntu/zesty64");
        assertThat(updates.get(0).getProvider()).isEqualTo("virtualbox");
        assertThat(updates.get(0).getLocalVersion()).isEqualTo("v20171219.0.0");
        assertThat(updates.get(0).getRemoteVersion()).isEqualTo("v20171219.0.0");
        assertThat(updates.get(0).getState()).isEqualTo(State.UP_TO_DATE);

        assertThat(updates.get(1).getName()).isEqualTo("ubuntu/bionic64");
        assertThat(updates.get(1).getProvider()).isEqualTo("virtualbox");
        assertThat(updates.get(1).getLocalVersion()).isEqualTo("20180809.0.0");
        assertThat(updates.get(1).getRemoteVersion()).isEqualTo("20190210.0.0");
        assertThat(updates.get(1).getState()).isEqualTo(State.OUTDATED);

        assertThat(updates.get(2).getName()).isEqualTo("ubuntu/artful64");
        assertThat(updates.get(2).getProvider()).isEqualTo("virtualbox");
        assertThat(updates.get(2).getLocalVersion()).isEqualTo("20180126.0.0");
        assertThat(updates.get(2).getRemoteVersion()).isEqualTo("20180718.0.0");
        assertThat(updates.get(2).getState()).isEqualTo(State.OUTDATED);

        assertThat(updates.get(3).getName()).isEqualTo("msabramo/mssqlserver2014express");
        assertThat(updates.get(3).getProvider()).isEqualTo("virtualbox");
        assertThat(updates.get(3).getLocalVersion()).isEqualTo("v0.1");
        assertThat(updates.get(3).getRemoteVersion()).isEqualTo("v0.1");
        assertThat(updates.get(3).getState()).isEqualTo(State.UP_TO_DATE);

        assertThat(updates.get(4).getName()).isEqualTo("test/mybox");
        assertThat(updates.get(4).getProvider()).isEqualTo("virtualbox");
        assertThat(updates.get(4).getLocalVersion()).isNull();
        assertThat(updates.get(4).getRemoteVersion()).isNull();
        assertThat(updates.get(4).getState()).isEqualTo(State.NO_VERSION_INFORMATION);
    }

    @Test
    void testParseBoxOutdatedOutputWithErrors()
    throws VagrantException
    {
        String s = "* 'ubuntu/artful64' for 'virtualbox': Error loading metadata: Illegal characters found in URL\n" +
                "* 'ubuntu/bionic64' for 'virtualbox': Error loading metadata: Could not resolve host: vagrantcloud.com\n" +
                "* 'msabramo/mssqlserver2014express' for 'virtualbox': Error loading metadata: Could not resolve host: vagrantcloud.com\n" +
                "* 'test/mybox' for 'virtualbox' wasn't added from a catalog, no version information";
        List<? extends BoxUpdateStatus> updates = vagrant.parseBoxOutdatedOutput(s);

        assertThat(updates).hasSize(4);

        assertThat(updates.get(0).getName()).isEqualTo("ubuntu/artful64");
        assertThat(updates.get(0).getProvider()).isEqualTo("virtualbox");
        assertThat(updates.get(0).getLocalVersion()).isNull();
        assertThat(updates.get(0).getRemoteVersion()).isNull();
        assertThat(updates.get(0).getState()).isEqualTo(State.ERROR);
        assertThat(updates.get(0).getDetail()).contains("Illegal characters found in URL");

        assertThat(updates.get(1).getName()).isEqualTo("ubuntu/bionic64");
        assertThat(updates.get(1).getProvider()).isEqualTo("virtualbox");
        assertThat(updates.get(1).getLocalVersion()).isNull();
        assertThat(updates.get(1).getRemoteVersion()).isNull();
        assertThat(updates.get(1).getState()).isEqualTo(State.ERROR);
        assertThat(updates.get(1).getDetail()).contains("Could not resolve host: vagrantcloud.com");

        assertThat(updates.get(2).getName()).isEqualTo("msabramo/mssqlserver2014express");
        assertThat(updates.get(2).getProvider()).isEqualTo("virtualbox");
        assertThat(updates.get(2).getLocalVersion()).isNull();
        assertThat(updates.get(2).getRemoteVersion()).isNull();
        assertThat(updates.get(2).getState()).isEqualTo(State.ERROR);
        assertThat(updates.get(2).getDetail()).contains("Could not resolve host: vagrantcloud.com");

        assertThat(updates.get(3).getName()).isEqualTo("test/mybox");
        assertThat(updates.get(3).getProvider()).isEqualTo("virtualbox");
        assertThat(updates.get(3).getLocalVersion()).isNull();
        assertThat(updates.get(3).getRemoteVersion()).isNull();
        assertThat(updates.get(3).getState()).isEqualTo(State.NO_VERSION_INFORMATION);
    }
}
