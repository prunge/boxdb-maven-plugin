package au.net.causal.maven.plugins.boxdb;

import org.junit.jupiter.api.Test;

import static org.assertj.core.api.Assertions.*;

class TestVersions 
{
    @Test
    void majorVersionFromNumeric() 
    {
        assertThat(Versions.majorVersion("5.6.7")).isEqualTo("5");
    }
    
    @Test
    void majorVersionFromMixed() 
    {
        assertThat(Versions.majorVersion("6.1e")).isEqualTo("6");
    }
    
    @Test
    void majorVersionFromSingleNumeric() 
    {
        assertThat(Versions.majorVersion("2012")).isEqualTo("2012");
    }
    
    @Test
    void majorVersionFromSingleMixed() 
    {
        assertThat(Versions.majorVersion("11g")).isEqualTo("11");
    }
}
