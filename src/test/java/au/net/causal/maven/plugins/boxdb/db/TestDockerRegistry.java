package au.net.causal.maven.plugins.boxdb.db;

import au.net.causal.maven.plugins.boxdb.db.DockerRegistry.ReadManifestResult;
import com.github.paweladamski.httpclientmock.HttpClientMock;
import org.junit.jupiter.api.Disabled;
import org.junit.jupiter.api.Test;

import java.io.IOException;
import java.net.URI;
import java.util.List;

import static org.assertj.core.api.Assertions.assertThat;

class TestDockerRegistry
{
    private static final URI TEST_URI = URI.create("http://galah.galah.galah");

    @Test
    void testRegistry()
    throws IOException
    {
        HttpClientMock http = new HttpClientMock(TEST_URI.getHost());
        http.onGet()
            .doReturnJSON("{\"name\":\"library/postgres\",\"tags\":[\"10-alpine\",\"10-beta1-alpine\",\"10-beta1\",\"latest\"]}");
        DockerRegistry r = new DockerRegistry(TEST_URI, http);
        List<String> result = r.readTags("library/postgres");
        assertThat(result).containsExactly("10-alpine", "10-beta1-alpine", "10-beta1", "latest");
    }

    @Test
    void testResponseParsing()
    {
        String responseString = "{\"name\":\"library/postgres\",\"tags\":[\"10-alpine\",\"10-beta1-alpine\",\"10-beta1\",\"latest\"]}";
        DockerRegistry r = new DockerRegistry(TEST_URI);
        List<String> result = r.parseTagsFromResponse(responseString);
        assertThat(result).containsExactly("10-alpine", "10-beta1-alpine", "10-beta1", "latest");
    }

    /**
     * Use this to test against a real-life Docker registry.
     */
    //@Test
    @Disabled("Don't do real network tests in unit tests")
    void testAgainstRealRegistry()
    throws IOException
    {
        DockerRegistry r = new DockerRegistry(URI.create("https://index.docker.io/v2/"));
        List<String> result = r.readTags("library/postgres");

        System.out.println(result);
    }

    //@Test
    @Disabled("Don't do real network tests in unit tests")
    void testReadManifest()
    throws IOException
    {
        DockerRegistry r = new DockerRegistry(URI.create("https://index.docker.io/v2/"));
        ReadManifestResult result = r.readManifest("library/postgres", "9.5.15"); //New manifest
        //ReadManifestResult result = r.readManifest("library/postgres", "8.4"); //Old manifest does not have hash
        //ReadManifestResult result = r.readManifest("library/postgres", "7.0"); //Not found

        System.out.println(result);
    }
}
