package au.net.causal.maven.plugins.boxdb;

import org.junit.jupiter.api.Test;

import java.io.BufferedReader;
import java.io.IOException;
import java.io.StringReader;
import java.util.ArrayList;
import java.util.List;

import static org.assertj.core.api.Assertions.*;

class TestSqlDivider
{
    private final SqlDivider divider = new SqlDivider();

    @Test
    void singleStatementWithEndSemicolon()
    throws IOException
    {
        String s = "select * from mytable;";

        List<String> sqls = new ArrayList<>();
        divider.splitAndProcessSql(new BufferedReader(new StringReader(s)), sqls::add);

        assertThat(sqls).contains("select * from mytable");
    }

    @Test
    void singleStatementWithoutEndSemicolon()
    throws IOException
    {
        String s = "select * from mytable";

        List<String> sqls = new ArrayList<>();
        divider.splitAndProcessSql(new BufferedReader(new StringReader(s)), sqls::add);

        assertThat(sqls).contains("select * from mytable");
    }

    @Test
    void oneStatementTwoLines()
    throws IOException
    {
        String s = "select * from mytable\nwhere name = 'Dinah-Kah';";

        List<String> sqls = new ArrayList<>();
        divider.splitAndProcessSql(new BufferedReader(new StringReader(s)), sqls::add);

        assertThat(sqls).contains("select * from mytable where name = 'Dinah-Kah'");
    }

    @Test
    void twoStatementsTwoLines()
    throws IOException
    {
        String s = "select * from mytable;\nselect name from cats;";

        List<String> sqls = new ArrayList<>();
        divider.splitAndProcessSql(new BufferedReader(new StringReader(s)), sqls::add);

        assertThat(sqls).containsSequence("select * from mytable", "select name from cats");
    }

    @Test
    void semicolonWithinQuotes()
    throws IOException
    {
        String s = "select * from mytable where name = ';'\n" +
                    "and age > 2;";

        List<String> sqls = new ArrayList<>();
        divider.splitAndProcessSql(new BufferedReader(new StringReader(s)), sqls::add);

        assertThat(sqls).contains("select * from mytable where name = ';' and age > 2");
    }
}
